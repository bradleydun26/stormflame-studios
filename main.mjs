import HyperExpress from 'hyper-express';
import LiveDirectory from 'live-directory';
import {MongoClient} from 'mongodb';
import {exec} from 'child_process';
import Filter from 'bad-words';
import TokenGenerator from 'uuid-token-generator';

import {FFA_SERVER} from '/home/ubuntu/stormflame-studios/pixel-tanks/ffa-server.mjs'; // FFA Server

const uri = 'mongodb+srv://cs641311:355608-G38@cluster0.z6wsn.mongodb.net/?retryWrites=true&w=majority';
const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

var db;
client.connect((err) => {
  if (err) {
    console.error(err);
    console.log('Main: Error on connection to database.');
  }
  console.log('Main: Connected to database.');
  db = client.db('data').collection('data');
  console.log('Main: Successfully accessed data.');
});

const filter = new Filter();

var tokens = [];
const tokgen = new TokenGenerator(256, TokenGenerator.BASE62); 

var sockets = [];
var banned = [];

const Server = new HyperExpress.Server({
  fast_buffers: true,
});

const Router = new HyperExpress.Router();

Router.ws('/', {
  compression: HyperExpress.compressors.SHARED_COMPRESSOR,
  maxPayloadLength: Infinity,
  idle_timeout: Infinity,
}, (socket) => {
  sockets.push(socket);
  socket.on('message', async (data) => {
    try {
      data = JSON.parse(data);
    } catch(e) {
      socket.send('Evanism Detected and Denied.');
      setTimeout(function() {
        socket.destroy();
      }.bind(this), 100);
      return;
    }
    if (!socket.username) {
      socket.username = data.username;
      if (banned.includes(data.username)) {
        socket.send('{"status":"error","message":"You are banned."}');
        setTimeout(function() {
          socket.destroy();
        }.bind(this), 1000);
        return;
      }
    }
    if (data.op === 'auth') {
      if (data.username === '' || !data.username) {
        socket.send('{"status":"error","message":"Invalid Username."}');
        return;
      }
      if (data.username === filter.clean(data.username)) {
        var item = await db.findOne({
          username: data.username,
        });
        if (item === null) {
          if (data.type === 'signup') {
            var token = tokgen.generate();
            tokens.push({
              username: data.username,
              token: token,
            });
            if (await db.insertOne({
              username: data.username,
              password: data.password,
              playerdata: '{}',
            })) {
              socket.send(JSON.stringify({
                status: 'success',
                token: token,
              }));
            } else {
              socket.send('{"status":"error","message":"Something went wrong, please try again."}');
            }
          } else if (data.type === 'login') {
            socket.send('{"status":"error","message":"This account does not exist."}')
          }
        } else {
          if (data.type === 'signup') {
            socket.send('{"status":"error","message":"This account already exists."}');
          } else if (data.type === 'login') {
            if (item.password === data.password) {
              item = null;
              var token = tokgen.generate();
              tokens.push({
                username: data.username,
                token: token,
              });
              socket.send(JSON.stringify({
                status: 'success',
                token: token,
              }));
            } else {
              socket.send('{"status":"error","message":"Incorrect password."}');
            }
          }
        }
      } else {
        socket.send('{"status":"error","message":"Username contains inappropriate word."}');
      }
    } else if (data.op === 'database') {
      if (!data.token) {
        socket.send('{"status":"error", "message":"No token."}');
        return;
      }
      var l = 0, valid = false;
      while (l<tokens.length) {
        if (tokens[l].username === data.username) {
          if (tokens[l].token === data.token) {
            valid = true;
          }
        }
        l++;
      }
      // Temporary Time-Saver
      valid = true;
      // REMOVE
      if (!valid) {
        socket.send('{"status":"error", "message":"Invalid token."}');
        return;
      }
      if (data.type === 'get') {
        try {
          socket.send(JSON.stringify({
            status: 'success',
            type: 'get',
            data: JSON.stringify(await db.findOne({
              username: data.username,
            }), (name, value) => {
              if (name === 'password') {
                return undefined;
              } else {
                return value;
              }
            }),
          }));
        } catch(e) {
          socket.send('{"status":"error", "message":"Error occurred while fetching data. Username='+data.username+' Error='+e+'"}');
        }
      } else if (data.type === 'set') {
        try {
          var temp = await db.findOne({
            username: data.username,
          });
        } catch(e) {
          socket.send('{"status":"error", "message":"Failed to locate item."}');
        }
        temp[data.key] = data.value;
        try {
          db.updateOne({
            username: data.username,
          }, {
            $set: temp,
          });
        } catch(e) {
          socket.send('{"status":"error", "message":"Failed to update item."}');
        }
      } else {
        socket.send('{"status":"error", "message":"Invalid or no task."}');
      }
    } else if (data.op === 'chat') {

    } else if (data.op === 'admin') {

    } else if (data.op === 'reboot') {
      if (socket.username === 'cs641311') exec('sudo pm2 restart web');
    } else {
      socket.send('{"status":"error", "message":"Invalid or no operation."}');
    }
  });
});

const PixelTanksStatic = new LiveDirectory({
  path: './pixel-tanks/',
  keep: {
    extensions: ['.otf', '.jar', '.ttf', '.css', '.js', '.json', '.png', '.jpg', '.jpeg', '.html', '.ico', '.mp3', '.mp4'],
  },
  ignore: (path) => {
    return path.startsWith('.');
  }
});

const KingdomsStatic = new LiveDirectory({
  path: './kingdoms/',
  keep: {
    extensions: ['.otf', '.jar', '.ttf', '.css', '.js', '.json', '.png', '.jpg', '.jpeg', '.html', '.ico', '.mp3', '.mp4'],
  },
  ignore: (path) => {
    return path.startsWith('.');
  }
});

Server.use((req, res, next) => {
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
})

Server.get('/*', (req, res) => {
  var path = req.path;
  if (path === '/') {
    path = '/index.html';
  }
  if (!path.includes('.')) {
    path += '.html';
  }
  var file;
  if (req.hostname.includes('tanks') || req.hostname === 'lunar69.ml') {
    file = PixelTanksStatic.get(path);
  } else if (req.hostname.includes('kingdom')) {
    file = KingdomsStatic.get(path);
  } else {
    return res.end('hi world!');
  }
  if (file === undefined) {
    return res.status(404).end('404');
  }
  return res.type(file.extension).send(file.buffer);
});

Server.use(FFA_SERVER);
Server.use(Router);
Server.listen(8888);
