(() => {
  window.oncontextmenu = () => {
    return false;
  }

  window.onerror = function(a, b, c) {
    var error = document.createElement('P');
    error.innerHTML = a + ' ' + b + ' ' + c;
    document.body.appendChild(error);
  }

  window.onload = () => {
    Kingdoms.start(); 
  }

  window.addEventListener('resize', () => {
    GUI.canvas.width = window.innerHeight*1.24;
    GUI.canvas.height = window.innerHeight;
    Kingdoms.resizer = window.innerHeight / 500;
    GUI.draw.setTransform(Kingdoms.resizer, 0, 0, Kingdoms.resizer, 0, 0);
    Menus.redraw();
  });
})();

class MegaSocket {
  constructor(url, options) {
    this.url = url;
    this.options = options;
    this.callstack = {
      open: [],
      close: [],
      message: [],
    }
    if (this.options == undefined) {
      this.options = {};
    }
    if (this.options.keepAlive == undefined) {
      this.options.keepAlive = true;
    }
    if (this.options.autoconnect == undefined) {
      this.options.autoconnect = true;
    }
    if (this.options.reconnect == undefined) {
      this.options.reconnect = true;
    }
    if (this.options.autoconnect) {
      this.status = 'connecting';
      this.connect();
    } else {
      this.status = 'idle';
      window.addEventListener('offline', function() { // Arrow Function Wont Work Due To Bind
        this.socket.close();
        this.socket.onclose();
      }.bind(this));
      if (this.options.reconnect) {
        window.addEventListener('online', function() {
          this.connect();
        }.bind(this))
      }
    }
  }
  connect() {
    try {
      this.socket = new WebSocket(this.url);
    } catch (e) {
      console.warn('WebSocket had trouble connecting to ' + this.url);
    }
    this.socket.onopen = function() {
      this.status = 'connected';
      if (this.options.keepAlive) {
        this.socket.keepAlive = setInterval(function() {
          this.socket.send('{}');
        }.bind(this), 50000);
      }
      var l = 0;
      while (l < this.callstack.open.length) {
        this.callstack.open[l]();
        l++;
      }
    }.bind(this);
    this.socket.onmessage = function(data) {
      data = JSON.parse(data.data);
      // custom error handling for server
      if (data.status === 'error') {
        alert('WebSocket Error: ' + data.message);
        return;
      }
      // end
      var l = 0;
      while (l < this.callstack.message.length) {
        this.callstack.message[l](data);
        l++;
      }
    }.bind(this);
    this.socket.onclose = function() {
      clearInterval(this.socket.keepAlive);
      this.status = 'disconnected';
      var l = 0;
      while (l < this.callstack.close.length) {
        this.callstack.close[l]();
        l++;
      }
      if (this.options.reconnect) {
        this.connect();
      }
    }.bind(this);
  }
  on(event, operation) {
    if (event == 'connect') {
      this.callstack.open.push(operation);
    }
    if (event == 'message') {
      this.callstack.message.push(operation);
    }
    if (event == 'close') {
      this.callstack.close.push(operation);
    }
  }
  no(event) {
    if (event == 'connect') {
      this.callstack.open = [];
    }
    if (event == 'message') {
      this.callstack.message = [];
    }
    if (event == 'close') {
      this.callstack.close = [];
    }
  }
  send(data) {
    data = JSON.stringify(data);
    this.socket.send(data);
  }
  close() {
    this.socket.close();
  }
}

class Network {
  static get(callback) {
    Kingdoms.socket.send({
      op: 'database',
      type: 'get',
      username: sessionStorage.username,
      token: sessionStorage.token,
    });
    Kingdoms.socket.on('message', (data) => {
      if (data.status === 'success' && data.type === 'get') {
        Kingdoms.socket.no('message');
        callback(JSON.parse(data.data));
      }
    });
  }
  static update(key, value) {
    try {
      Kingdoms.socket.send({
        op: 'database',
        type: 'set',
        username: sessionStorage.username,
        token: sessionStorage.token,
        key: key,
        value: value,
      });
      Kingdoms.socket.on('message', function(data) {
        if (data.success) {
          Kingdoms.socket.no('message');
          console.log('Saved Game Successfully!');
        }
      });
    } catch (e) {}
  }
  static auth(username, password, type, callback) {
    Kingdoms.socket.send({
      op: 'auth',
      type: type,
      username: username,
      password: password,
    });
    Kingdoms.socket.on('message', (data) => {
      if (data.status === 'success') {
        Kingdoms.socket.no('message');
        sessionStorage.username = username;
        sessionStorage.token = data.token;
        callback();
      }
    })
  }
}

class Menu {
  constructor(draw, listeners, element, context) {
    this.draw = draw.bind(this);
    this.element = element;
    this.listeners = listeners;
    if (context) {
      for (let property in this.listeners) {
        this.listeners[property] = this.listeners[property].bind(context);
      }
    } else {
      for (let property in this.listeners) {
        this.listeners[property] = this.listeners[property].bind(this);
      }
    }
  }
  addListeners() {
    for (let property in this.listeners) {
      this.element.addEventListener(property, this.listeners[property]);
    }
  }
  removeListeners() {
    for (let property in this.listeners) {
      this.element.removeEventListener(property, this.listeners[property]);
    }
  }
}

class Menus {
  static trigger(name) {
    if (Menus.current != undefined) {
      Menus.menus[Menus.current].removeListeners();
    }
    Menus.current = name;
    Menus.menus[Menus.current].draw();
    Menus.menus[Menus.current].addListeners();
  }

  static onclick(e) {
    var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
    var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    x -= GUI.canvas.offsetLeft;
    y -= GUI.canvas.offsetTop;
    x /= Kingdoms.resizer;
    y /= Kingdoms.resizer;
    var l = 0;
    while (l < this.data.buttons.length) {
      if (x > this.data.buttons[l].x && x < this.data.buttons[l].x + this.data.buttons[l].w) {
        if (y > this.data.buttons[l].y && y < this.data.buttons[l].y + this.data.buttons[l].h) {
          Menus.trigger(this.data.buttons[l].ref);
        }
      }
      l++;
    }
    var l = 0;
    while (l < this.data.exec.length) {
      if (x > this.data.exec[l].x && x < this.data.exec[l].x + this.data.exec[l].w) {
        if (y > this.data.exec[l].y && y < this.data.exec[l].y + this.data.exec[l].h) {
          eval(this.data.exec[l].ref);
        }
      }
      l++;
    }
  }

  static redraw() {
    Menus.menus[Menus.current].draw();
  }

  static removeListeners() {
    Menus.menus[Menus.current].removeListeners();
  }

}

class ImageLoader extends Image { // loads all images

  constructor() {
    super();
    super.onload = this.onload;
    super.onerror = this.onerror;
    Kingdoms.totalImages++;
  }

  onload() {
    Kingdoms.loadedImages++;
    Kingdoms.updateBootProgress();
  }

  onerror() {
    throw '[Kingdoms]:[boot]: Image ' + this.src + ' failed to load. (Gamebreaking) (Error)';
  }

}

class GUI {}

class Kingdoms {

  static start() {
    Kingdoms.setup();
    Kingdoms.boot();
  }

  static updateBootProgress() {
    GUI.draw.font = '20px Moron';
    GUI.draw.clearRect(0, 0, 620, 500);
    GUI.draw.fillStyle = '#000000';
    GUI.draw.fillRect(0, 0, 620, 500);
    GUI.draw.textAlign = 'center';
    GUI.draw.fillStyle = '#ffffff';
    GUI.draw.fillText('Loading ' + Math.floor(Kingdoms.loadedImages / Kingdoms.totalImages * 100) + '%', 250, 250);
    if (Kingdoms.loadedImages == Kingdoms.totalImages) {
      Kingdoms.launch();
    }
  }

  static boot() {
    Kingdoms.loadedImages = 0;
    Kingdoms.totalImages = 0;

    this.images = {
      menus: {
        start: 'start.png',
        main: 'main.png',
        inventory: 'inventory.png',
        armorTab: 'armor_tab.png',
        runes: 'runes.png',
        stats: 'stats.png',
        settings: 'settings.png',
        cheats: 'settings-cheats.png',
        controls: 'settings-controls.png',
        skins: 'settings-skins.png',
        multiplayer: 'multiplayer.png',
      },
      items: {
        mithril_armor: 'mithril_armor.png',
        gold_armor: 'gold_armor.png',
        obsidian_armor: 'obsidian_armor.png',
        amethyst_robe: 'amethyst_robe.png',
      }
    }

    for (var property in this.images.menus) {
      var src = this.images.menus[property];
      this.images.menus[property] = new ImageLoader();
      this.images.menus[property].src = '/images/gui/menus/'+src;
    }

    for (var property in this.images.items) {
      var src = this.images.items[property];
      this.images.items[property] = new ImageLoader();
      this.images.items[property].src = '/images/gui/items/inventory/'+src;
    }

    var menuActions = {
      start: {
        buttons: [],
        exec: [{
          x: 199,
          y: 410,
          w: 106,
          h: 32,
          ref: `(() => {
            Network.auth(Menus.menus.start.username, Menus.menus.start.password, 'login', () => {
              Kingdoms.user = {};
              Kingdoms.user.username = sessionStorage.username;
              Network.get((data) => {
                Kingdoms.userData = JSON.parse(data.playerdata)['kingdoms'];
                Kingdoms.playerdata = JSON.parse(data.playerdata);
                if (Kingdoms.userData === undefined) {
                  Kingdoms.userData = {};
                }
                Menus.trigger('main');
              });
            });
          })();`,
        }, {
          x: 314,
          y: 410,
          w: 106,
          h: 32,
          ref: `(() => {
            Network.auth(Menus.menus.start.username, Menus.menus.start.password, 'signup', () => {
              Kingdoms.user = {};
              Kingdoms.user.username = sessionStorage.username;
              Network.get((data) => {
                Kingdoms.userData = JSON.parse(data.playerdata)['kingdoms'];
                Kingdoms.playerdata = JSON.parse(data.playerdata);
                Menus.trigger('main');
              })
            })
          })();`,
        }, {
          x: 199,
          y: 326,
          w: 221,
          h: 32,
          ref: `(() => {
            Menus.menus.start.type = 'username';
          })();`
        }, {
          x: 199,
          y: 368,
          w: 221,
          h: 32,
          ref:  `(() => {
            Menus.menus.start.type = 'password';
          })();`
        }],
        listeners: {
          keydown: (e) => {
            var start = Menus.menus.start;
            if (e.key.length === 1) {
              start[start.type] += e.key;
            } else if (e.keyCode === 8) {
              start[start.type] = start[start.type].slice(0, -1);
            }
            Menus.redraw();
          },
        },
        customOnLoad: () => {
          /*if (sessionStorage.username) {
            Kingdoms.user = {};
            Kingdoms.user.username = sessionStorage.username;
            if (Kingdoms.socket.status !== 'connected') {
              alert(`Status: not connected, fatal, 
                VAR: Menus.menus.start.customOnLoad ->
                `+this.customOnLoad+`
                ctx: this.customOnLoad
              `)
              setTimeout(this.customOnLoad, 15);
              return;
            }
            Network.get((data) => {
              Kingdoms.userData = JSON.parse(data.playerdata)['pixel-tanks'];
              Kingdoms.playerData = JSON.parse(data.playerdata);
              if (Kingdoms.userData.material === undefined || Kingdoms.userData.material === null) {
                  Kingdoms.userData.material = Kingdoms.userData.health !== undefined ? (Kingdoms.userData.health-200)/100 : 0;
                }
              Kingdoms.userData.health = undefined; { // TEMP FIX
                Kingdoms.userData.item1 = 'toolkit';
                Kingdoms.userData.item2 = 'weak';
                Kingdoms.userData.item3 = 'duck_tape';
                Kingdoms.userData.item4 = 'bomb';
                if (!Kingdoms.userData.settings) {
                  Kingdoms.userData.settings = {
                    item1: 49,
                    item2: 50,
                    item3: 51,
                    item4: 52,
                  }
                }
              }
              Menus.trigger('main');
            });
          }*/
          if (!Menus.menus.start.type) {
            Menus.menus.start.type = 'username';
            Menus.menus.start.username = '';
            Menus.menus.start.password = '';
          }
          GUI.draw.fillStyle = '#ffffff';
          GUI.draw.font = '15px Moron';
          GUI.draw.textAlign = 'left';
          GUI.draw.fillText(Menus.menus.start.username, 205, 350);
          var l = 0,
            temp = '';
          while (l < Menus.menus.start.password.length) {
            temp += '*';
            l++;
          }
          GUI.draw.fillText(temp, 205, 395);
        },
      },
      main: {
        buttons: [{
          x: 80,
          y: 36,
          w: 46,
          h: 46,
          ref: 'inventory'
        }, {
          x: 494,
          y: 36,
          w: 46,
          h: 46,
          ref: 'settings',
        }, {
          x: 81,
          y: 408,
          w: 186,
          h: 36,
          ref: 'singleplayer',
        }, {
          x: 353,
          y: 408,
          w: 186,
          h: 36,
          ref: 'multiplayer',
        }],
        exec: [],
        listeners: {},
        customOnLoad: () => {},
      },
      inventory: {
        buttons: [{
          x: 40,
          y: 40,
          w: 118,
          h: 28,
          ref: 'stats',
        }, {
          x: 462,
          y: 40,
          w: 118,
          h: 28,
          ref: 'runes',
        }],
        listeners: {
          keydown: (e) => {
            if (e.keyCode === 73) Menus.trigger('main');
          },
          mousedown: (e) => {
            var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
            var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
            x -= GUI.canvas.offsetLeft;
            y -= GUI.canvas.offsetTop;
            x /= Kingdoms.resizer;
            y /= Kingdoms.resizer;
            if (Menus.menus.inventory.armorTab) {
              if (x > 116 && x < 164 && y > 274 && y < 322) {
                Kingdoms.userData.armor = 'mithril_armor';
              }
              if (x > 168 && x < 216 && y > 274 && y < 322) {
                Kingdoms.userData.armor = 'gold_armor';
              }
              if (x > 116 && x < 164 && y > 326 && y < 374) {
                Kingdoms.userData.armor = 'obsidian_armor';
              }
              if (x > 168 && x < 216 && y > 326 && y < 374) {
                Kingdoms.userData.armor = 'amethyst_robe';
              }
              Menus.menus.inventory.armorTab = false;
              Menus.redraw();
            } else {
              if (x > 91 && x < 241 && y > 249 && y < 399) {
                Menus.menus.inventory.armorTab = true;
                Menus.redraw();
              }
            }
            if (Menus.menus.inventory.weaponTab) {

            } else {
              if (x > 91 && x < 241 && y > 116 && y < 216) {
                alert('idot why would you want something to work?????');
              }
            }
          }
        },
        customOnLoad: () => {
          if (Kingdoms.userData.armor != undefined) {
            GUI.draw.drawImage(Kingdoms.images.items[Kingdoms.userData.armor], 91, 249);
          }
          if (Menus.menus.inventory.armorTab) {
            // draw tab
            GUI.draw.drawImage(Kingdoms.images.menus.armorTab, 112, 270);
          }
        },
      },
      runes: {
        buttons: [{
          x: 40,
          y: 40,
          w: 118,
          h: 28,
          ref: 'inventory',
        }, {
          x: 462,
          y: 40,
          w: 118,
          h: 28,
          ref: 'stats',
        }],
        exec: [],
        listeners: {
          keydown: (e) => {
            if (e.keyCode === 73) Menus.trigger('main');
          }
        },
        customOnLoad: () => {},
      },
      stats: {
        buttons: [{
          x: 40,
          y: 40,
          w: 118,
          h: 28,
          ref: 'runes',
        }, {
          x: 462,
          y: 40,
          w: 118,
          h: 28,
          ref: 'inventory',
        }],
        exec: [],
        listeners: {
          keydown: (e) => {
            if (e.keyCode === 73) Menus.trigger('main');
          }
        },
        customOnLoad: () => {},
      },
      settings: {
        buttons: [{
          x: 70,
          y: 106,
          w: 202,
          h: 45,
          ref: 'controls',
        }, {
          x: 349,
          y: 106,
          w: 202,
          h: 45,
          ref: 'skins'
        }, {
          x: 243,
          y: 294,
          w: 134,
          h: 45,
          ref: 'cheats',
        }, {
          x: 40,
          y: 40,
          w: 68,
          h: 28,
          ref: 'main',
        }],
        exec: [{
          x: 243,
          y: 386,
          w: 134,
          h: 45,
          ref: 'alert("idk")',
        }, {
          x: 210,
          y: 200,
          w: 202,
          h: 45,
          ref: 'alert("die idc about sound")',
        }],
        listeners: {},
        customOnLoad: () => {},
      },
      cheats: {
        buttons: [{
          x: 40,
          y: 40,
          w: 68,
          h: 28,
          ref: 'settings',
        }],
        exec: [],
        listeners: {},
        customOnLoad: () => {},
      },
      controls: {
        buttons: [{
          x: 40,
          y: 40,
          w: 68,
          h: 28,
          ref: 'settings',
        }],
        exec: [],
        listeners: {},
        customOnLoad: () => {},
      },
      skins: {
        buttons: [{
          x: 40,
          y: 40,
          w: 68,
          h: 28,
          ref: 'settings',
        }],
        exec: [],
        listeners: {},
        customOnLoad: () => {},
      },
      multiplayer: {
        buttons: [{
          x: 40,
          y: 40,
          w: 68,
          h: 28,
          ref: 'main',
        }],
        exec: [],
        listeners: {},
        customOnLoad: () => {},
      },
    }

    Menus.menus = {
      start: '',
      main: '',
      inventory: '',
      stats: '',
      runes: '',
      settings: '',
      controls: '',
      cheats: '',
      multiplayer: '',
      skins: '',
    }

    for (var property in Menus.menus) {
      Menus.menus[property] = new Menu(function() { // No arrow function here
        if (Kingdoms.images.menus[this.id]) {
          GUI.draw.drawImage(Kingdoms.images.menus[this.id], 0, 0);
        }
        this.data.customOnLoad();
      }, {
        click: Menus.onclick,
        ...menuActions[property].listeners,
      }, GUI.canvas);
      Menus.menus[property].data = menuActions[property];
      Menus.menus[property].id = property;
    }
    Kingdoms.socket = new MegaSocket('wss://'+window.location.hostname, {
      keepAlive: false,
    })
  }

  static setup() {
    GUI.canvas = document.getElementById('canvas');
    GUI.canvas.width = window.innerHeight*1.24;
    GUI.canvas.height = window.innerHeight;
    Kingdoms.resizer = window.innerHeight / 500; // scale factor
    GUI.draw = GUI.canvas.getContext('2d');
    GUI.draw.setTransform(Kingdoms.resizer, 0, 0, Kingdoms.resizer, 0, 0);
    GUI.draw.webkitImageSmoothingEnabled = false;
    GUI.draw.mozImageSmoothingEnabled = false;
    GUI.draw.imageSmoothingEnabled = false;
    GUI.draw.font = '15px Courier';
    GUI.draw.fillText('abcdefghijklmnopqrstuvwxyz', 0, 0); // init font
    GUI.draw.clearRect(0, 0, 620, 500);
    GUI.draw.font = '20px Courier';
    GUI.draw.fillStyle = '#000000';
    GUI.draw.fillRect(0, 0, 620, 500);
    GUI.draw.textAlign = 'center';
    GUI.draw.fillStyle = '#ffffff';
    GUI.draw.fillText('Loading 0%', 250, 250);
  }

  static launch() {
    Menus.trigger('start');
  }
}

class Room {
  constructor(options) {
    options = JSON.parse(options);
    this.colliders = options.colliders;
    this.backImage = options.backImage;
    this.frontImage = options.frontImage;
  }
}

class World {
  static triggerRoom(name) {

  }
}

