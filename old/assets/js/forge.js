var canvas = document.getElementById('canvas');
var draw = canvas.getContext('2d');

var player_image = new Image();
player_image.src = '/assets/images/forge/character_left.png';
class Player {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.oldChunk = {};
    this.dy = 0;
    this.i = [];
    this.t = [];
    this.keyIntervals = [];
    this.keyHandlers = [];
    this.draw = this.draw.bind(this);
    this.update = this.update.bind(this);
    this.move = this.move.bind(this);
    this.w = 16; // 4 Blocks Wide
    this.h = 44; // 11 Blocks Tall
    this.i.push(setInterval(this.update, 20));
  }
  draw() {
    draw.fillStyle = '#000000';
    draw.drawImage(player_image, this.x, this.y);
  }
  update() {
    var results = gravity(this.x, this.y+this.dy);
    if (results[0] == '1') {
      if (collision(this.x, results[1]-50)) {
        this.y = results[1]-50;
      }
      this.dy = 0;
    } else if (results[0] == '2') {
      this.canJump = true;
    } else if (results[0] == '3') {
      this.canJump = false;
      this.dy += .5;
    }
    if (collision(this.x, this.y+this.dy)) {
      this.y += this.dy;
    }
  }
  move(id) {
    if (id == 87 && this.canJump) {
      this.dy = -9;
    }
    if (id == 68) {
      if (collision(this.x+2, this.y)) {
        this.x += 2;
      }/* else {
        this.x += 2;
        var results = gravity(this.y);
        document.getElementById('logger').innerHTML = results[0];
        if (results[0] == '1' && this.canJump && collision(this.x, results[1]-50)) {
          this.y = results[1]-40;
          this.dy = 0;
        } else {
          this.x -= 2;
        }
      }*/
    } 
    if (id == 65) {
      if (collision(this.x-2, this.y)) {
        this.x -= 2;
      }/* else {
        this.x -= 2;
        var results = gravity(this.y);
        document.getElementById('logger').innerHTML = results[0];
        if (results[0] == '1' && this.canJump && collision(this.x, results[1]-50)) {
          this.y = results[1]-40;
          this.dy = 0;
        } else {
          this.x += 2;
        }
      }*/
    }
  }
  addEventListeners() {
    window.addEventListener('keydown', function(event) {
      if (!this.keyHandlers[event.keyCode]) {
        this.keyIntervals[event.keyCode] = setInterval(this.move, 15, event.keyCode);
        this.keyHandlers[event.keyCode] = true;
      }
    }.bind(this));
    window.addEventListener('keyup', function(event) {
      clearInterval(this.keyIntervals[event.keyCode]);
      this.keyHandlers[event.keyCode] = false;
    }.bind(this));
  }
}

class Slime {
  constructor(x, y) {
  
  }
}

class World {
  constructor() {
  }
}

function collision(x, y) {
  return true;
  var l = 0;
  while (l<user.world.platforms.length) {
    var platform = user.world.platforms[l];
    if ((x+20 > platform.x && x+20 < platform.x+20) || (x > platform.x && x < platform.x+20) || x == platform.x) {
      if ((y > platform.y-40) && (y < platform.y+20)) {
        return false;
      }
    }
    l++;
  }
  return true;
}

function gravity(x, y) {
  y+=50;
  if (y == 480 || y == 315) {
    return '2';
  } else if (y > 480 && y < 500) {
    return ['1', 480];
  } else if (y > 315 && y < 328) {
    return ['1', 315];
  } else return '3';
  /*
  var l = 0;
  while (l<user.world.platforms.length) {
    var platform = user.world.platforms[l]
    if ((x+20 > platform.x && x+20 < platform.x+20) || (x > platform.x && x < platform.x+20) || x == platform.x) {
      if (y+40 > platform.y && y+40 <= platform.y+20) {
        return ['1', platform.y];
      } else if (y+40 == platform.y) {
        return ['2']
      }
    }
    l++;
  }
  return ['3'];
  */
}

var user = {
  player: new Player(0, 250),
  world: new World(),
  data: null,
}

function runGame() {
  user.player.addEventListeners();
  setInterval(render, 20);
}
var bloooooobe = new Image();
bloooooobe.src = '/assets/images/forge/forge-room.png';
function render() {
  draw.clearRect(0, 0, 620, 500);
  draw.drawImage(bloooooobe, 0, 0);
  user.player.draw();
  draw.setTransform(1, 0, 0, 1, 0, 0);
}
runGame();
