import HyperExpress from 'hyper-express';
import LiveDirectory from 'live-directory';
import ytdl from 'ytdl-core';
import fs from 'fs';
import url from 'url';
import schedule from 'node-schedule';
import {MongoClient} from 'mongodb';
import Innertube from 'youtubei.js';
import {exec} from 'child_process';
import * as Sentry from '@sentry/node';
import '@sentry/tracing';
import * as util from 'util';

Sentry.init({
  dsn: "https://5f29aa9d8d354f6c8c16e83f38b88655@o569371.ingest.sentry.io/6003991",
  tracesSampleRate: 1.0,
});

var t;
setInterval(function() {
  if (t) {
    t.finish();
  }

  t = Sentry.startTransaction({
    op: 'Server',
    name: 'Transation',
  });

  Sentry.configureScope(scope => {
    scope.setSpan(t);
  });
}.bind(process), 120000);

process.on('uncaughtException', err => {
  Sentry.captureException(err);
  console.log('ERROR: '+err);
  console.log('FATAL!');
  console.log('Process forced to exit :(');
  process.exit(1) //mandatory (as per the Node.js docs)
})

var logs = [];
var originalLog = console.log;
console.log = function(data) {
  var l = 0;
  while (l < logSockets.length) {
    logSockets[l].send(JSON.stringify({
      event: 'log-update',
      data: {
        log: data,
      },
    }));
    l++;
  }
  logs.push(data);
  originalLog(data);
}

schedule.scheduleJob('0 0 * * *', () => {
  sessionTokens = [{
    username: 'admin',
    token: 1
  }, {
    username: 'CelestialKnight',
    token: 1
  }];
})

var sockets = [];
var logSockets = [];
var chatSockets = [];
var sessionTokens = [{
  username: 'admin',
  token: 1
}, {
  username: 'CelestialKnight',
  token: 1
}];
var bans = [];
var pvpRooms = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var duelRooms = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; // 10 Duel Rooms for Quick Play
var defenseRooms = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var tdmRooms = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var tagRooms = [0]
var status = {};
var servers = {
  tanks: {
    ffa: {},
    duels: {},
    defense: {},
    raids: {},
    tdm: {},
  },
  tag: {},
};

const uri = 'mongodb+srv://cs641311:355608-G38@cluster0.z6wsn.mongodb.net/?retryWrites=true&w=majority';
const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

var db, chat_db;
client.connect(function(err) {
  console.log('[DB] => Connected to remote DB server');
  db = client.db('data').collection('data');
  chat_db = client.db('data').collection('ChatServers');
  console.log('[DB] => Successfully accessed databases');
})

const core = new HyperExpress.Server({
  fast_buffers: true, // ZOOM
  max_body_length: 1000 * 1000 * 1000,
  fast_abort: true,
});

const youtube = await new Innertube();
core.get('/youtube-converter/download', async function(req, res, next) {
  const queryObject = url.parse(req.url, true).query;
  var video = queryObject.url;
  var quality = queryObject.quality;
  var filename = queryObject.name;
  if (filename == '') {
    filename = 'youtubetomp4-download';
  }
  res.attachment(filename+'.mp4');
  if (quality == 'default') {
    quality = '1080p';
  }
  res.stream(ytdl(video));
  return;
  if (!video.includes('youtube.com/watch?v=')) {
    res.status(403)
    res.header({
      'Content-Type': 'text/html'
    });
    res.send("<hr><h1 style='text-align:center'>Invalid Youtube Link: " + video + "</h1><hr>");
    console.log('Invalid Youtube String: ' + video);
    return;
  }
  var stream;
  if (quality != 'mp3') {
    stream = youtube.download(video.split('=')[1], {
      format: 'mp4',
      quality: '1080p',
      type: 'videoandaudio',
    });
  } else {
    stream = youtube.download(video.split('=')[1], {
      format: 'mp3',
      quality: quality, // if a video doesn't have a specific quality it'll fall back to 360p, also ignored when type is set to audio
      type: 'audio' // can be “video”, “audio” and “videoandaudio”
    });
    res.header('Content-Disposition', 'attachment;filename="' + filename + '.mp3"');
    res.header('Content-Type', ['application/octet-stream', 'video/mp3']);
  }
  var start;
  stream.on('info', (info) => {
    //res.attachment(filename+'.mp4')
    res.header('Content-Type', 'text/html'); 
    res.stream(stream);
  })
  stream.on('start', () => {
    start = new Date();
    console.log('Downloading: ' + video.split('=')[1]);
  });
  stream.on('end', () => {
    var end = new Date();
    var seconds = (end.getTime() - start.getTime()) / 1000;
    console.log(video.split('=')[1] + ': ' + seconds + ' second(s)');
  })
  stream.on('error', (error) => {
    res.removeHeader('Content-Disposition');
    res.removeHeader('Content-Type');
    res.header('Content-Type', 'text/html');
    res.write('<head><title>Please Try Again...</title></head><body><h1> Error Converting Video </h1><p>' + JSON.stringify(error) + '</body>');
    res.end();
  });
});

core.use(function(req, res, next) {
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
});

const LiveStatic = new LiveDirectory({
  path: './static/',
  keep: {
    extensions: ['.css', '.js', '.json', '.png', '.jpg', '.jpeg', '.html', '.ico', '.mp3', '.mp4'],
  },
  ignore: (path) => {
    return path.startsWith('.');
  }
})
core.get('/*', (req, res) => {
  var path = req.path;
  if (path === '/') {
    path = '/index.html';
  }
  if (!path.includes('.')) {
    path += '.html';
  }
  const file = LiveStatic.get(path);
  if (file === undefined) {
    return res.status(404).send('404');
  }
  return res.type(file.extension).send(file.buffer);
});

const Router = new HyperExpress.Router();

const options = {
  compression: HyperExpress.compressors.SHARED_COMPRESSOR,
  maxPayloadLength: Infinity, // AHAHAHAHA UNKILLABLE
  idle_timeout: Infinity, // AHAHAHAHAHAHA UNKILLABLE
}

Router.ws('/server', options, (socket) => {
  sockets.push(socket);
  socket.on('message', async function(data) {
    data = JSON.parse(data);
    var startTime = Date.now();
    if (socket.username == undefined) {
      socket.username = data.username;
      if (data.task != 'auth') {
        if (bans.includes(socket.username)) {
          socket.destroy();
        }
      }
    }
    if (data.operation === 'database') {
      /*var l = 0, valid = false;
      var len = sessionTokens.length;
      while (l < len) {
        if (sessionTokens[l].username == data.username) {
          if (data.token == sessionTokens[l].token) {
            valid = true;
          }
        }
        l++;
      }
      if (!valid && data.task != 'auth')  {
        console.log('REJECT');
        return;
      }*/
      if (data.task == 'list') {
        var values = [], items = [];
        var cursor = await db.findOne({});
        await cursor.forEach(function(value) {
          values.push(value);
        });
        console.log('LIST');
        var l = 0;
        while (l < values.length) {
          items.push(values[l][data.item]);
          l++;
        }
        socket.send(JSON.stringify({
          type: 'list-return',
          data: items,
        }));
      }
      if (data.task == 'get') {
        var values = [];
        var item = await db.findOne({
          username: data.username,
        });
        console.log(item);
        item.password = undefined;
        socket.send(JSON.stringify({
          type: 'get-return',
          data: [item],
        }))
      }
      if (data.task == 'auth') {
        if (bans.includes(data.username) || (data.username.includes('dmin') && data.username != 'admin')) {
          socket.send(JSON.stringify({
            isAccount: true,
            authencated: false,
            status: 400,
            message: 'You have been banned by an admin!',
          }));
          console.log('[SERVER] => ' + data.username + ' attempted to log on, but is banned.');
          return;
        }
        if (data.username.includes(' ')) {
          socket.send(JSON.stringify({
            isAccount: false,
            authenticated: false,
            status: 400,
            message: 'You are not allowed to have spaces in account names!',
          }));
          return;
        }
        if (data.username.length > 20) {
          socket.send(JSON.stringify({
            isAccount: false,
            authenticated: false,
            status: 400,
            message: 'You have exceed username length limit(20)!',
          }));
          return;
        }
        var values = [];
        var item = await db.findOne({
          username: data.username,
        });
        if (item == null) {
          if (data.authType == 'new-account') {
            var token = Math.random();
            sessionTokens.push({
              username: data.username,
              token: token
            });
            var success = await db.insertOne({
              username: data.username,
              password: data.password,
              playerdata: '{}',
              preferences: '{}',
              chat: '["public"]',
              fame: '0',
              userdata: JSON.stringify({coins: 0}),
              settings: '{}',
            });
            console.log('[SERVER] => New account ' + data.username + ' created.');
            if (!success.acknowledged) console.log('[DB] => Error creating new account ' + data.username);
            socket.send(JSON.stringify({
              created: success.acknowledged,
              authencated: false,
              isAccount: false,
              token: token,
              status: 200,
              message: 'Successfully Logged In',
            }));
            return;
          } else if (data.authType == 'login') {
            socket.send(JSON.stringify({
              isAccount: false,
              authencated: false,
              status: 404,
              message: 'This account does not exist!',
            }));
            return;
          }
        } else if (item != null && data.authType == 'new-account') {
          socket.send(JSON.stringify({
            isAccount: true,
            authenticated: false,
            status: 403,
            message: 'Account Username is Already Taken!',
          }));
          return;
        }
        if (item.password == data.password) {
          var token = Math.random();
          sessionTokens.push({
            username: data.username,
            token: token
          });
          console.log('[SERVER] => ' + data.username + ' logged in.');
          socket.send(JSON.stringify({
            authencated: true,
            token: token,
            status: 200,
            message: 'Successfully Logged In',
          }));
        } else {
          socket.send(JSON.stringify({
            authencated: false,
            isAccount: true,
            status: 400,
            message: 'Incorrect Password!',
          }));
          return;
        }
      }
      if (data.task == 'update') {
        var values = [];
        var item = await db.findOne({
          username: data.username,
        });
        item[data.key] = data.value;
        var mod = new Object();
        mod[data.key] = data.value;
        db.updateOne({
          username: item.username
        }, {
          $set: mod
        });
        socket.send(JSON.stringify({
          success: true,
        }));
      }
    } else if (data.operation === 'web-request') {
      var response = fs.readFileSync(data.url.substr(1)).toString();
      socket.send(JSON.stringify({
        event: 'web-response',
        data: response,
      }));
    } else if (data.operation === 'multiplayer') {
      if (data.task == 'tag') {
        if (socket.room == undefined) {
          socket.room = 'main';
          if (tagRooms[0] == 0) {
            socket.tagRoom = 0;
            servers.tag[socket.room] = new HostTag();
            servers.tag[socket.room].control(socket.room);
            servers.tag[socket.room].sockets.push(socket);
            tagRooms[0]++;
          } else {
            socket.tagRoom = 0;
            servers.tag[socket.room].sockets.push(socket);
            tagRooms[0]++;
          }
        } else {
          if (data.event == 'joinerupdate') {
            try {
              servers.tag[socket.room].joinerupdate(data);
            } catch {

            }
          }
          if (data.event == 'joinerjoin') {
            servers.tag[socket.room].joinerjoin(data);
          }
        }
      } else if (data.task == 'pixel-tanks') {
        if (data.mode == 'swap') {
          if (data.room == 'quick-join') {
            var found = false;
            var q = 0;
            while (!found) {
              if (servers.tanks[data.gamemode]) {
                if (servers.tanks[data.gamemode][data.room+q]) {
                  console.log('room found');
                  var l = 0;
                  while (l<servers.tanks[data.gamemode][data.room+q].sockets.length) { 
                    if (servers.tanks[data.gamemode][data.room+q].sockets[l].username === data.username) {
                      servers.tanks[data.gamemode][data.room+q].sockets[l].swapped = true;
                      servers.tanks[data.gamemode][data.room+q].sockets.splice(l, 1);
                      socket.gamemode = data.gamemode;
                      socket.room = data.room+q;
                      servers.tanks[data.gamemode][data.room+q].sockets.push(socket);
                      console.log('success');
                      found = true;
                    }
                    l++;
                  }
                }
              }
              q++;
            }
          } else {
            var l = 0;
            while (l<servers.tanks[data.gamemode][data.room].sockets.length) {
              if (servers.tanks[data.gamemode][data.room].sockets[l].username === data.username) {
                servers.tanks[data.gamemode][data.room].sockets[l].off('disconnect');
                servers.tanks[data.gamemode][data.room].sockets.splice(l, 1);
                socket.gamemode = data.gamemode;
                socket.room = data.room;
                servers.tanks[data.gamemode][data.room+q].sockets.push(socket);
              }
              l++;
            }
          }
          servers.tanks[data.gamemode][data.room].sockets.push(socket);
        }
        if (data.gamemode == 'ffa') {
          if (socket.room == undefined) {
            socket.gamemode = 'ffa';
            if (data.mode == 'quick-join') {
              var l = 0, ip;
              while (l < pvpRooms.length) {
                if (pvpRooms[l] < 5) {
                  ip = 'quick-join' + l;
                  pvpRooms[l] += 1;
                  socket.pvpRoom = l;
                  socket.room = ip;
                  if (pvpRooms[l] == 1) {
                    console.log('[MULTI] => New FFA server created ' + ip);
                    servers.tanks.ffa[ip] = new HostTanks(ip, 'FFA');
                  }
                  servers.tanks.ffa[ip].sockets.push(socket);
                  l = pvpRooms.length;
                }
                l++;
              }
              if (socket.pvpRoom == undefined) {
                socket.destroy();
                sockets = sockets.filter(s => s !== socket);
                return;
              }
            } else {
              if (servers.tanks.ffa[data.room] == undefined) {
                if (data.room == undefined) {
                  return;
                }
                console.log('[MULTI] => New FFA room created ' + data.room);
                servers.tanks.ffa[data.room] = new HostTanks(data.room, 'FFA');
              }
              servers.tanks.ffa[data.room].sockets.push(socket);
              socket.room = data.room;
            }
          } else {
            if (data.event == 'joinerupdate') {
              try {
                servers.tanks.ffa[socket.room].joinerupdate(data);
              } catch(e) {}
            }
            if (data.event == 'joinerjoin') {
              try {
                servers.tanks.ffa[socket.room].joinerjoin(data.data);
              } catch(e) {}
            }
          }
        } else if (data.gamemode == 'duels') {
          if (socket.room === undefined) {
            socket.gamemode = 'duels';
            if (data.mode == 'quick-join') {
              var l = 0, ip;
              var available = [], empty = [];
              while (l < duelRooms.length) {
                if (duelRooms[l] == 1) {
                  available.push({
                    id: l,
                    players: duelRooms[l],
                  })
                } else {
                  empty.push({
                    id: l,
                    players: duelRooms[l],
                  })
                }
                l++;
              }
              if (available.length != 0) {
                var num = Math.floor(Math.random() * available.length);
                ip = 'pvp' + available[num].id;
                duelRooms[num]++;
                socket.duelRoom = num;
                socket.room = ip;
                servers.tanks.duels[ip].sockets.push(socket);
              } else if (empty.length != 0) {
                var num = empty[0].id;
                ip = 'pvp' + num;
                duelRooms[num]++;
                socket.duelRoom = num;
                socket.room = ip;
                if (duelRooms[num] == 1) {
                  console.log('[SERVER] => New DUELS room created ' + ip);
                  servers.tanks.duels[ip] = new HostTanks(ip, 'DUELS');
                }
                servers.tanks.duels[ip].sockets.push(socket);
              } else {
                console.log('[SERVER] => Warning!!! The Duels Servers are all full!');
              }
              if (socket.duelRoom == undefined) {
                socket.destroy();
                sockets = sockets.filter(s => s !== socket);
                return;
              }
            } else {
              if (servers.tanks.duels[data.room] == undefined) {
                console.log('[MULTI] => New DUELS room created ' + data.room);
                servers.tanks.duels[data.room] = new HostTanks(data.room, 'DUELS');
              }
              if (servers.tanks.duels[data.room].pt.length != 2) {
                servers.tanks.duels[data.room].sockets.push(socket);
                socket.room = data.room;
              } else {
                socket.send(JSON.stringify({
                  'error': true,
                  'msg': 'Room Full :(',
                }));
              }
            }
          } else {
            if (data.event == 'joinerupdate') {
              servers.tanks.duels[socket.room].joinerupdate(data);
            }
            if (data.event == 'joinerjoin') {
              servers.tanks.duels[socket.room].joinerjoin(data.data);
            }
          }
        } else if (data.gamemode == 'defense') {
          if (socket.room === undefined) {
            socket.gamemode = 'defense';
            if (data.mode == 'quick-join') {
              var l = 0, ip;
              while (l < defenseRooms.length) {
                if (defenseRooms[l] < 5) {
                  ip = 'defense-' + l;
                  defenseRooms[l] += 1;
                  socket.defenseRoom = l;
                  socket.room = ip;
                  if (defenseRooms[l] == 1) {
                    console.log('[MULTI] => New DEFENSE room created ' + ip);
                    servers.tanks.defense[ip] = new HostTanks(ip, 'DEFENSE');
                  }
                  servers.tanks.defense[ip].sockets.push(socket);
                  l = defenseRooms.length;
                }
                l++;
              }
              if (socket.defenseRoom == undefined) {
                socket.destroy();
                sockets = sockets.filter(s => s !== socket);
                return;
              }
            } else {
              if (servers.tanks.defense[data.room] == undefined) {
                console.log('[MULTI] => New DEFENSE room created ' + data.room);
                servers.tanks.defense[data.room] = new HostTanks(data.room, 'DEFENSE');
              }
              if (servers.tanks.defense[data.room].pt.length != 2) {
                servers.tanks.defense[data.room].sockets.push(socket);
                socket.room = data.room;
              } else {
                socket.send(JSON.stringify({
                  'error': true,
                  'msg': 'Room Full :(',
                }));
              }
            }
          } else {
            if (data.event == 'joinerupdate') {
              servers.tanks.defense[socket.room].joinerupdate(data);
            }
            if (data.event == 'joinerjoin') {
              servers.tanks.defense[socket.room].joinerjoin(data.data);
            }
          }
        } else if (data.gamemode == 'tdm') {
          if (socket.room === undefined) {
            socket.gamemode = 'tdm';
            if (data.mode == 'quick-join') {
              var l = 0, ip;
              var available = [], empty = [];
              while (l < tdmRooms.length) {
                if (tdmRooms[l] != 0 && tdmRooms[l] != 4) {
                  available.push({
                    id: l,
                    players: tdmRooms[l],
                  })
                } else if (tdmRooms[l] == 0) {
                  empty.push({
                    id: l,
                    players: tdmRooms[l],
                  })
                }
                l++;
              }
              if (available.length != 0) {
                var num = Math.floor(Math.random() * available.length);
                ip = 'tdm-' + available[num].id;
                tdmRooms[num]++;
                socket.tdmRoom = num;
                socket.room = ip;
                servers.tanks.tdm[ip].sockets.push(socket);
              } else if (empty.length != 0) {
                var num = empty[0].id;
                ip = 'tdm-' + num;
                tdmRooms[num]++;
                socket.tdmRoom = num;
                socket.room = ip;
                if (tdmRooms[num] == 1) {
                  console.log('[SERVER] => New TDM room created ' + ip);
                  servers.tanks.tdm[ip] = new HostTanks(ip, 'TDM');
                }
                servers.tanks.tdm[ip].sockets.push(socket);
              } else {
                console.log('[SERVER] => Warning!!! The TDM Servers are all full!');
              }
              if (socket.tdmRoom == undefined) {
                socket.destroy();
                sockets = sockets.filter(s => s !== socket);
                return;
              }
            } else {
              if (servers.tanks.tdm[data.room] == undefined) {
                console.log('[MULTI] => New TDM room created ' + data.room);
                servers.tanks.tdm[data.room] = new HostTanks(data.room, 'TDM');
              }
              if (servers.tanks.tdm[data.room].pt.length != 2) {
                servers.tanks.tdm[data.room].sockets.push(socket);
                socket.room = data.room;
              } else {
                socket.send(JSON.stringify({
                  'error': true,
                  'msg': 'Room Full :(',
                }));
              }
            }
          } else {
            if (data.event == 'joinerupdate') {
              servers.tanks.tdm[socket.room].joinerupdate(data);
            }
            if (data.event == 'joinerjoin') {
              servers.tanks.tdm[socket.room].joinerjoin(data.data);
            }
          }
        }
      }
    } else if (data.operation == 'status') {
      /*var l = 0,
        valid = false;
      var len = sessionTokens.length;
      while (l < len) {
        if (sessionTokens[l].username == data.username) {
          if (data.token == sessionTokens[l].token) {
            valid = true;
          }
        }
        l++;
      }
      if (!valid) return;*/ // Security Thing 
      if (data.task == 'get') {
        if (data.target == '*') {
          var allOnline = Object.getOwnPropertyNames(status);
          var response = [];
          var l = 0;
          while (l < allOnline.length) {
            if (typeof status[allOnline[l]] == 'object' && status[allOnline[l]].status != undefined) {
              response.push({
                name: allOnline[l],
                status: status[allOnline[l]].status,
                message: status[allOnline[l]].message
              });
            }
            l++;
          }
          socket.send(JSON.stringify({
            data: response,
          }));
        } else {
          if (typeof status[data.target] == 'object') {
            if (status[data.target].status == undefined) {
              socket.send(JSON.stringify({
                status: 'offline',
                message: '',
              }));
            } else {
              socket.send(JSON.stringify({
                status: status[data.target].status,
                message: status[data.target].message,
              }));
            }
          } else {
            socket.send(JSON.stringify({
              status: 'offline',
              message: '',
            }));
          }
        }
      }
      if (data.task == 'set') {
        if (status[data.username] != undefined) {
          clearTimeout(status[data.username].interval);
        }
        status[data.username] = {
          status: data.status,
          message: data.message,
        }
        status[data.username].interval = setTimeout(function(username) {
          console.log('[ONLINE] => ' + username + ' is inactive.')
          status[username].status = undefined;
          status[username].message = undefined;
        }, 30000, data.username);
        socket.send(JSON.stringify({
          success: true,
        }));
      }
      if (data.task == 'admin-ban') {
        if (data.password == 'e') {
          if (data.victim != 'admin' && data.victim != 'bradley') {
            bans.push(data.victim);
            console.log('[BANS] => New ban added. Total Bans => ' + bans);
            var l = 0;
            while (l < sockets.length) {
              if (sockets[l].username == data.victim) {
                sockets[l].destroy();
              }
              l++;
            }
          }
        }
      }
      if (data.task == 'admin-kick') {
        if (data.password == 'e') {
          if (data.victim != 'admin' && data.victim != 'bradley') {
            console.log('[BANS] Kicking ' + data.victim + '. Command issued by ' + data.username);
            var l = 0;
            while (l < sockets.length) {
              if (sockets[l].username == data.victim) {
                sockets[l].destroy();
              }
              l++;
            }
          }
        }
      }
      if (data.task == 'admin-modify' && data.password == 'MONEYZ') {
        var values = [];
        var item = await db.findOne({
          username: data.target,
        });
        item.playerdata = JSON.parse(item.playerdata);
        if (data.mode == 'coins') {
          item.playerdata['increedible-tanks'].coins += data.num;
        } else if (data.mode == 'crates') {
          item.playerdata['increedible-tanks'].crates += data.num;
        }
        item.playerdata = JSON.stringify(item.playerdata);
        db.updateOne({
          username: item.username
        }, {
          $set: {
            playerdata: item.playerdata,
          }
        });
        socket.send(JSON.stringify({
          success: true,
        }));
      }
      if (data.task == 'admin-servers') {
        var l = 0,
          serversOnline = 0,
          serverData = [],
          g = Object.values(servers);
        while (l < g.length) {
          if (g[l] != undefined) {
            serversOnline++;
            var q = 0,
              players = [];
            while (q < g[l].sockets.length) {
              players.push(g[l].sockets[q].username);
              q++;
            }
            serverData.push({
              serverRoom: g[l].channelname,
              playerNum: g[l].sockets.length,
              players: players,
            });
          }
          l++;
        }
        socket.send(JSON.stringify({
          servers: serverData,
          serversOnline: serversOnline,
        }));
      }
      if (data.task == 'admin-crash') {
        var l = 0,
          g = Object.values(servers);
        while (l < g.length) {
          if (g[l].channelname == data.channel) {
            var q = 0;
            while (q < g[l].sockets.length) {
              g[l].sockets[q].destroy();
              q++;
            }
          }
          l++;
        }
      }
      /*if (data.task == 'admin-kick') {
        var l = 0, g = Object.values(servers);
        while (l<g.length) {
          var q = 0;
          while (q<g[l].sockets.length) {
            if (g[l].sockets[q].username == data.victim) {
              g[l].sockets[q].destroy();
            }
            q++;
          }
          g++;
        }
      }*/
    } else if (data.operation == 'chat') {
      chatSockets.forEach(function(s) {
        if (data.message.private) {
          if (s.username == data.message.to) {
            s.send(JSON.stringify({
              type: 'chat',
              message: data.message.message,
              send: data.message.send,
              timestamp: data.message.timestamp,
              server: data.server,
              private: true,
            }));
          }
        } else {
          if (s != socket) {
            s.send(JSON.stringify({
              type: 'chat',
              message: data.message.message,
              send: data.message.send,
              timestamp: data.message.timestamp,
              server: data.server,
            }));
          }
        }
      })
    } else if (data.operation == 'chat-servers') {
      if (data.task == 'connect') {
        // connects chat socket to mainframe to get new messages live
        chatSockets.push(socket);
      } else if (data.task == 'new') {
        var values = [];
        var item = await chat_db.findOne({
          name: data.name,
        });
        if (item != null) {
          socket.send(JSON.stringify({
            success: false,
            message: 'This Chat Server Already Exists!',
          }));
          return;
        }
        var success = await chat_db.insertOne({
          name: data.name,
          messages: "[]",
          members: "[]",
          owner: data.username,
        });
        var values = [];
        var account = await db.findOne({
          username: data.username,
        });
        var array = JSON.parse(account.chat);
        array.push(data.name);
        account.chat = JSON.stringify(array);
        db.updateOne({
          username: account.username
        }, {
          $set: {
            chat: account.chat
          }
        });
        socket.send(JSON.stringify({
          success: true,
        }));
        return;
      }
      var values = [];
      var item = await chat_db.findOne({
        name: data.name,
      });
      if (item == undefined) return;
      if (!JSON.parse(item.members).includes(data.username) && JSON.parse(item.members)[0] != '*') {
        if (item.elite) {
          if (!JSON.parse(item.elite).includes(data.username)) {
            if (item.owner != data.username) {
              return;
            }
          }
        } else {
          item.elite = "[]";
        }
      }
      if (data.task == 'get') {
        socket.send(JSON.stringify({
          type: 'chat-servers-return',
          data: item.messages,
        }));
      } else if (data.task == 'get-people') {
        socket.send(JSON.stringify({
          type: 'chat-servers-return',
          data: {
            members: item.members,
            elite: item.elite,
            owner: item.owner,
          }
        }));
      } else if (data.task == 'add-message') {
        item.messages = JSON.stringify(JSON.parse(item.messages).concat(data.addition));
        chat_db.updateOne({
          name: item.name
        }, {
          $set: {
            messages: item.messages
          }
        });
        socket.send(JSON.stringify({
          success: true,
        }));
      } else if (data.task == 'delete-message') {
        if (data.username != data.removal.send) { // cancel delete if they do not have permissions
          if (item.owner != data.username) {
            return;
          }
          if (!JSON.parse(item.elite).includes(data.username)) {
            return;
          }
        }
        item.messages = JSON.parse(item.messages);
        var l = 0;
        while (l < item.messages.length) {
          if (item.messages[l].timestamp == data.removal.timestamp) {
            if (item.messages[l].message == data.removal.message) {
              if (item.messages[l].send == data.removal.send) {
                item.messages.splice(l, 1);
              }
            }
          }
          l++;
        }
        item.messages = JSON.stringify(item.messages);
        await chat_db.updateOne({
          name: item.name
        }, {
          $set: {
            messages: item.messages
          }
        });
        socket.send(JSON.stringify({
          success: true,
        }));
      }
      if (item.elite) {
        if (!JSON.parse(item.elite).includes(data.username) && data.username != item.owner && data.task != 'unshare') return;
      } else {
        item.elite = "[]";
        if (!JSON.parse(item.elite).includes(data.username) && data.username != item.owner && data.task != 'unshare') return;
      }
      if (data.task == 'share') {
        var values = [];
        var account = await db.findOne({
          username: data.new_member,
        });
        if (account == null) {
          socket.send(JSON.stringify({
            success: false,
            message: 'Account does not exist!',
          }));
          return;
        }
        var array = JSON.parse(account.chat);
        array.push(data.name);
        account.chat = JSON.stringify(array);
        db.updateOne({
          username: account.username
        }, {
          $set: {
            chat: account.chat
          }
        });
        item.members = JSON.parse(item.members);
        item.members.push(data.new_member);
        item.members = JSON.stringify(item.members);
        chat_db.updateOne({
          name: item.name
        }, {
          $set: {
            members: item.members
          }
        });
        socket.send(JSON.stringify({
          success: true,
        }));
      } else if (data.task == 'unshare') {
        if (data.remove_member != data.username) {
          if (!JSON.parse(item.elite).includes(data.username) && data.username != item.owner) return;
        }
        item.members = JSON.parse(item.members);
        var l = 0;
        while (l < item.members.length) {
          if (item.members[l] == data.remove_member) {
            item.members.splice(l, 1);
          }
          l++;
        }
        item.members = JSON.stringify(item.members);
        chat_db.updateOne({
          name: item.name
        }, {
          $set: {
            members: item.members
          }
        });
        var values = [];
        var account = await db.findOne({
          username: data.username,
        });
        if (account == null) {
          socket.send(JSON.stringify({
            success: false,
            message: 'This Account Does Not Exist!',
          }));
          return;
        }
        var array = JSON.parse(account.chat);
        var q = 0,
          found = false;
        while (q < array.length) {
          if (array[q] == item.name) {
            found = true;
            array.splice(q, 1);
          }
          q++;
        }
        if (!found) {
          socket.send(JSON.stringify({
            success: null,
            message: 'The Account Did Not Have Access To The Server',
          }));
          return;
        }
        account.chat = JSON.stringify(array);
        db.updateOne({
          username: account.username
        }, {
          $set: {
            chat: account.chat
          }
        });
        socket.send(JSON.stringify({
          success: true,
        }));
      }
      if (item.owner != data.username) return;
      if (data.task == 'reset') {
        chat_db.updateOne({
          name: item.name
        }, {
          $set: {
            messages: '[]'
          }
        });
      } else if (data.task == 'add-elite') {
        // add admin to chat server
      } else if (data.task == 'remove-elite') {
        // remove admin from chat server
      } else if (data.task == 'delete') {
        var targets = JSON.parse(item.members);
        chat_db.deleteOne({
          name: data.name,
        });
        var l = 0;
        while (l < targets.length) {
          var values = [];
          var account = await db.findOne({
            username: targets[l],
          });
          if (account != null) {
            var array = JSON.parse(account.chat);
            var q = 0;
            while (q < array.length) {
              if (array[q] == item.name) {
                array.splice(q, 1);
              }
              q++;
            }
            account.chat = JSON.stringify(array);
            db.updateOne({
              username: data.username
            }, {
              $set: {
                chat: account.chat
              }
            });
          }
          l++;
        }
        socket.send(JSON.stringify({
          success: true,
        }));
      }
    } else if (data.operation == 'logs') {
      if (data.task == 'get') {
        logSockets.push(socket);
        socket.send(JSON.stringify({
          event: 'logs',
          data: {
            logs: logs,
          }
        }));
      } else if (data.task == 'reboot') {
        exec('sudo pm2 start web');
      }
    }
  });
  socket.on('close', function(code, reason) {
    if (socket.swapped) {
      return;
    }
    if (socket.room != undefined && socket.tagRoom == undefined) {
      servers.tanks[socket.gamemode][socket.room].disconnect(socket.username);
    } else if (socket.room != undefined && socket.tagRoom != undefined) {
      servers.tag[socket.room].disconnect(socket.username);
    }
    if (socket.pvpRoom != undefined) {
      pvpRooms[socket.pvpRoom] -= 1;
    }
    if (socket.duelRoom != undefined) {
      duelRooms[socket.duelRoom] -= 1;
    }
    if (socket.tdmRoom != undefined) {
      tdmRooms[socket.tdmRoom] -= 1;
    }
    if (socket.defenseRoom != undefined) {
      defenseRooms[socket.tdmRoom] -= 1;
    }
    if (socket.tagRoom != undefined) {
      tagRooms[socket.tagRoom] -= 1;
    }
    sockets = sockets.filter(s => s !== socket);
  });
});
class TagBot {
  constructor(x, y) {
    this.canChangeDirection = true;
    this.up = false;
    this.down = false;
    this.left = false;
    this.right = false;
    this.mode = 'hunt'; // run or hunt or random
    this.goal = 'hunt';
    this.player = {
      x: x,
      y: y,
      infected: false,
    }
    this.reactionTimeMin = 250;
    this.reactionTimeMax = 500;
    // delay in ms that bot will take
  }
  detect(data) {
    if (this.goal == 'run') {
      this.identifyThreat(data);
    } else if (this.goal == 'hunt') {
      this.findTarget(data);
    }
  }
  border(x, y) {
    if (x < 0 || y < 0 || x + 20 > 1500 || y + 20 > 1500) {
      return false;
    }
    return true;
  }
  findTarget(gameData) {
    var l = 0,
      possible_targets = [];
    while (l < gameData.players.length) {
      if (!gameData.players[l].infected) {
        if (Math.abs(gameData.players[l].x - this.player.x) <= 250) {
          if (Math.abs(gameData.players[l].y - this.player.y) <= 250) {
            possible_targets.push({
              id: l,
              distance: Math.sqrt((gameData.players[l].x - this.player.x) * (gameData.players[l].x - this.player.x) + (gameData.players[l].y - this.player.y) * (gameData.players[l].y - this.player.y))
            });
          }
        }
      }
      l++;
    }
    if (possible_targets.length != 0) {
      possible_targets.sort(function(a, b) {
        return a.distance - b.distance;
      }); // find lowest distance target
      this.target = possible_targets[0].id;
      this.mode = 'hunt';
    } else {
      this.target = undefined;
      this.mode = 'random';
    }
  }
  identifyThreat(gameData) {
    var l = 0,
      possible_targets = [];
    while (l < gameData.players.length) {
      if (gameData.players[l].infected) {
        if (Math.abs(gameData.players[l].x - this.player.x) <= 250) {
          if (Math.abs(gameData.players[l].y - this.player.y) <= 250) {
            possible_targets.push({
              id: l,
              distance: Math.sqrt((gameData.players[l].x - this.player.x) * (gameData.players[l].x - this.player.x) + (gameData.players[l].y - this.player.y) * (gameData.players[l].y - this.player.y))
            });
          }
        }
      }
      l++;
    }
    if (possible_targets.length != 0) {
      possible_targets.sort(function(a, b) {
        return a.distance - b.distance;
      }); // find lowest distance target
      this.target = possible_targets[0].id;
      this.mode = 'run';
    } else {
      this.target = undefined;
      this.mode = 'random';
    }
  }
  moveCalc(gameData) {
    var up, down, left, right;
    if (this.mode == 'random') {
      up = this.up;
      down = this.down;
      left = this.left;
      right = this.right;
      if (this.canChangeDirection) {
        this.up = Math.random() < 0.5;
        this.down = Math.random() < 0.5;
        this.left = Math.random() < 0.5;
        this.right = Math.random() < 0.5;
        this.canChangeDirection = false;
        setTimeout(function(bot) {
          try {
            bot.canChangeDirection = true;
          } catch (e) {}
        }, Math.floor(Math.random() * (10000 - 500 + 1) + 500), this);
      }
    } else if (this.mode == 'hunt' && this.target != undefined) {
      var target = gameData.players[this.target];
      var xdistance = this.player.x - target.x;
      var ydistance = this.player.y - target.y;
      if (xdistance < 0) {
        right = true;
      } else {
        left = true;
      }
      if (ydistance < 0) {
        down = true;
      } else {
        up = true;
      }
    } else if (this.mode == 'run' && this.target != undefined) {
      var target = gameData.players[this.target];
      var xdistance = this.player.x - target.x;
      var ydistance = this.player.y - target.y;
      if (xdistance > 0) {
        right = true;
      } else {
        left = true;
      }
      if (ydistance > 0) {
        down = true;
      } else {
        up = true;
      }
    }
    if (this.player.x < 100 || this.player.y < 100 || this.player.x + 20 > 4900 || this.player.y + 20 > 4900) {
      up = left;
      down = right
      left = down;
      right = up;
    }
    return {
      up: up,
      down: down,
      left: left,
      right: right,
    };
  }
  move(host, id) {
    if (!this.player.infected) {
      this.goal = 'run';
    } else if (this.player.infected) {
      this.goal = 'hunt';
    }
    var instructions = this.moveCalc({
      players: host.players
    });
    setTimeout(function(bot, host, id) {
      try {
        if (instructions.up && bot.border(bot.player.x, bot.player.y - 2)) {
          bot.player.y -= 2;
        }
        if (instructions.down && bot.border(bot.player.x, bot.player.y + 2)) {
          bot.player.y += 2;
        }
        if (instructions.left && bot.border(bot.player.x - 2, bot.player.y)) {
          bot.player.x -= 2;
        }
        if (instructions.right && bot.border(bot.player.x + 2, bot.player.y)) {
          bot.player.x += 2;
        }
        host.players[id].x = bot.player.x;
        host.players[id].y = bot.player.y;
      } catch (e) {};
    }, Math.floor(Math.random() * (this.reactionTimeMax - this.reactionTimeMin + 1) + this.reactionTimeMin), this, host, id);
  }
  override(host, id, data) {
    if (data.x) {
      host.players[id].x = data.x;
      this.player.x = data.x;
    }
    if (data.y) {
      host.players[id].y = data.y;
      this.player.y = data.y;
    }
    if (data.infected) {
      host.players[id].infected = true;
      this.player.infected = data.infected
    }
  }
}
class HostTag {
  control(channelname) {
    this.channelname = channelname;
    this.players = [];
    this.sockets = [];
    this.infected = 0;
    this.notInfected = 0;
    this.timer = Infinity;
    this.i = [];
    this.message = 'Waiting for Players';
    this.gamePhase = 0;
    this.i.push(setInterval(function(host) {
      host.send();
    }, 200, this));
    this.i.push(setInterval(function(host) {
      host.gameRunner(host);
    }, 30, this));
    this.i.push(setInterval(function(host) {
      var l = 0;
      while (l < host.players.length) {
        if (host.players[l].isBot) {
          host.players[l].bot.move(host, l);
        }
        l++;
      }
    }, 10, this));
    this.i.push(setInterval(function(host) {
      var l = 0;
      while (l < host.players.length) {
        if (host.players[l].isBot) {
          host.players[l].bot.detect({
            players: host.players
          });
        }
        l++;
      }
    }, 200, this));
    this.five = false;
    this.ten = false;
    this.twenty = false;
    this.bots = true; // add bots to the game
  }
  count() {
    if (this.timer > 1) {
      this.timer--;
    } else {
      clearInterval(this.countdown);
      if (this.gamePhase == 0) {
        this.startGame();
      } else {
        this.endGame();
      }
    }
  }
  endGame() {
    this.gamePhase = 2;
    setTimeout(function(host) {
      var l = 0;
      while (l < host.sockets.length) {
        host.sockets[l].destroy();
        l++;
      }
      var l = 0;
      while (l < host.i) {
        clearInterval(host.i[l]);
        l++;
      }
      clearInterval(host.countdown);
      servers.tag[host.channelname] = undefined;
      console.log('[MULTI] => Server shutdown - ' + host.channelname);
    }, 30000, this);
  }
  startGame() {
    this.gamePhase = 1;
    var l = 0;
    var infected = Math.floor(Math.random() * (this.players.length));
    while (l < this.players.length) {
      if (this.players[l].isBot) {
        if (infected == l) {
          this.players[l].bot.override(this, l, {
            x: Math.floor(Math.random() * (1490 - 10 + 1) + 10),
            y: Math.floor(Math.random() * (1490 - 10 + 1) + 10),
            infected: true,
          })
        } else {
          this.players[l].bot.override(this, l, {
            x: Math.floor(Math.random() * (1490 - 10 + 1) + 10),
            y: Math.floor(Math.random() * (1490 - 10 + 1) + 10),
            infected: false,
          });
        }
      } else {
        var q = 0;
        while (q < this.sockets.length) {
          if (this.sockets[q].username == this.players[l].username) {
            if (q != infected) {
              this.sockets[q].send(JSON.stringify({
                event: 'override',
                x: Math.floor(Math.random() * (1490 - 10 + 1) + 10),
                y: Math.floor(Math.random() * (1490 - 10 + 1) + 10),
                infected: false,
              }));
            } else {
              this.sockets[q].send(JSON.stringify({
                event: 'override',
                x: Math.floor(Math.random() * (1490 - 10 + 1) + 10),
                y: Math.floor(Math.random() * (1490 - 10 + 1) + 10),
                infected: true,
              }));
              this.players[l].infected = true;
            }
          }
          q++;
        }
      }
      l++;
    }
    this.timer = 60 * 3;
    this.countdown = setInterval(function(host) {
      host.count();
    }, 1000, this);
  }
  gameRunner() {
    if (this.gamePhase == 0) {
      if (this.players.length >= 20 && !this.twenty) {
        this.twenty = true;
        this.timer = 10;
        clearInterval(this.countdown);
        this.countdown = setInterval(function(host) {
          host.count();
        }, 1000, this);
      } else if (this.players.length >= 10 && !this.ten) {
        this.ten = true;
        this.timer = 60;
        clearInterval(this.countdown);
        this.countdown = setInterval(function(host) {
          host.count();
        }, 1000, this);
      } else if (this.players.length >= 5 && !this.five) {
        this.five = true;
        this.timer = 100;
        clearInterval(this.countdown);
        this.countdown = setInterval(function(host) {
          host.count();
        }, 1000, this);
      }
      if (this.bots && this.players.length < 50) {
        if (true) {
          var x = Math.floor(Math.random() * (1490 - 10 + 1) + 10);
          var y = Math.floor(Math.random() * (1490 - 10 + 1) + 10);
          var bot = new TagBot(x, y);
          this.players.push({
            x: x,
            y: y,
            infected: false,
            isBot: true,
            bot: bot,
          });
        }
      }
      if (this.timer != Infinity) {
        this.message = 'Game Starting in ' + this.timer + ' second(s)';
        this.bottom_message = 'Avoid Red at all costs!'
      }
    } else if (this.gamePhase == 1) {
      var l = 0;
      this.infected = 0;
      this.notInfected = 0;
      while (l < this.players.length) {
        if (this.players[l].infected) {
          this.infected++;
        } else {
          this.notInfected++;
        }
        if (this.tagger(this.players[l].x, this.players[l].y) && !this.players[l].infected) {
          this.players[l].infected = true;
          var q = 0;
          while (q < this.sockets.length) {
            if (this.players[l].isBot) {
              this.players[l].bot.override(this, l, {
                //x: Math.floor(Math.random()*(1490-10+1)+10),
                //y: Math.floor(Math.random()*(1490-10+1)+10),
                infected: true,
              });
            } else {
              if (this.sockets[q].username == this.players[l].username) {
                this.sockets[q].send(JSON.stringify({
                  event: 'override',
                  //x: Math.floor(Math.random()*(1490-10+1)+10),
                  //y: Math.floor(Math.random()*(1490-10+1)+10),
                  infected: true,
                }));
              }
            }
            q++;
          }
        }
        l++;
      }
      if (this.notInfected == 0) {
        this.gamePhase = 2;
      }
      if (this.infected == 1 && this.notInfected == 1) {
        this.message = this.infected + ' Hunter and ' + this.notInfected + ' Hider';
      } else if (this.infected == 1 && this.notInfected != 1) {
        this.message = this.infected + ' Hunter and ' + this.notInfected + ' Hiders';
      } else if (this.infected != 1 && this.notInfected == 1) {
        this.message = this.infected + ' Hunters and ' + this.notInfected + ' Hider';
      } else if (this.infected != 1 && this.notInfected != 1) {
        this.message = this.infected + ' Hunters and ' + this.notInfected + ' Hiders';
      }
      this.bottom_message = 'Dont Get Tagged for ' + this.timer + ' seconds!';
    } else if (this.gamePhase == 2) {
      if (this.notInfected != 0) {
        this.message = 'NonInfected Players Win! \n Server Shutting Down in ' + this.timer + ' secs';
      } else {
        this.message = 'Infected Players Win! \n Server Shutting Down';
      }
    }
  }
  joinerjoin(data) {
    this.players.push(data.data);
    var l = 0;
    while (l < this.sockets.length) {
      if (this.sockets[l].username == data.data.username) {
        this.sockets[l].send(JSON.stringify({
          event: 'override',
          x: Math.floor(Math.random() * (1490 - 10 + 1) + 10),
          y: Math.floor(Math.random() * (1490 - 10 + 1) + 10),
          infected: false,
        }));
      }
      l++;
    }
  }
  joinerupdate(data) {
    var l = 0;
    while (l < this.players.length) {
      if (data.username == this.players[l].username) {
        this.players[l].x = data.x;
        this.players[l].y = data.y;
      }
      l++;
    }
  }
  send() {
    var l = 0;
    while (l < this.sockets.length) {
      this.sockets[l].send(JSON.stringify({
        event: 'hostupdate',
        players: this.players,
        message: this.message,
        bottom_message: this.bottom_message,
        popup: this.popup,
      }));
      l++;
    }
    // send data to bots
    /*if (this.bots) {
      var l = 0;
      while (l<this.players.length) {
        if (this.players[l].isBot) {
          this.players[l].bot.gameData = {players: this.players};
        }
        l++;
      }
    }*/
  }
  disconnect(username) {
    var l = 0;
    while (l < this.players.length) {
      if (this.players[l].username == username) {
        this.players.splice(l, 1);
      }
      l++;
    }
    var l = 0;
    while (l < this.sockets.length) {
      if (this.sockets[l].username == username) {
        this.sockets.splice(l, 1);
      }
      l++;
    }
    if (this.sockets.length == 0) {
      var l = 0;
      while (l < this.i) {
        clearInterval(this.i[l]);
        l++;
      }
      clearInterval(this.countdown);
      servers.tag[this.channelname] = undefined;
      console.log('[MULTI] => Server shutdown - ' + this.channelname);
    }
  }
  tagger(x, y) {
    var l = 0;
    while (l < this.players.length) {
      if (this.players[l].infected) {
        if ((x + 20 >= this.players[l].x && x + 20 <= this.players[l].x + 20) || (x >= this.players[l].x && x <= this.players[l].x + 20)) {
          if ((y + 20 >= this.players[l].y && y + 20 <= this.players[l].y + 20) || (y >= this.players[l].y && y <= this.players[l].y + 20)) {
            return true;
          }
        }
      }
      l++;
    }
    return false;
  }
}
const Timeout = setTimeout(function(){}, 0).constructor;
function isTimer(t) { return t instanceof Timeout; }
function replacer(key, value) {
  if (isTimer(value)) {
    return undefined;
  } else if (typeof value == 'function') {
    return undefined;
  } else {
    if (['blockId', 'isScaffolding', 'isLoot', 'isInvincible', 'firedRecent', 'isBuilder', 'canTakeThermalDamage', 'isThermal', 'class', 'kills', 't', 'i', 'immune', 'canChangeInvisStatus', 'canInvis', 'team', 'addx', 'addy', 'xm', 'ym', 'x1', 'y1', 'x2', 'y2', 'dx', 'dy', 'dr', 'd', 'sgn', 'x_1', 'x_2', 'y_1', 'y_2', 'counter', 'id', 'damage', 'ownerRank'].includes(key)) {
      return undefined;
    } else {
      return value;
    }
  }
  //{"particleFrame":2,"rot":178,"team":"cs641311","addx":176,"addy":1480,"type":"condensed_bullet","xm":-0.1590909090909091,"ym":-7,"x1":20,"y1":880,"x2":-20,"y2":-880,"dx":-40,"dy":-1760,"dr":1760.4544867732309,"d":0,"sgn":-1,"x_1":0.7952491873652955,"x_2":-0.7952491873652955,"y_1":34.990964244073,"y_2":-34.990964244073,"x":152.37520535808554,"y":440.509035755927,"counter":0,"id":51,"startx":175.2047508126347,"starty":1445.009035755927,"damage":74.01887934180036,"ownerRank":17}
}

class HostTanks {
  constructor(channelname, gamemode) {
    this.spawn = {
      x: 0,
      y: 0,
    }
    this.channelname = channelname;
    this.gamemode = gamemode;
    this.gamestate = 0; // 0 -> Waiting for Players, 1 -> Game started!
    this.s = [];
    this.b = [];
    this.pt = [];
    this.ai = [];
    this.scaffolding = [];
    this.sockets = [];
    this.i = [];
    this.t = [];
    this.update = {
      block: true,
      bullet: true,
      tank: true,
      scaffolding: true,
      blocks: [], // optimization only enabled for blocks due to high object count
      /*{
        operation: null, // 'full', 'update', 'delete' // full update every 5 seconds???
        id: null, // id of editted object (only for update and delete);
        updateTo: null, // what to update the blok to (full, update and delete)
      },*/
    }
    const OUTPUT_FPS = 120; // rate limit fps
    this.send = this.send.bind(this); // lock the function
    this.i.push(setInterval(this.send, 1000/OUTPUT_FPS));
    /*this.spam = function() {
      setImmediate(function() {
        this.send();
        setImmediate(this.spam);
      }.bind(this));
    }.bind(this);
    this.spam();*/
    this.fpsC = 0;
    this.i.push(setInterval(function(host) {
      this.fps = this.fpsC;
      this.fpsC = 0;
    }.bind(this), 1000));
    if (this.gamemode == 'DUELS' || this.gamemode == 'RAID' || this.gamemode == 'DEFENSE' || this.gamemode == 'TDM') {
      this.i.push(setInterval(function(host) {
        host.gameRunner(host);
      }, 30, this));
    }
    this.i.push(setInterval(function(host) {
      var l = 0;
      while (l < host.s.length) {
        host.s[l].update(host, l);
        l++;
      }
      var l = 0;
      while (l<host.pt.length) {
        if (host.pt[l].pushback != 0) {
          host.pt[l].pushback++;
        }
        l++;
      }
    }, 1000/40, this)); // Game Ticks (for now)
    this.i.push(setInterval(function(host) {
      var l = 0;
      while (l < host.pt.length) {
        var results = thermal_check(host.pt[l].x, host.pt[l].y, host.pt[l].username, host);
        if (results[0] && host.pt[l].canTakeThermalDamage && !host.pt[l].ded && results[1] != host.pt[l].team) {
          host.pt[l].canTakeThermalDamage = false;
          setTimeout(function(host, username) {
            var l = 0;
            var len = host.pt.length;
            while (l < len) {
              if (host.pt[l].username == username) {
                host.pt[l].canTakeThermalDamage = true;
              }
              l++;
            }
          }, 1000, host, host.pt[l].username)
          host.pt[l].health -= 20;
          host.pt[l].invis = false;
          host.pt[l].damagedRecent = true;
          clearTimeout(host[host.pt[l].username + '_damageTimer']);
          host[host.pt[l].username + '_damageTimer'] = setTimeout(function(username, host) {
            var l = 0,
              id;
            while (l < host.pt.length) {
              if (host.pt[l].username == username) {
                id = l;
              }
              l++;
            }
            if (id != undefined) host.pt[id].damagedRecent = false;
          }, 500, host.pt[l].username, host);
          if (host.pt[l].health <= 0) {
            host.pt[l].ded = true;
            host.pt[l].dedTime = new Date();
            var q = 0, murderer;
            while (q < host.pt.length) {
              if (host.pt[q].username == results[1]) {
                murderer = host.pt[q].username;
                host.pt[q].kills++;
              }
              q++;
            }
            console.log(host.pt[l].username+' was killed by '+murderer);
            chatSockets.forEach(function(s) {
              s.send(JSON.stringify({
                type: 'chat',
                message: '<span style="color: darkblue;">'+host.pt[l].username+' was killed by '+murderer,
                send: '[SERVER]:',
                timestamp: Date.now(),
                server: 'public',
              }));
            });
            setTimeout(function(username, host) {
              var l = 0,
                id;
              while (l < host.pt.length) {
                if (host.pt[l].username == username) {
                  id = l;
                }
                l++;
              }
              if (id != undefined) {
                host.pt[id].ded = false;
                host.pt[id].health = host.pt[id].maxHealth;
              }
            }, 10000, host.pt[l].username, host);
          }
        }
        l++;
      }
      /*var l = 0;
      while (l < host.pt.length) {
        if (!host.pt[l].ded && !host.pt[l].serverOverrideActive) {
          var results = ai_check(host.pt[l].x, host.pt[l].y, false, host);
          if (results[0] && results[2] != host.pt[l].team && !results[3].grappled) { // add immunity to own bullets
            if (host.pt[l].shields > 0) {
              host.pt[l].shields -= 1;
            } else if (host.pt[l].immune) {} else {
              //clearInterval(host.pt[l].healInterval);
              host.pt[l].health -= results[1];
              host.pt[l].invis = false;
              host.pt[l].damagedRecent = true;
              clearTimeout(host[host.pt[l].username + '_damageTimer']);
              host[host.pt[l].username + '_damageTimer'] = setTimeout(function(username, host) {
                var l = 0,
                  id;
                while (l < host.pt.length) {
                  if (host.pt[l].username == username) {
                    id = l;
                  }
                  l++;
                }
                if (id != undefined) host.pt[id].damagedRecent = false;
              }, 500, host.pt[l].username, host);
              if (host.pt[l].health <= 0) {
                host.pt[l].ded = true;
                host.pt[l].dedTime = new Date();
                host.pt[l].kills = 0;
                var murderer = results[2];
                console.log(host.pt[l].username+' was killed by '+murderer);
                chatSockets.forEach(function(s) {
                  s.send(JSON.stringify({
                    type: 'chat',
                    message: '<span style="color: darkblue;">'+host.pt[l].username+' was killed by '+murderer,
                    send: '[SERVER]:',
                    timestamp: Date.now(),
                    server: 'public',
                  }));
                });
                // Action on death
                if (host.gamemode == 'FFA') {
                  var q = 0;
                  while (q < host.pt.length) {
                    if (host.pt[q].username == results[2]) {
                      host.pt[q].kills++;
                      var o = 0;
                      while (o < host.sockets.length) {
                        if (host.sockets[o].username == host.pt[q].username) {
                          host.sockets[o].send(JSON.stringify({
                            event: 'kill'
                          }));
                        }
                        o++;
                      }
                    }
                    q++
                  }
                  host.pt[l].ded = false;
                  host.pt[l].serverOverrideActive = true;
                  host.pt[l].invis = true;
                  host.pt[l].canTakeThermalDamage = false;
                  host.pt[l].isThermal = false;
                  var q = 0;
                  while (q<host.sockets.length) {
                    if (host.sockets[q].username == host.pt[l].username) {
                      host.sockets[q].send(JSON.stringify({
                        event: 'override',
                        data: {
                          key: 'invis',
                          value: true,
                          key2: 'spectator',
                          value2: true,
                        }
                      }));
                    }
                    q++;
                  }
                  setTimeout(function(username, host) {
                    var l = 0,
                      id;
                    while (l < host.pt.length) {
                      if (host.pt[l].username == username) {
                        id = l;
                      }
                      l++;
                    }
                    if (id != undefined) {
                      host.pt[id].serverOverrideActive = false;
                      host.pt[id].invis = false;
                      host.pt[id].ded = false;
                      host.pt[id].shields += 10;
                      var q = 0;
                      while (q<host.sockets.length) {
                        if (host.sockets[q].username == host.pt[id].username) {
                          host.sockets[q].send(JSON.stringify({
                            event: 'override',
                            data: {
                              key: 'invis',
                              value: false,
                              key2: 'spectator',
                              value2: false,
                            }
                          }));
                        }
                        q++;
                      }
                      host.pt[id].health = host.pt[id].maxHealth;
                    }
                  }, 10000, host.pt[l].username, host);
                } else if (host.gamemode == 'DUELS') {
                  if (host.gamestate == 1) { // this should ALWAYS be true, but unless some hacker kills himself while waiting for players.....
                    var q = 0,
                      winner;
                    while (q < host.pt.length) {
                      if (host.pt[q].username != host.pt[l].username) {
                        winner = host.pt[q].username;
                      }
                      q++;
                    }
                    var insult = generateInsult().replace('{1}', host.pt[l].username).replace('{2}', winner);
                    var q = 0;
                    while (q < host.sockets.length) {
                      host.sockets[q].send(JSON.stringify({
                        event: 'gameover',
                        data: {
                          winner: winner,
                          loser: host.pt[l].username,
                          message: insult,
                        },
                      }));
                      host.sockets[q].destroy(); // End the game
                      q++;
                    }
                  }
                } else if (host.gamemode == 'RAID') {
                  // kick the player from the game
                } else if (host.gamemode == 'TDM') {
                  host.pt[l].ded = false;
                  host.pt[l].serverOverrideActive = true;
                  host.pt[l].invis = true;
                  host.pt[l].canTakeThermalDamage = false;
                  host.pt[l].isThermal = false;
                  var q = 0;
                  while (q<host.sockets.length) {
                    if (host.sockets[q].username == host.pt[l].username) {
                      host.sockets[q].send(JSON.stringify({
                        event: 'override',
                        data: {
                          key: 'invis',
                          value: true,
                          key2: 'spectator',
                          value2: true,
                        }
                      }));
                    }
                    q++;
                  }
                  var q = 0;
                  while (q<host.teams[host.pt[l].team].length) {
                    if (host.teams[host.pt[l].team].username == host.pt[l].username) {
                      host.teams[host.pt[l].team].splice(q, 1);
                    }
                    q++;
                  }
                  if (host.teams[host.pt[l].team].length == 0) {
                    this.gameMessage = 'GAME OVER :D'
                  }
                  // end game if 0 players
                }
              }
            }
          } else if (results[0] && results[2] != this.team && results[3].grappled) {
            host.pt[l].grappled = true;
            host.pt[l].grapplePos = {
              startx: host.pt[l].x,
              starty: host.pt[l].y,
              x: results[3].startx-20,
              y: results[3].starty-20,
            }
            clearInterval(host.pt[l].grappleInterval);
            host.pt[l].grappleInterval = setInterval(function(host) {
              if (this.x > this.grapplePos.x) {
                if (this.x-this.grapplePos.x<20) {
                  if (checker(this.x-(this.x-this.grapplePos.x), this.y, host)) {
                    this.x -= this.x-this.grapplePos.x;
                  }
                } else {
                  if (checker(this.x-20, this.y, host)) {
                    this.x -= 20;
                  }
                }
              } else if (this.x < this.grapplePos.x) {
                if (this.grapplePos.x-this.x<20) {
                  if (checker(this.x+(this.grapplePos.x-this.x), this.y, host)) {
                    this.x += this.grapplePos.x-this.x;
                  }
                } else {
                  if (checker(this.x+20, this.y, host)) {
                    this.x += 20;
                  }
                }
              }
              if (this.y > this.grapplePos.y) {
                if (this.y-this.grapplePos.y<20) {
                  if (checker(this.x, this.y-(this.y-this.grapplePos.y), host)) {
                    this.y -= this.y-this.grapplePos.y;
                  }
                } else {
                  if (checker(this.x, this.y-20, host)) {
                    this.y -= 20;
                  }
                }
              } else if (this.y < this.grapplePos.y) {
                if (this.grapplePos.y-this.y<20) {
                  if (checker(this.x, this.y+(this.grapplePos.y-this.y), host)) {
                    this.y += this.grapplePos.y-this.y;
                  }
                } else {
                  if (checker(this.x, this.y+20, host)) {
                    this.y += 20;
                  }
                }
              }
              var m = 0;
              while (m<host.sockets.length) {
                if (host.sockets[m].username == this.username) {
                  host.sockets[m].send(JSON.stringify({
                    event: 'override',
                    data: {
                      key: 'x',
                      value: this.x,
                      key2: 'y',
                      value2: this.y,
                    }
                  }));
                }
                m++;
              }
              if (this.x == this.grapplePos.x && this.y == this.grapplePos.y) {
                this.grappled = false;
                clearInterval(this.grappleInterval);
              }
            }.bind(host.pt[l]), 20, host);
          }
          if (host.pt[l].pushback != 0) {
            host.pt[l].pushback += 1;
          }
        }
        l++;
      }*/
      /*var l = 0;
      while (l < host.b.length) {
        var results = ai_check(host.b[l].x * 50, host.b[l].y * 50, true, host);
        if (results[0]) {
          host.update.block = true; // BLOCK UPDATE
          host.update.bullet = true; // bullet update (bullet died to damage blok)
          host.b[l].health -= results[1];
          if (host.b[l].health <= 0) {
            let isScaffolding = false;
            var t = 0;
            while (t < host.b.length) {
              var q = 0;
              while (q < host.scaffolding.length) {
                if (host.b[t].x == host.scaffolding[q].x) {
                  if (host.b[l].y == host.scaffolding[q].y) {
                    isScaffolding = true;
                  }
                }
                q++;
              }
              t++;
            }
            if (isScaffolding) {
              var q = 0;
              while (q < host.scaffolding.length) {
                if (host.b[l].y == host.scaffolding[q].y) {
                  if (host.b[l].x == host.scaffolding[q].x) {
                    host.scaffolding.splice(q, 1);
                  }
                }
                q++;
              }
            }
            var q = 0;
            while (q < host.scaffolding.length) {
              if (host.b[l].y == host.scaffolding[q].y) {
                if (host.b[l].x == host.scaffolding[q].x) {
                  host.scaffolding.splice(q, 1);
                }
              }
              q++;
            }
            host.b[l].destroy(l, host);
          }
        }
        l++;
      }*/
    }, 30, this));
    if (this.gamemode == 'FFA') {
      var level = [
        [' #   #   #   #      #    #### ',' #   #   #   #  ### # ## #  # ',' #   #   #   #  # #      #  # ',' #############  # ### ##    # ',' #           #  #      # #  # ',' # ######### #  #### ### #### ',' # #         #  #  #   #      ',' # # ####### #  # #### # #####',' # #       # #  # #          #',' # ####### # #  # # ######## #',' #         # #  #   #  # # # #',' # ######### #  ###### #     #',' #           #  #      ## ####',' # # # # # #      # ##  # #   ',' #            @   #    ## # ##',' ## # # # #  #  #   ##        ',' #           #  # #    #######',' #  ### ###  #  # # ##        ',' # #   #   # #  #       ##### ',' # #       # #  # ### #       ',' # #       # #  #   #   # ####',' #  #     #  #  ##### ###     ',' # # #   #   #  #      #  ### ',' #    # #  # #  # # # ## ## # ',' ## #  # #  ##  # #      #  # ',' #   #     # #  # # # ##      ',' ## #   # #  #  # # # #  # ## ',' # #  #  # # #  #   #  # #    ',' #   # #     #  ### # ## # ## ','   #    # # ##        #  #    '],
        ['==============================','==============================','==============================','==============================','==============================','==============================','==============================','==============================','##############################','111111111111#     ###111111111','1111111111122####22##111111111','1111#11111#22    22##1111#1111','111111111222      222111111111','11111111122        22111111111','111111111#    #     #1########','1#########     @#   #111111111','111111111#   #      #########1','########1#     #    #111111111','11111111122        22111111111','111111111222      2221111#1111','1111#1111##22    22#1111111111','111111111##22####2211111111111','111111111###     #111111111111','##############################','===============================','==============================','==============================','==============================','===============================','=============================='],
        ['    #   #                     ', ' ## #   # 11 1111 11        # ', ' ## #   #  1 1111 1  #####11  ', '       ##  1 1111 1       11  ', '###   #   1        1       #  ', '     2      ######         #  ', '    #  2####      ####2    #  ', '   #  222            222   #  ', '####  #21            12#   #  ', '      #  1          1  #      ', ' 2    #   1###  ###1   #    2 ', '   2  #   #        #   #  2   ', ' 2   #    #        #    #   2 ', '   2 #    #  #  #  #    # 2   ', ' 21  #        @         #   2 ', '   2 #                  # 21  ', ' 2   #    #  #  #  #    #   2 ', '   2 #    #        #    # 2   ', ' 2    #   #        #   #    2 ', '   2  #   1###  ###1   #  2   ', '      #  1          1  #      ', '  #   #21            12#  ####', '  #   222            222  #   ', '  #    2####      ####2  #    ', '  #         ######      2     ', '  #       1        1   #   ###', '  11       1 1111 1  ##       ', '  11#####  1 1111 1  #   # ## ', ' #        11 1111 11 #   # ## ', '                     #   #    '],
        ['                              ','  #                        #  ','     #  #  #  ##  #  #  #     ','                              ','  #                        #  ','     22################22     ','     22     # 2222     22     ','  #  # # #  ######  # # #  #  ','     #  @  #  ##  #     #     ','     # #  #   ##   #  # #     ','  #  #   #    ##    #   #  #  ','     #  # 22 1  1 22 #  #     ','     # #    #    #    # #     ','###### #   1 #  # 1   # ######','######222 #  #  #  # 222######','######222 #  #  #  # 222#######','###### #   1 #  # 1   # ######','     # #    #    #    # #     ','     #  # 22 1  1 22 #  #     ','  #  #   #    ##    #   #  #  ','     # #  #   ##   #  # #     ','     #     #  ##  #     #     ','  #  # # #  ######  # # #  #  ','     22     2222 #     22     ','     22################22     ','  #                        #  ','                              ','     #  #  #  ##  #  #  #     ','  #                        #  ','                           '],
        ['  222222        #    222    2 ','   22     2  2  #22       2  2','2   2   2       #2     2      ','22  2      2    #   #######   ','2222  2        2#2  #22    2  ','2       2       #  2#2       2','2 2         2   #   #    2    ','2   2         ####2### #######','      2       ##11111#        ','2        2    ##11111######## ','  2           2211111#        ','       2    2 ##11111# #######','  #  #    2   ##11111#11111111',' #    #      @########11111111','##    ###2#####      ####  ###','###  ####      #####2###    ##','11111111########      2#    # ','11111111#11111## 2      #  #  ','####### #11111##    2         ','        #1111122  2     2     ',' ########11111##            2 ','        #11111##              ','####### ###2####   2   2     2','  222    #   #   2        2  2','         #2  #          2    2',' 2    22 #  2#    2  2    2222','   #######   # 2         2  22','         2   #     2     2   2','2    2      2#  2        22   ','         222 #        222222  '],  
        ['                              ','###### 11111  222222222222222 ','  #  # 11111                2 ','  #  # 11111  2222222222222 2 ','  #  # 11111                2 ','  #  # 11111  222222222222222 ','  #  #                        ','  #  #                        ','  #  #          #             ','  #  #    #               #   ','  #  #                        ','                      #       ','                              ','  111   ####                  ','  111   #                  #  ',' 11111  # ###  #####          ',' 11111  #   #  #   #          ','   2    ### #  #   #   #      ','   2        #  #   #          ','   2     ####  #####          ','                              ','#################### 1   1 1  ','    #    #    #    #1 1   1   ','    #    #    #    # 1   1 1  ',' ####### # ####### #1 1   1   ','         #         # 1   1 1  ','         #         #1 1   1   ','11########11######## 1   1 1  ','                              ','                              '],
      ]
      var random = Math.floor(Math.random()*(level.length));
      levelReader(level[random], true, this);
    } else if (this.gamemode == 'DUELS') {
      levelReader(['  ########################2222','   #######################2222','#   ######################2222','##   #####################2222','###           11          ####','####         1111         ####','####     ####2  2####     ####','####     ####2  2####     ####','####    22###2  2###22    ####','####  ##222##2  2##222##  ####','####  ###21        12###  ####','####  #### 11    11 ####  ####','####  #### 1 2222 1 ####  ####','#### 12222  22##22  22221 ####','####11      2####2      11####','####11      2####2      11####','#### 12222  22##22  22221 ####','####  #### 1 2222 1 ####  ####','####  #### 11    11 ####  ####','####  ###21        12###  ####','####  ##222##2  2##222##  ####','####    22###2  2###22    ####','####     ####2  2####     ####','####     ####2  2####     ####','####         1111         ####','####          11           ###','2222#####################   ##','2222######################   #','2222#######################   ','2222########################  '], true, this);

    }
  }
  countdown() {

  }
  gameRunner(host) {
    if (host.gamemode == 'DUELS') {
      if (host.gamestate == 0) {
        if (host.pt.length == 1) {
          host.sockets[0].send(JSON.stringify({
            event: 'override',
            data: {
              key: 'x',
              value: 0,
              key2: 'y',
              value2: 0,
            }
          }));
          host.gameMessage = 'Waiting For Opponent';
        }
        if (host.pt.length == 2) {
          host.gamestate = 2; // counting down to game
          setTimeout(function(host) {
            host.gamestate = 1;
          }, 4000, host);
          setTimeout(function(host) {
            host.gameMessage = 'Starting in 3...';
          }, 1000, host);
          setTimeout(function(host) {
            host.gameMessage = 'Starting in 2...';
          }, 2000, host);
          setTimeout(function(host) {
            host.gameMessage = 'Starting in 1...';
          }, 3000, host);
        }
      } else if (host.gamestate == 1) {
        host.gameMessage = host.pt[0].username + ' VS. ' + host.pt[1].username;
      } else if (host.gamestate == 2) {
        host.sockets[0].send(JSON.stringify({
          event: 'override',
          data: {
            key: 'x',
            value: 0,
            key2: 'y',
            value2: 0,
          }
        }));
        host.sockets[1].send(JSON.stringify({
          event: 'override',
          data: {
            key: 'x',
            value: 1460,
            key2: 'y',
            value2: 1460,
          }
        }));
      }
    } else if (host.gamemode == 'DEFENSE') {
      if (host.startWave) {
        host.startWave = false;
        // spawn waves
      }
      if (host.ai.length == 0) {

      }
    } else if (host.gamemode == 'TDM') {
      if (host.gamestate == 0) {
        host.gameMessage = 'Waiting For Players '+host.pt.length+'/4';
      } else if (host.gamestate == 1) {
        // counting down :D
      } else if (host.gamestate == 2) {
        host.gameMessage = host.teams.red.length+' Red VS '+host.teams.blue.length+' Blue'; // in game
      }
    }
  }
  joinerupdate(data) {
    var tank = data.data;
    var l = 0;
    while (l < this.pt.length) {
      if (this.pt[l].username == tank.username) {
        /*clearTimeout(this.pt[l].inactiveTimer);
        this.pt[l].inactiveTimer = setTimeout(function(username) {
          var q = 0;
          while (q<this.sockets.length) {
            if (this.sockets[q].username == username) {
              this.sockets[q].destroy();
            }
            q++;
          }
        }.bind(this), 60000, this.pt[l].username);*/ // INACTIVE FUNCTION
        this.pt[l].pings.push(new Date(new Date().toISOString()).getTime() - new Date(data.sendTime).getTime());
        if (this.pt[l].pings.length > 60) {
          this.pt[l].pings.shift();
        }
        if (!this.pt[l].ded) {
          var userActive = false;
          if (this.pt[l].x != tank.x) {
            if (!this.pt[l].grappled) {
              this.pt[l].x = tank.x;
            }
            userActive = true;
            this.update.tank = true; // TANK UPDATE
          }
          if (this.pt[l].isBuilder && tank.immune && false) {
            if (tank.x > 0) {
              // += 
              if ((this.pt[l].x % 50 < 10 && this.pt[l].x > 0) || (Math.abs(this.pt[l].x) % 50 > 40 && this.pt[l].x < 0)) {
                var amountX = 1;
                if (this.pt[l].x < 0) {
                  amountX = 2;
                }
                var amountY = 0;
                if (this.pt[l].y < 0) {
                  amountY = 1;
                }
                weak((this.pt[l].x - this.pt[l].x % 50) / 50 - amountX, (this.pt[l].y - this.pt[l].y % 50) / 50 - amountY, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x - this.pt[l].x % 50) / 50 - amountX,
                  y: (this.pt[l].y - this.pt[l].y % 50) / 50 - amountY,
                  type: 'weak'
                });
              }
            } else {
              // -=
              if (((this.pt[l].x + 40) % 50 > 40 && this.pt[l].x > 0) || (Math.abs(this.pt[l].x + 40) % 50 < 10 && this.pt[l].x < 0)) {
                var amountX = 1;
                if (this.pt[l].x < 0) {
                  amountX = 0;
                }
                var amountY = 0;
                if (this.pt[l].y < 0) {
                  amountY = -1;
                }
                weak((this.pt[l].x - (this.pt[l].x % 50)) / 50 + amountX, (this.pt[l].y - this.pt[l].y % 50) / 50 + amountY, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x - (this.pt[l].x % 50)) / 50 + amountX,
                  y: (this.pt[l].y - (this.pt[l].y % 50)) / 50 + amountY,
                  type: 'weak'
                });
              }
            }
          }
          if (this.pt[l].y != tank.y) {
            if (!this.pt[l].grappled) {
              this.pt[l].y = tank.y;
            }
            userActive = true;
            this.update.tank = true; // TANK UPDATE
          }
          if (this.pt[l].isBuilder && tank.immune && false) {
            if (tank.y > 0) {
              // +=
              if ((this.pt[l].y % 50 < 10 && this.pt[l].y > 0) || (Math.abs(this.pt[l].y) % 50 > 40 && this.pt[l].y < 0)) {
                var amountX = 0;
                if (this.pt[l].x < 0) {
                  amountX = 1;
                }
                var amountY = 1;
                if (this.pt[l].y < 0) {
                  amountY = 2;
                }
                weak((this.pt[l].x - this.pt[l].x % 50) / 50 - amountX, (this.pt[l].y - this.pt[l].y % 50) / 50 - amountY, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x - this.pt[l].x % 50) / 50 - amountX,
                  y: (this.pt[l].y - (this.pt[l].y % 50)) / 50 - amountY,
                  type: 'weak'
                });
              }
            } else {
              // -=
              if (((this.pt[l].y + 40) % 50 > 40 && this.pt[l].y > 0) || (Math.abs(this.pt[l].y + 40) % 50 < 10 && this.pt[l].y < 0)) {
                var amountX = 0;
                if (this.pt[l].x < 0) {
                  amountX = -1;
                }
                var amountY = 1;
                if (this.pt[l].y < 0) {
                  amountY = 0;
                }
                weak((this.pt[l].x - (this.pt[l].x % 50)) / 50 + amountX, (this.pt[l].y - this.pt[l].y % 50) / 50 + amountY, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x - (this.pt[l].x % 50)) / 50 + amountX,
                  y: (this.pt[l].y - (this.pt[l].y % 50)) / 50 + amountY,
                  type: 'weak'
                });
              }
            }
          }
          if (tank.shielded) {
            this.pt[l].shields = 5;
            setTimeout(function(username, host) {
              var l = 0,
                id;
              while (l<host.pt.length) {
                if (host.pt[l].username == username) {
                  id = l;
                }
                l++;
              }
              if (id != undefined) {
                host.pt[id].shields = 0;
              }
            }, 10000, this.pt[l].username, this);
          }
          this.pt[l].base = tank.base;
          if (this.pt[l].rotation != tank.rotation) {
            this.pt[l].rotation = tank.rotation;
            userActive = true;
            this.update.tank = true; // TANK UPDATE
          }
          /*if (userActive) {
            clearTimeout(this.pt[l].inactiveTimer2);
            this.pt[l].inactiveTimer2 = false;
          } else if (!this.pt[l].inactiveTimer2) {
            this.pt[l].inactiveTimer2 = setTimeout(function(username) {
              var q = 0;
              while (q<this.sockets.length) {
                if (this.sockets[q].username == username) {
                  this.sockets[q].destroy();
                }
                q++;
              }
            }.bind(this), 120000, this.pt[l].username);
          }*/ // INACTIVE FUNCTION
          this.pt[l].leftright = tank.leftright;
          this.pt[l].immune = tank.immune;
          this.pt[l].cosmetic = tank.cosmetic;
          if (!this.pt[l].damagedRecent || !this.pt[l].firedRecent) this.pt[l].invis = tank.invis;
          this.pt[l].canChangeInvisStatus = tank.canChangeInvisStatus;
          this.pt[l].canInvis = tank.canInvis;
          if (tank.flashbangFired) {
            this.update.scaffolding = true;
            this.update.block = true;
            var blocks = checker2(this.pt[l].x, this.pt[l].y, this); // require checker2
            var g = 0;
            while (g < blocks.length) {
              let isScaffolding = false;
              var q = 0;
              while (q < this.scaffolding.length) {
                if (this.b[blocks[g]].x == this.scaffolding[q].x) {
                  if (this.b[blocks[g]].y == this.scaffolding[q].y) {
                    isScaffolding = true;
                  }
                }
                q++;
              }
              if (isScaffolding) {
                var q = 0;
                while (q < this.scaffolding.length) {
                  if (this.b[blocks[g]].y == this.scaffolding[q].y) {
                    if (this.b[blocks[g]].x == this.scaffolding[q].x) {
                      this.scaffolding.splice(q, 1);
                    }
                  }
                  q++;
                }
              }
              this.b[blocks[g]].destroy(blocks[g], this); // require block_support may need to be Host function
              g++;
            }
          }
          if (tank.placeScaffolding && !this.pt[l].serverOverrideActive) {
            if (tank.rotation >= 337.5 || tank.rotation < 22.5) {
              if (tank.scaffoldingType == 'weak') {
                weak((this.pt[l].x - 5) / 50, (this.pt[l].y + 40) / 50, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x - 5) / 50,
                  y: (this.pt[l].y + 40) / 50,
                  type: 'weak',
                });
              } else {
                gold((this.pt[l].x - 5) / 50, (this.pt[l].y + 40) / 50, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x - 5) / 50,
                  y: (this.pt[l].y + 40) / 50,
                  type: 'gold',
                });
              }
            }
            if (tank.rotation >= 22.5 && tank.rotation < 67.5) {
              if (tank.scaffoldingType == 'weak') {
                weak((this.pt[l].x - 50) / 50, (this.pt[l].y + 40) / 50, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x - 50) / 50,
                  y: (this.pt[l].y + 40) / 50,
                  type: 'weak'
                });
              } else {
                gold((this.pt[l].x - 50) / 50, (this.pt[l].y + 40) / 50, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x - 50) / 50,
                  y: (this.pt[l].y + 40) / 50,
                  type: 'gold'
                });
              }
            }
            if (tank.rotation >= 67.5 && tank.rotation < 112.5) {
              if (tank.scaffoldingType == 'weak') {
                weak((this.pt[l].x - 50) / 50, (this.pt[l].y - 5) / 50, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x - 50) / 50,
                  y: (this.pt[l].y - 5) / 50,
                  type: 'weak'
                });
              } else {
                gold((this.pt[l].x - 50) / 50, (this.pt[l].y - 5) / 50, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x - 50) / 50,
                  y: (this.pt[l].y - 5) / 50,
                  type: 'gold'
                });
              }
            }
            if (tank.rotation >= 112.5 && tank.rotation < 157.5) {
              if (tank.scaffoldingType == 'weak') {
                weak((this.pt[l].x - 50) / 50, (this.pt[l].y - 50) / 50, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x - 50) / 50,
                  y: (this.pt[l].y - 50) / 50,
                  type: 'weak'
                });
              } else {
                gold((this.pt[l].x - 50) / 50, (this.pt[l].y - 50) / 50, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x - 50) / 50,
                  y: (this.pt[l].y - 50) / 50,
                  type: 'gold'
                });
              }
            }
            if (tank.rotation >= 157.5 && tank.rotation < 202.5) {
              if (tank.scaffoldingType == 'weak') {
                weak((this.pt[l].x - 5) / 50, (this.pt[l].y - 50) / 50, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x - 5) / 50,
                  y: (this.pt[l].y - 50) / 50,
                  type: 'weak'
                });
              } else {
                gold((this.pt[l].x - 5) / 50, (this.pt[l].y - 50) / 50, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x - 5) / 50,
                  y: (this.pt[l].y - 50) / 50,
                  type: 'gold'
                });
              }
            }
            if (tank.rotation >= 202.5 && tank.rotation < 247.5) {
              if (tank.scaffoldingType == 'weak') {
                weak((this.pt[l].x + 40) / 50, (this.pt[l].y - 50) / 50, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x + 40) / 50,
                  y: (this.pt[l].y - 50) / 50,
                  type: 'weak'
                });
              } else {
                gold((this.pt[l].x + 40) / 50, (this.pt[l].y - 50) / 50, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x + 40) / 50,
                  y: (this.pt[l].y - 50) / 50,
                  type: 'gold'
                });
              }
            }
            if (tank.rotation >= 247.5 && tank.rotation < 292.5) {
              if (tank.scaffoldingType == 'weak') {
                weak((this.pt[l].x + 40) / 50, (this.pt[l].y - 5) / 50, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x + 40) / 50,
                  y: (this.pt[l].y - 5) / 50,
                  type: 'weak'
                });
              } else {
                gold((this.pt[l].x + 40) / 50, (this.pt[l].y - 5) / 50, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x + 40) / 50,
                  y: (this.pt[l].y - 5) / 50,
                  type: 'gold'
                });
              }
            }
            if (tank.rotation >= 292.5 && tank.rotation < 337.5) {
              if (tank.scaffoldingType == 'weak') {
                weak((this.pt[l].x + 40) / 50, (this.pt[l].y + 40) / 50, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x + 40) / 50,
                  y: (this.pt[l].y + 40) / 50,
                  type: 'weak'
                });
              } else {
                gold((this.pt[l].x + 40) / 50, (this.pt[l].y + 40) / 50, true, this);
                this.scaffolding.push({
                  x: (this.pt[l].x + 40) / 50,
                  y: (this.pt[l].y + 40) / 50,
                  type: 'gold'
                });
              }
            }
            this.update.scaffolding = true; // Scaffolding UPDATE
            this.update.block = true; // Block Update
          }
          if (tank.usingToolkit && !this.pt[l].serverOverrideActive) {
            this.pt[l].healChunks = this.pt[l].maxHealth;
            this.pt[l].healInterval = setInterval(function() {
              if (this.ded) {
                clearInterval(this.healInterval);
              }
              var healChunk = 1;
              if (this.health+healChunk<=this.maxHealth) {
                this.health += healChunk;
                this.healChunks--;
                if (this.healChunks == 0) {
                  clearInterval(this.healInterval);
                }
              } else {
                this.health = this.maxHealth;
                clearInterval(this.healInterval);
              }
            }.bind(this.pt[l]), 30);
            this.pt[l].t.push(setTimeout(function() {
              clearInterval(this.healInterval);
            }.bind(this.pt[l]), 40000));
          }
          if (tank.blockShield) {
            this.pt[l].shields += 10;
          }
          if (tank.fire && !this.pt[l].serverOverrideActive) {
            if (tank.invis) {
              this.pt[l].invis = false;
              this.pt[l].firedRecent = true;
              setTimeout(function(host, l) {
                host.pt[l].firedRecent = false;
              }, 500, this, l);
            }
            this.pt[l].pushback = -3;
            /*if (tank.fireType == 3) {
              setTimeout(function(a, tank, l) {
                if (tank.rotations[0] > 180 && tank.rotations[0] < 270) {
                  a.s.push(new Shot(a.pt[l].x + 20, a.pt[l].y + 20, a.s.length - 1, -tank.yd[0], tank.xd[0], tank.type, a.pt[l].team, tank.rotations[0], tank.rank));
                } else if (tank.rotations[0] > 270) {
                  a.s.push(new Shot(a.pt[l].x + 20, a.pt[l].y + 20, a.s.length - 1, tank.yd[0], -tank.xd[0], tank.type, a.pt[l].team, tank.rotations[0], tank.rank));
                } else {
                  a.s.push(new Shot(a.pt[l].x + 20, a.pt[l].y + 20, a.s.length - 1, tank.xd[0], tank.yd[0], tank.type, a.pt[l].team, tank.rotations[0], tank.rank));
                }
              }, 20, this, tank, l);
              setTimeout(function(a, tank, l) {
                if (tank.rotations[0] > 180 && tank.rotations[0] < 270) {
                  a.s.push(new Shot(a.pt[l].x + 20, a.pt[l].y + 20, a.s.length - 1, -tank.yd[0], tank.xd[0], tank.type, a.pt[l].team, tank.rotations[0], tank.rank));
                } else if (tank.rotations[0] > 270) {
                  a.s.push(new Shot(a.pt[l].x + 20, a.pt[l].y + 20, a.s.length - 1, tank.yd[0], -tank.xd[0], tank.type, a.pt[l].team, tank.rotations[0], tank.rank));
                } else {
                  a.s.push(new Shot(a.pt[l].x + 20, a.pt[l].y + 20, a.s.length - 1, tank.xd[0], tank.yd[0], tank.type, a.pt[l].team, tank.rotations[0], tank.rank));
                }
              }, 40, this, tank, l);
              setTimeout(function(a, tank, l) {
                if (tank.rotations[0] > 180 && tank.rotations[0] < 270) {
                  a.s.push(new Shot(a.pt[l].x + 20, a.pt[l].y + 20, a.s.length - 1, -tank.yd[0], tank.xd[0], tank.type, a.pt[l].team, tank.rotations[0], tank.rank));
                } else if (tank.rotations[0] > 270) {
                  a.s.push(new Shot(a.pt[l].x + 20, a.pt[l].y + 20, a.s.length - 1, tank.yd[0], -tank.xd[0], tank.type, a.pt[l].team, tank.rotations[0], tank.rank));
                } else {
                  a.s.push(new Shot(a.pt[l].x + 20, a.pt[l].y + 20, a.s.length - 1, tank.xd[0], tank.yd[0], tank.type, a.pt[l].team, tank.rotations[0], tank.rank));
                }
              }, 60, this, tank, l);
              setTimeout(function(a, tank, l) {
                if (tank.rotations[0] > 180 && tank.rotations[0] < 270) {
                  a.s.push(new Shot(a.pt[l].x + 20, a.pt[l].y + 20, a.s.length - 1, -tank.yd[0], tank.xd[0], tank.type, a.pt[l].team, tank.rotations[0], tank.rank));
                } else if (tank.rotations[0] > 270) {
                  a.s.push(new Shot(a.pt[l].x + 20, a.pt[l].y + 20, a.s.length - 1, tank.yd[0], -tank.xd[0], tank.type, a.pt[l].team, tank.rotations[0], tank.rank));
                } else {
                  a.s.push(new Shot(a.pt[l].x + 20, a.pt[l].y + 20, a.s.length - 1, tank.xd[0], tank.yd[0], tank.type, a.pt[l].team, tank.rotations[0], tank.rank));
                }
              }, 80, this, tank, l);
              setTimeout(function(a, tank, l) {
                if (tank.rotations[0] > 180 && tank.rotations[0] < 270) {
                  a.s.push(new Shot(a.pt[l].x + 20, a.pt[l].y + 20, a.s.length - 1, -tank.yd[0], tank.xd[0], tank.type, a.pt[l].team, tank.rotations[0], tank.rank));
                } else if (tank.rotations[0] > 270) {
                  a.s.push(new Shot(a.pt[l].x + 20, a.pt[l].y + 20, a.s.length - 1, tank.yd[0], -tank.xd[0], tank.type, a.pt[l].team, tank.rotations[0], tank.rank));
                } else {
                  a.s.push(new Shot(a.pt[l].x + 20, a.pt[l].y + 20, a.s.length - 1, tank.xd[0], tank.yd[0], tank.type, a.pt[l].team, tank.rotations[0], tank.rank));
                }
              }, 100, this, tank, l);
            } else {*/
              var q = 0;
              while (q < tank.xd.length) {
                if (tank.rotations[q] > 180 && tank.rotations[q] < 270) {
                  this.s.push(new Shot(this.pt[l].x + 20, this.pt[l].y + 20, this.s.length - 1, -tank.yd[q], tank.xd[q], tank.type, this.pt[l].team, tank.rotations[0], tank.rank, tank.username));
                } else if (tank.rotations[q] > 270) {
                  this.s.push(new Shot(this.pt[l].x + 20, this.pt[l].y + 20, this.s.length - 1, tank.yd[q], -tank.xd[q], tank.type, this.pt[l].team, tank.rotations[0], tank.rank, tank.username));
                } else {
                  this.s.push(new Shot(this.pt[l].x + 20, this.pt[l].y + 20, this.s.length - 1, tank.xd[q], tank.yd[q], tank.type, this.pt[l].team, tank.rotations[0], tank.rank, tank.username));
                }
                q++;
              }
            //}
          }
        }
      }
      l++;
    }
  }
  joinerjoin(data) { //done
    console.log(data.username+' joined room '+this.gamemode+':'+this.channelname);
    chatSockets.forEach(function(s) {
      s.send(JSON.stringify({
        type: 'chat',
        message: '<span style="color: green">'+data.username+' joined room '+this.channelname+' in '+this.gamemode+'</span>',
        send: '[SERVER]:',
        timestamp: Date.now(),
        server: 'public',
      }));
    }.bind(this))
    // Trigger FULL UPDATE
    this.update.block = true;
    this.update.blocks = [{
      operation: 'full',
      id: '*',
    }] // delete prevois updates (full update includes all);
    this.update.tank = true;
    this.update.scaffolding = true;

    // registers a new tank to the server
    // pt = playertanks, teamData = team core hp and team playertanks
    var tank = data;
    if (this.gamemode == 'FFA') {
      var l = 0;
      while (l<this.sockets.length) {
        if (this.sockets[l].username == tank.username) {
          this.sockets[l].send(JSON.stringify({
            event: 'override',
            data: {
              key: 'x',
              value: this.spawn.x,
              key2: 'y',
              value2: this.spawn.y,
            }
          }));
        }
        l++;
      }
    } else if (this.gamemode == 'DUELS') {
      var l = 0;
      while (l < this.sockets.length) {
        if (this.sockets[l].username == tank.username) {
          if (this.pt.length == 0) {
            this.sockets[l].send(JSON.stringify({
              event: 'override',
              data: {
                key: 'x',
                value: 0,
                key2: 'y',
                value2: 0,
              }
            }));
          } else if (this.pt.length == 1) {
            this.sockets[l].send(JSON.stringify({
              event: 'override',
              data: {
                key: 'x',
                value: 1460,
                key2: 'y',
                value2: 1460,
              }
            }));
          }
        }
        l++;
      }
    } else if (this.gamemode == 'DEFENSE') {
      var l = 0;
      while (l < this.sockets.length) {
        if (this.sockets[l].username == tank.username) {
          this.sockets[l].send(JSON.stringify({
            event: 'override',
            data: {
              key: 'team',
              value: 'Players',
            }
          }));
        }
        l++;
      }
    } else if (this.gamemode == 'TDM') {
      tank.team = 'lobby';
      tank.damagedRecent = false;
      this.pt.push(tank);
      if (this.pt.length == 4) {
        var l = 0, blue = [], red = [];
        while (l<this.pt.length) {
          var canBlue, canRed;
          if (blue.length != 2) {
            canBlue = true;
          } else {
            canBlue = false;
          }
          if (red.length != 2) {
            canRed = true;
          } else {
            canRed = false;
          }
          if (canBlue && canRed) {
            if (Math.random() < 0.5) {
              blue.push(this.pt[l]);
              this.pt[l].team = 'blue';
            } else {
              red.push(this.pt[l]);
              this.pt[l].team = 'red';
            }
          } else {
            if (canBlue) {
              blue.push(this.pt[l]);
              this.pt[l].team = 'blue';
            } else if (canRed) {
              red.push(this.pt[l]);
              this.pt[l].team = 'red';
            }
          }
          l++;
        }
        this.teams = {
          blue: blue,
          red: red,
        }
        this.gamestate = 1;
        levelReader(['                              ','           #      #           ','  #        #      #  ####  #  ','  #   ##   ###  ###        #  ','  #                           ','  #                           ','  #  #####   ####   #####  #  ','                        #  #  ','                        #  #  ','###  #########  ############  ','  #  #                     #  ','  #  #                     #  ','     #    ##  ##  ##   #####  ','     #   #          #         ','  ########  #    #  #         ','         #  #    #  ########  ','         #          #   #     ','  #####   ##  ##  ##    #     ','  #                     #  #  ','  #                     #  #  ','  ############  #########  ###','  #  #                        ','  #  #                        ','  #  #####   ####   #####  #  ','                           #  ','                           #  ','  #        ###  ###        #  ','  #  ####  #      #   ##   #  ','           #      #           ','                              '], true, this);
        this.count = 10;
        this.gameMessage = 'Game Starting in '+this.count;
        this.countdown = setInterval(function() {
          var l = 0;
          while (l<this.pt.length) {
            if (this.pt[l].team == 'blue') {
              var q = 0;
              while (q<this.sockets.length) {
                if (this.pt[l].username == this.sockets[q].username) {
                  this.sockets[q].send(JSON.stringify({
                    event: 'override',
                    data: {
                      key: 'x',
                      value: 0,
                      key2: 'y',
                      value2: 730,
                    }
                  }));
                }
                q++;
              }
            } else if (this.pt[l].team == 'red') {
              var q = 0;
              while (q<this.sockets.length) {
                if (this.pt[l].username == this.sockets[q].username) {
                  this.sockets[q].send(JSON.stringify({
                    event: 'override',
                    data: {
                      key: 'x',
                      value: 1460,
                      key2: 'y',
                      value2: 730,
                    }
                  }));
                }
                q++;
              }
            }
            l++;
          }
          this.count -= 1;
          this.gameMessage = 'Game Starting in '+this.count;
          if (this.count == 0) {
            setTimeout(function() {
              this.gamestate = 2;
            }.bind(this), 3000);
            this.gameMessage = 'FIGHT'
            clearInterval(this.countdown);
          }
        }.bind(this), 1000);
      }
      return;
    }
    tank.damagedRecent = false;
    this.pt.push(tank);
  }
  disconnect(username) { // done?
    console.log(username+' ragequit room '+this.gamemode+':'+this.channelname);
    chatSockets.forEach(function(s) {
      s.send(JSON.stringify({
        type: 'chat',
        message: '<span style="color: red;">'+username+' ragequit room '+this.channelname+' in '+this.gamemode+'</span>',
        send: '[SERVER]:',
        timestamp: Date.now(),
        server: 'public',
      }));
    }.bind(this))
    if (this.gamemode == 'DUELS') {
      this.gamestate = 0; // If someone leaves, prevent game crashing by going back to waiting for opponent.
    }
    var l = 0;
    while (l < this.pt.length) {
      if (this.pt[l].username == username) {
        var q = 0;
        while (q<this.pt[l].t.length) {
          clearTimeout(this.pt[l].t[q]);
          q++;
        }
        var q = 0;
        while (q<this.pt[l].i.length) {
          clearInterval(this.pt[l].i[q]);
          q++;
        }
        clearTimeout(this.pt[l].inactiveTimer);
        clearTimeout(this.pt[l].inactiveTimer2);
        clearInterval(this.pt[l].healInterval);
        this.pt.splice(l, 1);
      }
      l++;
    }
    var l = 0,
      gamemode;
    while (l < this.sockets.length) {
      if (this.sockets[l].username == username) {
        gamemode = this.sockets[l].gamemode;
        this.sockets.splice(l, 1);
      }
      l++;
    }
    if (this.sockets.length == 0) {
      var l = 0;
      while (l < this.i) {
        clearInterval(this.i[l]);
        l++;
      }
      servers.tanks[gamemode][this.channelname] = undefined;
      console.log('[MULTI] => Server shutdown - ' + this.channelname);
    }
  }
  send() { //done
    if (!this.update.tank && !this.update.block && !this.update.bullet && !this.update.scaffolding) {
      return;
    }
    this.fpsC++;
    var m = 0;
    while (m<this.sockets.length) {
      var q = 0, kills, ded, dedTime;
      while (q<this.pt.length) {
        if (this.pt[q].username == this.sockets[m].username) {
          dedTime = this.pt[q].dedTime
          ded = this.pt[q].ded;
          kills = this.pt[q].kills;
        }
        q++;
      }
      var gameMessage;
      if (!this.gameMessage) {
        if (ded) {
          gameMessage = 'Back in '+new String(new Date().getTime() - dedTime.getTime());
        } else {
          gameMessage = kills+' Kills <-> '+this.fps+' FPS';
        }
      } else {
        gameMessage = this.gameMessage;
      }
      var updateObject = {
        sendTime: new Date().toISOString(),
        event: 'hostupdate',
        gameMessage: gameMessage,
      };
      if (this.update.block) {
        updateObject.blocks = JSON.parse(JSON.stringify(this.b, replacer));
      }
      if (this.update.scaffolding) {
        updateObject.scaffolding = JSON.parse(JSON.stringify(this.scaffolding, replacer));
      }
      if (this.update.tank) {
        var pt = JSON.parse(JSON.stringify(this.pt, replacer));
        updateObject.tanks = pt;
      }
      if (this.update.bullet) {
        updateObject.bullets = JSON.parse(JSON.stringify(this.s, replacer));
      }
      this.sockets[m].atomic(function() {
        this.send(JSON.stringify(updateObject));
      }.bind(this.sockets[m]));
      m++;
    }
    this.update.block = false;
    this.update.tank = false;
    this.update.bullet = false;
    this.update.scaffolding = false;
  }
}
var insults = [ // {1} = loser, {2} = winner
  `{1} went out in a blaze of skill`,
  `{1} ran out of IQ while fighting {2}`,
  `{1} took the L to {2}`,
  `{1} was obliterated by {2}`,
  `{2} could't resist murdering {1}`,
  `{2} blew {1} up`,
  `{1} wasn't in {2}'s class`,
  `{1}'s skill was no match for {2}`,
  `{1} didn't last long`,
  `{2} proved to be superior to {1}`,
  `{1} forgot to toolkit`,
  `{2} wasn't even trying`,
  `{1} got flexed on by {2}`,
  `{2}'s skill is unmatched by {1}`,
  `{2} got lucky...`,
  `{1} tripped`,
  `{1} forgot their mouse`,
  `{1} fell and can't get up`,
  `{2} > {1}`,
  `{1} didn't die, it was just an illusion`,
  `{1} refused to die until {2} convinced them`,
  `{1} deserved to die`,
  `{1} was playing the wrong game`,
  `{1} got pushed off a cliff by {2}`,
  `{1} got oofed`,
  `{1} had a heart attack`,
  `{1} raged quit`,
  `{1} got ran over by a train`,
  `{2} couln't stand {1} any longer`,
  `{2} hired a hitman`,
  `{1} typed the code wrong`,
  `{2} is running from the cops`,
  `{1} got hit by a meteor`,
  `{2} hit {1} with the sun`,
  `{2} used Evanism on {1}`,
  `{1} refused to live`,
  `{1} got Evanismed`,
  `{1} >< _ ><`,
  `{1} is having a bad day`,
  `{1}'s credit score went down 50%`,
  `{2} dropped a nuke on {1}`,
  `{1} didn't have a nuke tank`,
  `{1} was busy eating`,
  `{2} flodded {1}'s tank`,
  `{2} has anger issues`,
  `{2} forced {1} to read this message`,
  `{2} flushed {1} down the toilet`,
  `{2} took out the trash`,
  `{1} ran out of boosts`,
  `{1} is getting yelled at by parents`,
  `{1} ran out of electonic time`,
];

function generateInsult() {
  return insults[Math.floor(Math.random() * (insults.length))];
}
class Block {
  constructor(health, x, y, isInvincible, isLoot, isScaffolding, host, type) {
    this.damage = this.damage.bind(this);
    this.damagedRecent = false;
    this.x = x;
    this.y = y;
    this.maxHealth = health;
    this.health = health;
    this.type = type;
    this.isInvincible = isInvincible;
    this.isLoot = isLoot;
    this.isScaffolding = isScaffolding;
  }
  damage(s, host) {
    if (this.health == Infinity) {
      return;
    }
    host.update.block = true;
    clearTimeout(this.damageTimeout);
    this.damagedRecent = true;
    this.damageTimeout = setTimeout(function(host) {
      this.damagedRecent = false;
      host.update.block = true;
    }.bind(this), 1000, host);
    this.health -= s.damage;
    if (this.health <= 0) {
      this.destroy(host);
    }
  }
  destroy(host) {
    host.b.splice(host.b.indexOf(this), 1);
  }
}
class Shot { // done
  constructor(x, y, id, xm, ym, type, team, originalRotation, rank, username) { // soon: change this to an options object and extract all the values from it
    this.distance = 0;
    this.username = username;
    this.particleFrame = 0;
    this.rot = originalRotation;
    this.team = team;
    this.addx = x;
    this.addy = y;
    this.type = type;
    this.xm = xm;
    this.ym = ym;
    while (this.xm * this.xm + this.ym * this.ym > 1.2 || this.xm * this.xm + this.ym * this.ym < 1) {
      if (this.xm * this.xm + this.ym * this.ym > 1.1) {
        this.xm /= 1.01;
        this.ym /= 1.01;
      }
      if (this.xm * this.xm + this.ym * this.ym < 1.1) {
        this.xm *= 1.01;
        this.ym *= 1.01;
      }
    }
    this.xm *= 10;
    this.ym *= 10;
    if (this.type == 'grapple') {
      this.xm *= 2;
      this.ym *= 2;
    } else if (this.type == 'shotgun_bullet') {
      this.xm *= .9;
      this.ym *= .9;
    } else if (this.type == 'condensed_bullet') {
      this.xm *= 1.1;
      this.ym *= 1.1;
    }
    this.x1 = 20; // get point (x1, y1) on line that intersects circle
    this.y1 = (this.ym / this.xm) * this.x1;
    this.x2 = -20; // get point (x2, y2) on line that intersects circle
    this.y2 = (this.ym / this.xm) * this.x2;
    this.dx = this.x2 - this.x1;
    this.dy = this.y2 - this.y1;
    this.dr = Math.sqrt(this.dx * this.dx + this.dy * this.dy);
    this.d = this.x1 * this.y2 - this.x2 * this.y1;
    if (this.dy < 0) {
      this.sgn = -1;
    } else {
      this.sgn = 1;
    }
    // evil math stuff:
    this.x_1 = (this.d * this.dy + this.sgn * this.dx * Math.sqrt(1225 * (this.dr * this.dr) - (this.d * this.d))) / (this.dr * this.dr);
    this.x_2 = (this.d * this.dy - this.sgn * this.dx * Math.sqrt(1225 * (this.dr * this.dr) - (this.d * this.d))) / (this.dr * this.dr);
    this.y_1 = (-this.d * this.dx + Math.abs(this.dy) * Math.sqrt(1225 * (this.dr * this.dr) - (this.d * this.d))) / (this.dr * this.dr);
    this.y_2 = (-this.d * this.dx - Math.abs(this.dy) * Math.sqrt(1225 * (this.dr * this.dr) - (this.d * this.d))) / (this.dr * this.dr);
    //this.y_1 *= -1; //convert from normal graph to canvas graph
    //this.y_2 *= -1;
    if (this.ym >= 0) {
      this.x = this.x_1 + this.addx;
      this.y = this.y_1 + this.addy;
    } else {
      this.x = this.x_2 + this.addx;
      this.y = this.y_2 + this.addy;
    }
    if (xm == 0 && ym == 1) {
      this.x = 0 + this.addx;
      this.y = 35 + this.addy;
    } else if (xm == -1 && ym == 0) {
      this.x = -35 + this.addx;
      this.y = 0 + this.addy;
    } else if (xm == 0 && ym == -1) {
      this.x = 0 + this.addx;
      this.y = -35 + this.addy;
    } else if (xm == 1 && ym == 0) {
      this.x = 35 + this.addx;
      this.y = 0 + this.addy;
    }
    this.counter = 0;
    this.id = id;
    this.startx = this.x;
    this.starty = this.y;
    if (this.type == 'bullet') {
      this.damage = 20;
      this.bounces = Infinity;
    } else if (this.type == 'powermissle') {
      this.xm *= 2;
      this.ym *= 2;
      this.damage = 100;
      this.bounces = 0;
    } else if (this.type == 'megamissle') {
      this.xm *= 2;
      this.ym *= 2;
      this.damage = 200;
      this.bounces = 0;
    } else if (this.type == 'shotgun_bullet') {
      this.damage = 25;
      this.bounces = 0;
    } else if (this.type == 'condensed_bullet') {
      this.damage = 5;
      this.bounces = 0;
    } else if (this.type == 'grapple') {
      this.damage = 0;
      this.bounces = 0;
    }
    this.damage *= 1+.02*rank;
    this.ownerRank = rank;
  }
  spotIdiot(host) {
    // right now only the player is an option
    var possibleTargets = [];
    // If player is Celestial or this tank is a summoned tank, change the target spotting to the reverse :)
    var l = 0;
    while (l<host.pt.length) {
      if (host.pt[l].username != this.username) {
        possibleTargets.push({
          x: host.pt[l].x+20, // aim at the center of target tank >:D
          y: host.pt[l].y+20,
        });
      }
      l++;
    }
    // add more targets here >:D (like spefically Celestial);
    var l = 0;
    while (l < possibleTargets.length) {
      var distance = Math.sqrt(Math.pow(this.x - possibleTargets[l].x, 2) + Math.pow(this.y - possibleTargets[l].y, 2));
      possibleTargets[l].distance = distance;
      l++;
    }
    possibleTargets.sort(function(a, b) {
      return a.distance - b.distance;
    });
    var success = false;
    if (possibleTargets.length != 0) {
      success = true;
    }
    this.target = possibleTargets[0];
    return success;
  }
  fireCalc(rotation) {
    if (rotation < 0) {
      rotation = 360 + rotation;
    }
    if (rotation >= 360) {
      rotation = rotation - 360;
    }
    var xd = 1;
    var angle = rotation % 90;
    var yd = Math.abs(((xd * angle) - (90 * xd)) / angle);
    if (rotation < 90 || rotation > 270) {
      yd = Math.abs(yd);
    } else {
      yd = 0 - Math.abs(yd);
    }
    if (rotation > 180) {
      xd = Math.abs(xd);
    } else {
      xd = 0 - Math.abs(xd);
    }
    if (rotation == 0) {
      xd = 0;
      yd = 1;
    } else if (rotation == 90) {
      xd = -1;
      yd = 0;
    } else if (rotation == 180) {
      xd = 0;
      yd = -1;
    } else if (rotation == 270) {
      xd = 1;
      yd = 0;
    }
    return {
      xd: xd,
      yd: yd
    };
  }
  update(host, id) {
    if (this.type == 'powermissle' || this.type == 'megamissle') {
      var success = this.spotIdiot(host);
      if (success) {
        var xdis = this.target.x - this.x;
        var ydis = this.target.y - this.y;
        if (Math.sqrt(Math.pow(xdis, 2)+Math.pow(ydis, 2)) < 300) {
          // in range
          var rotation = 360 - Math.atan2(xdis, ydis) * (180 / Math.PI);
          rotation -= 360;
          if (rotation < 0) {
            rotation = 360 + rotation;
          }
          /*if (rotation < this.rot) {
            rotation = this.rot - 5;
          } else if (rotation > this.rot) {
            rotation = this.rot + 5;
          }*/
          var data = this.fireCalc(rotation);
          if ((data.xd < 0 && data.yd < 0) || (data.xd > 0 && data.yd > 0)) {
            this.xm = data.yd;
            this.ym = data.xd;
          } else {
            this.xm = data.xd;
            this.ym = data.yd;
          }
          while (this.xm * this.xm + this.ym * this.ym > 1.2 || this.xm * this.xm + this.ym * this.ym < 1) {
            if (this.xm * this.xm + this.ym * this.ym > 1.1) {
              this.xm /= 1.01;
              this.ym /= 1.01;
            }
          if (this.xm * this.xm + this.ym * this.ym < 1.1) {
              this.xm *= 1.01;
              this.ym *= 1.01;
            }
          }
          this.xm *= 20; // x2 cause rookets
          this.ym *= 20;
          this.rot = rotation;
        }
      }
    }
    host.update.bullet = true; // bullet update
    host.update.tank = true; // tank update
    if (this.particleFrame == 4) {
      this.particleFrame = 0;
    } else {
      this.particleFrame++;
    }
    if (bulletCollision(this, host)[0]) {
      host.s.splice(id, 1);
      return;
    }
    this.x += this.xm;
    var xresults = bulletCollision(this, host);
    if (xresults[0]) {
      if (this.bounces > 0 && xresults[3]) {
        this.x -= this.xm;
        this.xm = -this.xm;
        this.rot = 180-this.rot;
        this.bounces--;
      } else {
        // destroy bullet
        host.s.splice(id, 1);
        return;
      }
    }
    this.y += this.ym;
    var yresults = bulletCollision(this, host);
    if (yresults[0]) {
      if (this.bounces > 0 && yresults[3]) {
        this.y -= this.ym;
        this.ym = -this.ym;
        this.rot = 180-this.rot;
        this.bounces--;
      } else {
        // destroy bullet
        host.s.splice(id, 1);
        return;
      }
    }
    this.distance += Math.sqrt(Math.pow(this.xm, 2)+Math.pow(this.ym, 2));
    if (this.type == 'shotgun_bullet') {
      this.damage = 25 - (this.distance / 200) * 25;
      this.damage *= 1+.02*this.ownerRank;
      if (this.distance >= 150) {
        host.s.splice(id, 1);
      }
    } else if (this.type == 'condensed_bullet') {
      var distance = Math.sqrt(Math.abs(this.x - this.startx) * Math.abs(this.x - this.startx) + Math.abs(this.y - this.starty) * Math.abs(this.y - this.starty));
      this.damage = 5 + (distance / 500) * 25;
      this.damage *= 1+.02*this.ownerRank;
      if (this.distance >= 3000) {
        host.s.splice(id, 1);
      }
    } else {
      if (this.distance >= 3000) {
        host.s.splice(id, 1);
      }
    }
  }
}

function ai_check(x, y, isBlock, host) {
  var t = 40;
  if (isBlock) t = 50;
  var l = 0;
  while (l < host.s.length) {
    if (host.s[l].x > x || host.s[l].x + 5 > x) {
      if (host.s[l].x < x + t || host.s[l].x + 5 < x + t) {
        if (host.s[l].y > y || host.s[l].y + 5 > y) {
          if (host.s[l].y < y + t || host.s[l].y + 5 < y + t) {
            var team = host.s[l].team;
            var damage = host.s[l].damage;
            var grappleData = { grappled: false };
            if (host.s[l].type == 'grapple') {
              grappleData = {
                grappled: true,
                startx: host.s[l].startx+host.s[l].xm*4,
                starty: host.s[l].starty+host.s[l].ym*4,
              }
            }
            delete host.s[l];
            host.s.splice(l, 1);
            return [true, damage, team, grappleData];
          }
        }
      }
    }
    if (host.s[l].x < 0 || host.s[l].x > 1500 || host.s[l].y < 0 || host.s[l].y > 1500) {
      delete host.s[l];
      host.s.splice(l, 1);
    }
    l++;
  }
  return false;
}

function bulletCollision(bullet, host) {
  // returns [is_hit: true or false, team, username, isBouncy]
  var x = bullet.x;
  var y = bullet.y;
  var team = bullet.team;
  var firer = bullet.username;
  var collided = false;
  var isBouncy = false;
  var victim;
  // Blocks
  var l = 0;
  while (l < host.b.length) {
    if ((x > host.b[l].x*50 || x + 5 > host.b[l].x*50) && (x < host.b[l].x*50 + 50 || x + 5 < host.b[l].x*50 + 50)) {
      if ((y > host.b[l].y*50 || y + 5 > host.b[l].y*50) && (y < host.b[l].y*50 + 50 || y + 5 < host.b[l].y*50 + 50)) {
        collided = true;
        victim = host.b[l];
        host.b[l].damage(bullet, host);
        if (victim.health == Infinity) {
          isBouncy = true;
        }
      }
    }
    l++;
  }
  // Ai
  /*var l = 0;
  while (l < ai.length) {
    var a;
    if (ai[l].turret) {
      a = 50;
    } else {
      a = 40;
    }
    if ((x > host.ai[l].x || x + 5 > host.ai[l].x) && (x < host.ai[l].x + a || x + 5 < host.ai[l].x + a)) {
      if ((y > host.ai[l].y || y + 5 > host.ai[l].y) && (y < host.ai[l].y + a || y + 5 < host.ai[l].y + a)) {
        collided = true;
        if (team != host.ai[l].team) {
          victim = host.ai[l];
          host.ai[l].damage(bullet);
        }
      }
    }
    l++;
  }*/ // not ready yet :(
  // Player
  var l = 0;
  while (l<host.pt.length) {
    if ((x > host.pt[l].x || x + 5 > host.pt[l].x) && (x < host.pt[l].x + 40 || x + 5 < host.pt[l].x + 40)) {
      if ((y > host.pt[l].y || y + 5 > host.pt[l].y) && (y < host.pt[l].y + 40 || y + 5 < host.pt[l].y + 40)) {
        if (!host.pt[l].serverOverrideActive) {
          collided = true;
          if (team != host.pt[l].team) {
            victim = host.pt[l];
            damagePlayer(bullet, host, l);
          } else {
            isBouncy = true;
          }
        }
      }
    }
    l++;
  }
  if (x < 0 || x > 1500 || y < 0 || y > 1500) {
    return [true, null, null, true];
  }
  return [collided, team, firer, isBouncy];
}

function damagePlayer(s, host, l) {
  if (host.pt[l].serverOverrideActive) {
    return;
  }
  if (host.pt[l].shields > 0) {
    host.pt[l].shields -= 1;
  } else if (host.pt[l].immune) {} else {
    host.pt[l].health -= s.damage;
    host.pt[l].invis = false;
    host.pt[l].damagedRecent = true;
    clearTimeout(host[host.pt[l].username + '_damageTimer']);
    host[host.pt[l].username + '_damageTimer'] = setTimeout(function(username, host) {
      var l = 0,
        id;
      while (l < host.pt.length) {
        if (host.pt[l].username == username) {
          id = l;
        }
        l++;
      }
      if (id != undefined) host.pt[id].damagedRecent = false;
    }, 500, host.pt[l].username, host);
    if (host.pt[l].health <= 0) {
      host.pt[l].ded = true;
      host.pt[l].dedTime = new Date();
      host.pt[l].kills = 0;
      var murderer = s.username;
      console.log(host.pt[l].username + ' was Evanismed by ' + murderer);
      chatSockets.forEach(function(s) {
        s.send(JSON.stringify({
          type: 'chat',
          message: '<span style="color: darkblue;">' + host.pt[l].username + ' was Evanismed by ' + murderer,
          send: '[SERVER]:',
          timestamp: Date.now(),
          server: 'public',
        }));
      });
      // Action on death
      if (host.gamemode == 'FFA') {
        var q = 0;
        while (q < host.pt.length) {
          if (host.pt[q].username == s.username) {
            host.pt[q].kills++;
            var o = 0;
            while (o < host.sockets.length) {
              if (host.sockets[o].username == host.pt[q].username) {
                host.sockets[o].send(JSON.stringify({
                  event: 'kill'
                }));
              }
              o++;
            }
          }
          q++;
        }
        host.pt[l].ded = false;
        host.pt[l].serverOverrideActive = true;
        host.pt[l].invis = true;
        host.pt[l].canTakeThermalDamage = false;
        host.pt[l].isThermal = false;
        var q = 0;
        while (q < host.sockets.length) {
          if (host.sockets[q].username == host.pt[l].username) {
            host.sockets[q].send(JSON.stringify({
              event: 'override',
              data: {
                key: 'invis',
                value: true,
                key2: 'spectator',
                value2: true,
              }
            }));
          }
          q++;
        }
        setTimeout(function(username, host) {
          var l = 0,
            id;
          while (l < host.pt.length) {
            if (host.pt[l].username == username) {
              id = l;
            }
            l++;
          }
          if (id != undefined) {
            host.pt[id].serverOverrideActive = false;
            host.pt[id].invis = false;
            host.pt[id].ded = false;
            host.pt[id].shields += 10;
            var q = 0;
            while (q < host.sockets.length) {
              if (host.sockets[q].username == host.pt[id].username) {
                host.sockets[q].send(JSON.stringify({
                  event: 'override',
                  data: {
                    key: 'invis',
                    value: false,
                    key2: 'spectator',
                    value2: false,
                  }
                }));
              }
              q++;
            }
            host.pt[id].health = host.pt[id].maxHealth;
          }
        }, 10000, host.pt[l].username, host);
      } else if (host.gamemode == 'DUELS') {
        if (host.gamestate == 1) { // this should ALWAYS be true, but unless some hacker kills himself while waiting for players.....
          var q = 0,
            winner;
          while (q < host.pt.length) {
            if (host.pt[q].username != host.pt[l].username) {
              winner = host.pt[q].username;
            }
            q++;
          }
          var insult = generateInsult().replace('{1}', host.pt[l].username).replace('{2}', winner);
          var q = 0;
          while (q < host.sockets.length) {
            host.sockets[q].send(JSON.stringify({
              event: 'gameover',
              data: {
                winner: winner,
                loser: host.pt[l].username,
                message: insult,
              },
            }));
            host.sockets[q].destroy(); // End the game
            q++;
          }
        }
      } else if (host.gamemode == 'RAID') {
        // kick the player from the game
      } else if (host.gamemode == 'TDM') {
        host.pt[l].ded = false;
        host.pt[l].serverOverrideActive = true;
        host.pt[l].invis = true;
        host.pt[l].canTakeThermalDamage = false;
        host.pt[l].isThermal = false;
        var q = 0;
        while (q < host.sockets.length) {
          if (host.sockets[q].username == host.pt[l].username) {
            host.sockets[q].send(JSON.stringify({
              event: 'override',
              data: {
                key: 'invis',
                value: true,
                key2: 'spectator',
                value2: true,
              }
            }));
          }
          q++;
        }
        var q = 0;
        while (q < host.teams[host.pt[l].team].length) {
          if (host.teams[host.pt[l].team].username == host.pt[l].username) {
            host.teams[host.pt[l].team].splice(q, 1);
          }
          q++;
        }
        if (host.teams[host.pt[l].team].length == 0) {
          this.gameMessage = 'GAME OVER :D'
        }
        // end game if 0 players
      }
    }
  }
  /*} else if (results[0] && results[2] != this.team && results[3].grappled) {
  host.pt[l].grappled = true;
  host.pt[l].grapplePos = {
    startx: host.pt[l].x,
    starty: host.pt[l].y,
    x: results[3].startx - 20,
    y: results[3].starty - 20,
  }
  clearInterval(host.pt[l].grappleInterval);
  host.pt[l].grappleInterval = setInterval(function(host) {
    if (this.x > this.grapplePos.x) {
      if (this.x - this.grapplePos.x < 20) {
        if (checker(this.x - (this.x - this.grapplePos.x), this.y, host)) {
          this.x -= this.x - this.grapplePos.x;
        }
      } else {
        if (checker(this.x - 20, this.y, host)) {
          this.x -= 20;
        }
      }
    } else if (this.x < this.grapplePos.x) {
      if (this.grapplePos.x - this.x < 20) {
        if (checker(this.x + (this.grapplePos.x - this.x), this.y, host)) {
          this.x += this.grapplePos.x - this.x;
        }
      } else {
        if (checker(this.x + 20, this.y, host)) {
          this.x += 20;
        }
      }
    }
    if (this.y > this.grapplePos.y) {
      if (this.y - this.grapplePos.y < 20) {
        if (checker(this.x, this.y - (this.y - this.grapplePos.y), host)) {
          this.y -= this.y - this.grapplePos.y;
        }
      } else {
        if (checker(this.x, this.y - 20, host)) {
          this.y -= 20;
        }
      }
    } else if (this.y < this.grapplePos.y) {
      if (this.grapplePos.y - this.y < 20) {
        if (checker(this.x, this.y + (this.grapplePos.y - this.y), host)) {
          this.y += this.grapplePos.y - this.y;
        }
      } else {
        if (checker(this.x, this.y + 20, host)) {
          this.y += 20;
        }
      }
    }
    var m = 0;
    while (m < host.sockets.length) {
      if (host.sockets[m].username == this.username) {
        host.sockets[m].send(JSON.stringify({
          event: 'override',
          data: {
            key: 'x',
            value: this.x,
            key2: 'y',
            value2: this.y,
          }
        }));
      }
      m++;
    }
    if (this.x == this.grapplePos.x && this.y == this.grapplePos.y) {
      this.grappled = false;
      clearInterval(this.grappleInterval);
    }
  }.bind(host.pt[l]), 20, host);*/
}

function levelReader(array, m, host) {
  var l, q;
  for (l = 0; l < array.length; l++) {
    for (q = 0; q < array[l].split("").length; q++) {
      var p = array[l].split(""); // Block key: # = invincible, 1 = weak, 2 = strong, @ = player, A = ai
      if (p[q] == "#") {
        wall(q, l, m, host);
      } else if (p[q] == "1") {
        weak(q, l, m, host);
      } else if (p[q] == "2") {
        strong(q, l, m, host);
      } else if (p[q] == "A") {
        if (quad) {
          //createAi((q - 10) * 50, (l - 10) * 50, m, 3, 3);
        } else {
          //createAi(q * 50, l * 50, m, 3, 3);
        }
      } else if (p[q] == '@') {
        host.spawn.x = q*50;
        host.spawn.y = l*50;
      }
    }
  }
}

function weak(x, y, m, host) {
  if (m) {
    host.b.push(new Block(80, x, y, false, false, false, host, 'weak'));
  }
  // check if block hit maybe? this loops
}

function strong(x, y, m, host) {
  if (m) {
    host.b.push(new Block(120, x, y, false, false, false, host, 'strong'));
  }
  // loops
}

function gold(x, y, m, host) {
  if (m) {
    host.b.push(new Block(600, x, y, false, false, false, host, 'gold'));
  }
}

function wall(x, y, m, host) {
  if (m) {
    host.b.push(new Block(Infinity, x, y, true, false, false, host, 'wall'));
  }
}

function thermal_check(x, y, username, host) {
  var l = 0;
  while (l < host.pt.length) {
    if (host.pt[l].isThermal) {
      if ((x + 45 >= host.pt[l].x - 5 && x + 40 <= host.pt[l].x + 5 + 40) || (x >= host.pt[l].x - 5 && x <= host.pt[l].x + 5 + 40)) {
        if ((y + 40 >= host.pt[l].y - 5 && y + 40 <= host.pt[l].y + 5 + 40) || (y >= host.pt[l].y - 5 && y <= host.pt[l].y + 5 + 40)) {
          if (username != host.pt[l].username) return [true, host.pt[l].team];
        }
      }
    }
    l++;
  }
  return [false];
}

function checker2(x, y, host) {
  var l = 0,
    targets = [];
  while (l < host.b.length) {
    if ((x + 40 > host.b[l].x * 50 && x + 40 < host.b[l].x * 50 + 50) || (x > host.b[l].x * 50 && x < host.b[l].x * 50 + 50)) {
      if ((y > host.b[l].y * 50 && y < host.b[l].y * 50 + 50) || (y + 40 > host.b[l].y * 50 && y + 40 < host.b[l].y * 50 + 50)) {
        targets.push(l);
      }
    }
    l++;
  }
  return targets;
} 

class Ai {
    constructor(type, fireType, isInvis, turretFireType, team) {
      if (type == 'turret') {
        this.turret = true;
      } else {
        this.turret = false;
      }
      this.team = team
      this.fireType = fireType;
      this.sawUser = false;
      this.instructions = {};
      this.type = type;
      this.rotation = 0;
      this.base = 0;
      this.pushback = 0;
      this.fire = this.fire.bind(this);
      if (!this.turret) {
        if (fireType == 0) {
          this.health = 60;
        } else if (fireType == 1) {
          this.health = 200;
        } else if (fireType == 2) {
          this.health = 320;
        } else if (fireType == 3) {
          this.health = 250;
        }
      } else { // Turrets have x3 health
        if (fireType == 1) {
          this.health = 600;
        }
      }
      this.maxHealth = this.health;
      this.inactive = false;
      this.update = true;
      this.leftright = false;
      this.canShoot = true;
      this.canTakeThermalDamage = true;
    }
    draw() {
      if (this.inactive != true) {
        if (this.leftright) {
          if (!this.turret) {
            draw.translate(this.x + 20, this.y + 20);
          } else {
            draw.translate(this.x + 25, this.y + 25);
          }
          draw.rotate(90 * Math.PI / 180);
          if (this.base == 1) {
            if (!this.turret) {
              if (this.fireType == 0) {
                draw.drawImage(stupid_tank_base, -20, -20)
              } else if (this.fireType == 1) {
                draw.drawImage(tank_base_png, -20, -20);
              } else if (this.fireType == 2) {
                draw.drawImage(area_tank_base, -20, -20);
              } else if (this.fireType == 3) {
                draw.drawImage(condensed_tank_base, -20, -20);
              }
            } else {
              draw.drawImage(ai_turret_base, -25, -25, 50, 50);
            }
          } else {
            if (!this.turret) {
              if (this.fireType == 0) {
                draw.drawImage(stupid_tank_base2, -20, -20);
              } else if (this.fireType == 1) {
                draw.drawImage(tank_base2, -20, -20);
              } else if (this.fireType == 2) {
                draw.drawImage(area_tank_base2, -20, -20);
              } else if (this.fireType == 3) {
                draw.drawImage(condensed_tank_base2, -20, -20);
              }
            } else {
              draw.drawImage(ai_turret_base, -25, -25, 50, 50);
            }
          }
          draw.rotate(-90 * Math.PI / 180);
          if (!this.turret) {
            draw.translate((-this.x - 20), (-this.y - 20));
          } else {
            draw.translate((-this.x - 25), (-this.y - 25));
          }
        } else {
          if (this.base == 1) {
            if (!this.turret) {
              if (this.fireType == 0) {
                draw.drawImage(stupid_tank_base, this.x, this.y);
              } else if (this.fireType == 1) {
                draw.drawImage(tank_base_png, this.x, this.y);
              } else if (this.fireType == 2) {
                draw.drawImage(area_tank_base, this.x, this.y);
              } else if (this.fireType == 3) {
                draw.drawImage(condensed_tank_base, this.x, this.y);
              }
            } else {
              draw.drawImage(ai_turret_base, this.x, this.y, 50, 50);
            }
          } else {
            if (!this.turret) {
              if (this.fireType == 0) {
                draw.drawImage(stupid_tank_base2, this.x, this.y);
              } else if (this.fireType == 1) {
                draw.drawImage(tank_base2, this.x, this.y);
              } else if (this.fireType == 2) {
                draw.drawImage(area_tank_base2, this.x, this.y);
              } else if (this.fireType == 3) {
                draw.drawImage(condensed_tank_base2, this.x, this.y);
              }
            } else {
              draw.drawImage(ai_turret_base, this.x, this.y, 50, 50);
            }
          }
        }
        if (!this.turret) {
          draw.translate(this.x + 20, this.y + 20);
        } else {
          draw.translate(this.x + 25, this.y + 25);
        }
        draw.rotate(this.rotation * Math.PI / 180);
        if (!this.turret) {
          if (this.fireType == 0) {
            draw.drawImage(stupid_tank_top, -20, -20+this.pushback);
          } else if (this.fireType == 1) {
            draw.drawImage(ai_top, -20, -20 + this.pushback);;
          } else if (this.fireType == 2) {
            draw.drawImage(area_tank_top, -20, -20 + this.pushback);
          } else if (this.fireType == 3) {
            draw.drawImage(condensed_tank_top, -20, -20 + this.pushback);
          }
        } else {
          if (this.fireType == 1) {
            draw.drawImage(ai_top, -20, -20+this.pushback);
          }
        }
        if (this.pushback != 0) {
          this.pushback += 1;
        }
        draw.rotate(-(this.rotation * Math.PI / 180));
        if (!this.turret) {
          draw.translate(-this.x - 20, -this.y - 20);
        } else {
          draw.translate(-this.x - 25, -this.y - 25);
        }
        draw.fillStyle = '#000000'; 
        draw.fillRect(this.x, this.y + 50, 40, 5);
        draw.fillStyle = '#FF0000';
        draw.fillRect(this.x + 2, this.y + 51, 36 * this.health / this.maxHealth, 3);
      }
    }
    move() {
      if (this.grappled) {
        if (this.x > this.grapplePos.x) {
          if (this.x-this.grapplePos.x<20) {
            if (checker(this.x-(this.x-this.grapplePos.x), this.y, {x: this.x, y: this.y})) {
              this.x -= this.x-this.grapplePos.x;
            } else {
              this.grappled = false;
            }
          } else {
            if (checker(this.x-20, this.y, {x: this.x, y: this.y})) {
              this.x -= 20;
            } else {
              this.grappled = false;
            }
          }
        } else if (this.x < this.grapplePos.x) {
          if (this.grapplePos.x-this.x<20) {
            if (checker(this.x+(this.grapplePos.x-this.x), this.y, {x: this.x, y: this.y})) {
              this.x += this.grapplePos.x-this.x;
            } else {
              this.grappled = false;
            }
          } else {
            if (checker(this.x+20, this.y, {x: this.x, y: this.y})) {
              this.x += 20;
            } else {
              this.grappled = false;
            }
          }
        }
        if (this.y > this.grapplePos.y) {
          if (this.y-this.grapplePos.y<20) {
            if (checker(this.x, this.y-(this.y-this.grapplePos.y), {x: this.x, y: this.y})) {
              this.y -= this.y-this.grapplePos.y;
            } else {
              this.grappled = false;
            }
          } else {
            if (checker(this.x, this.y-20, {x: this.x, y: this.y})) {
              this.y -= 20;
            } else {
              this.grappled = false;
            }
          }
        } else if (this.y < this.grapplePos.y) {
          if (this.grapplePos.y-this.y<20) {
            if (checker(this.x, this.y+(this.grapplePos.y-this.y), {x: this.x, y: this.y})) {
              this.y += this.grapplePos.y-this.y;
            } else {
              this.grappled = false;
            }
          } else {
            if (checker(this.x, this.y+20, {x: this.x, y: this.y})) {
              this.y += 20;
            } else {
              this.grappled = false;
            }
          }
        }
        if (this.x == this.grapplePos.x && this.y == this.grapplePos.y) {
          this.grappled = false;
        }
        return;
      }
      if (!this.inactive) {
        var remap = false;
        this.xdistance = this.seeUserX - this.x;
        this.ydistance = this.seeUserY - this.y;
        if (this.xdistance == 0) {
          this.xdistance = 1;
        }
        this.rotation = Math.round(Math.atan(this.ydistance / this.xdistance) * 180 / Math.PI);
        if (this.xdistance > 0) {
          this.rotation -= 90;
        } else {
          this.rotation += 90;
        }
        if (this.seeUser) {
          this.rotation += 1;
          this.seeUserX = user.tank.x;
          this.seeUserY = user.tank.y
        }
        if (this.instructions.movement) { // check if movement is a valid value
          // followpath
          // include block break checker
          if (this.instructions.movement[0] == 2) {
            if (checker(this.x, this.y-5, {x: this.x, y: this.y})) {
              this.y -= 5;
            } else {
              remap = true;
            }
          }
          if (this.instructions.movement[0] == 1) {
            if (checker(this.x, this.y+5, {x: this.x, y: this.y})) {
              this.y += 5;
            } else {
              remap = true;
            }
          }
          if (this.instructions.movement[0] == 3) {
            if (checker(this.x-5, this.y, {x: this.x, y: this.y})) {
              this.x -= 5;
            } else {
              remap = true;
            }
          }
          if (this.instructions.movement[0] == 4) {
            if (checker(this.x+5, this.y, {x: this.x, y: this.y})) {
              this.x += 5;
            } else {
              remap = true;
            }
          }
          if (this.instructions.movement[0] == 5) {
            if (checker(this.x-5, this.y-5, {x: this.x, y: this.y})) {
              this.x -= 5;
              this.y -= 5;
            } else {
              remap = true;
            }
          }
          if (this.instructions.movement[0] == 6) {
            if (checker(this.x+5, this.y-5, {x: this.x, y: this.y})) {
              this.x += 5;
              this.y -= 5;
            } else {
              remap = true;
            }
          }
          if (this.instructions.movement[0] == 7) {
            if (checker(this.x-5, this.y+5, {x: this.x, y: this.y})) {
              this.x -= 5;
              this.y += 5;
            } else {
              remap = true;
            }
          }
          if (this.instructions.movement[0] == 8) {
            if (checker(this.x+5, this.y+5, {x: this.x, y: this.y})) {
              this.x += 5;
              this.y += 5;
            } else {
              remap = true;
            }
          }
          if (remap) {
            var AiPosX = [];
            AiPosX.push(((this.x) - ((this.x) % 50))/50);
            AiPosX.push(((this.x+40) - ((this.x+40) % 50))/50);
            var AiPosY = [];
            AiPosY.push(((this.y) - ((this.y) % 50))/50);
            AiPosY.push(((this.y+40) - ((this.y+40) % 50))/50);
            if (AiPosX[0] == 30) {
              AiPosX[0] = 29;
            }
            if (AiPosX[1] == 30) {
              AiPosX[1] = 29;
            }
            if (AiPosY[0] == 30) {
              AiPosX[0] = 29;
            }
            if (AiPosY[1] == 30) {
              AiPosX[1] = 29;
            }
            if (AiPosX[0] == -1) {
              AiPosX[0] = 0;
            }
            if (AiPosX[1] == -1) {
              AiPosX[1] = 0;
            }
            if (AiPosY[0] == -1) {
              AiPosX[0] = 0;
            }
            if (AiPosY[1] == -1) {
              AiPosX[1] = 0;
            }
            var pathfind = true;
            var l = 0;
            while (l<AiPosX.length) {
              if (AiPosX[l] == this.instructions.AiPosX-1 && checker(this.x, this.y-5, {x: this.x, y: this.y})) {
                this.x += 5;
              }
              if (AiPosX[l] == this.instructions.AiPosX+1 && checker(this.x, this.y+5, {x: this.x, y: this.y})) {
                this.x -= 5;
              }
              l++;
            }
            var l = 0;
            while (l<AiPosY.length) {
              if (AiPosY[l] == this.instructions.AiPosY-1 && checker(this.x-5, this.y, {x: this.x, y: this.y})) {
                this.y += 5;
              }
              if (AiPosY[l] == this.instructions.AiPosY+1 && checker(this.x+5, this.y, {x: this.x, y: this.y})) {
                this.y -= 5;
              }
              l++;
            }
          }
        }
        if (this.instructions.sawUser) {
          // followpath
          // include block breaker
        }
      }
    }
    pathfind() {
      if (!this.inactive) {
        this.instructions = this.moveCalc();
      }
    }
    shoot(instance) {
      if (!instance.inactive) {
        if (instance.seeUser) {
          instance.fire();
        }
      }
    }
    fire() {
      this.pushback = -3;
      var xd = (this.x + 20) - (user.tank.x + 20);
      var yd = (this.y + 20) - (user.tank.y + 20);
      if (xd < 0) {
        var neg = false;
      } else {
        var neg = true;
      }
      if (xd == 0) {
        xd = 1;
      }
      yd = yd / xd;
      xd = 1;
      if (neg) {
        xd = 0 - 1;
        yd = 0 - yd;
      }
      var resize = 0;
      if (this.turret) resize = 5; 
      if (this.fireType == 1) {
        s.push(new Shot(this.x + 20+resize, this.y + 20+resize, s.length - 1, xd, yd, 'bullet', this.team, this.rotation, this.username));
      } else if (this.fireType == 2) {
        s.push(new Shot(this.x + 20+resize, this.y + 20+resize, s.length - 1, xd, yd, 'shotgun_bullet', this.rotation, this.username));
        s.push(new Shot(this.x + 20+resize, this.y + 20+resize, s.length - 1, xd+xd*.6, yd, 'shotgun_bullet', this.team, this.rotation, this.username));
        s.push(new Shot(this.x + 20+resize, this.y + 20+resize, s.length - 1, xd, yd-yd*.6, 'shotgun_bullet', this.team, this.rotation, this.username));
        s.push(new Shot(this.x + 20+resize, this.y + 20+resize, s.length - 1, xd, yd+yd*.9, 'shotgun_bullet', this.team, this.rotation, this.username));
        s.push(new Shot(this.x + 20+resize, this.y + 20+resize, s.length - 1, xd-xd*.9, yd, 'shotgun_bullet', this.team, this.rotation, this.username));
      } else if (this.fireType == 3) {
        s.push(new Shot(this.x + 20+resize, this.y + 20+resize, s.length - 1, xd, yd, 'condensed_bullet', this.team, this.rotation, this.username));
        setTimeout(function(x, y, team, rotation, username) {
          s.push(new Shot(x + 20+resize, y + 20+resize, s.length - 1, xd, yd, 'condensed_bullet', team, rotation, username));
          setTimeout(function(x, y, team, rotation, username) {
            s.push(new Shot(x + 20+resize, y + 20+resize, s.length - 1, xd, yd, 'condensed_bullet', team, rotation, username));
            setTimeout(function(x, y, team, rotation, username) {
              s.push(new Shot(x + 20+resize, y + 20+resize, s.length - 1, xd, yd, 'condensed_bullet', team, rotation, username));
              setTimeout(function(x, y, team, rotation, username) {
                s.push(new Shot(x + 20+resize, y + 20+resize, s.length - 1, xd, yd, 'condensed_bullet', team, rotation, username));
              }, 20, x, y, team, rotation, username);
            }, 20, x, y, team, rotation, username);
          }, 20, x, y, team, rotation, username);
        }, 20, this.x, this.y, this.team, this.rotation, this.username);
      }
    }
    check() {
      if (userData.kit == 'thermal') {
        if (thermal_check(this.x, this.y) && this.canTakeThermalDamage) {
          this.health -= 20;
          if (this.health <= 0) {
            var max, min;
            if (this.fireType == 0) {
              max = 50;
              min = 0;
            } else if (this.fireType == 1) {
              max = 200;
              min = 100;
            } else if (this.fireType == 2) {
              max = 500;
              min = 200;
            } else if (this.fireType == 3) {
              max = 500;
              min = 200;
            }
            if (userData.kit == 'scavenger') {
              Game.coins += 2*(Math.floor(Math.random() * max) + min);
            } else {
              Game.coins += Math.floor(Math.random() * max) + min;
            }
            Game.foes--;
            this.x = undefined;
            this.y = undefined;
            this.inactive = true;
            if (Game.foes == 0) {
              document.exitFullscreen();
              if (Game.level < 10000) {
                setTimeout(victory, 200);
                Game.endGame();
              }
            }
          }
          this.canTakeThermalDamage = false;
          setTimeout(function (tank) {
            tank.canTakeThermalDamage = true;
          }, 1000, this);
        }
      }
      var results = ai_check(this.x, this.y, this.turret);
      if (results[0] && results[2] != this.team && !results[3].grappled) {
        if (this.inactive != true) {
          draw.fillStyle = '#FF0000';
          draw.fillRect(this.x, this.y, 40, 40);
          this.health -= results[1];
          this.sawUser = true; // detects user if shot
          if (this.health <= 0) {
            var max, min;
            if (this.fireType == 0) {
              max = 50;
              min = 0;
            } else if (this.fireType == 1) {
              max = 200;
              min = 100;
            } else if (this.fireType == 2) {
              max = 500;
              min = 200;
            } else if (this.fireType == 3) {
              max = 500;
              min = 200;
            }
            if (userData.kit == 'scavenger') {
              Game.coins += 2*(Math.floor(Math.random() * max) + min);
            } else {
              Game.coins += Math.floor(Math.random() * max) + min;
            }
            Game.foes--;
            this.x = undefined;
            this.y = undefined;
            this.inactive = true;
            if (Game.foes == 0) {
              document.exitFullscreen();
              if (Game.level < 10000) {
                setTimeout(victory, 200);
                Game.endGame();
              }
            }
          }
        }
      } else if (results[0] && results[2] != this.team && results[3].grappled) {
        this.grappled = true;
        this.grapplePos = {
          startx: this.x,
          starty: this.y,
          x: results[3].startx-20,
          y: results[3].starty-20,
        }
      }
    }
    moveCalc() {
      this.seeUser = false;
      var AiPosX = ((this.x+20) - ((this.x+20) % 50))/50;
      var AiPosY = ((this.y+20) - ((this.y+20) % 50))/50;
      var PlayerPosX = [];
      PlayerPosX.push(((user.tank.x) - ((user.tank.x) % 50))/50);
      PlayerPosX.push(((user.tank.x) - ((user.tank.x) % 50))/50);
      PlayerPosX.push(((user.tank.x+40) - ((user.tank.x+40) % 50))/50);
      PlayerPosX.push(((user.tank.x+40) - ((user.tank.x+40) % 50))/50);
      var PlayerPosY = [];
      PlayerPosY.push(((user.tank.y) - ((user.tank.y) % 50))/50);
      PlayerPosY.push(((user.tank.y+40) - ((user.tank.y+40) % 50))/50);
      PlayerPosY.push(((user.tank.y) - ((user.tank.y) % 50))/50);
      PlayerPosY.push(((user.tank.y+40) - ((user.tank.y+40) % 50))/50);
      if (PlayerPosX[0] == 30) {
        PlayerPosX[0] = 29;
        PlayerPosX[1] = 29;
      }
      if (PlayerPosX[2] == 30) {
        PlayerPosX[2] = 29;
        PlayerPosX[3] = 29;
      }
      if (PlayerPosY[0] == 30) {
        PlayerPosY[0] = 29;
        PlayerPosY[2] = 29;
      }
      if (PlayerPosY[1] == 30) {
        PlayerPosY[1] = 29;
        PlayerPosY[3] = 29;
      }
      if (PlayerPosX[0] == -1) {
        PlayerPosX[0] = 0;
        PlayerPosX[1] = 0;
      }
      if (PlayerPosX[2] == -1) {
        PlayerPosX[2] = 0;
        PlayerPosX[3] = 0;
      }
      if (PlayerPosY[0] == -1) {
        PlayerPosY[0] = 0;
        PlayerPosY[2] = 0;
      }
      if (PlayerPosY[1] == -1) {
        PlayerPosY[1] = 0;
        PlayerPosY[3] = 0;
      }
      var seeUser;
      var k = 0; 
      //var startSeeUser = new Date();
      while (k<4) {
        seeUser = true;
        if (PlayerPosY[k] - AiPosY == 0) { // needs work 
          var slope = 0;
          var yInt = AiPosY;
        } else if (PlayerPosX[k] - AiPosX == 0) {
          var slope = 'v'; // needs work
        } else {
          var slope = (PlayerPosY[k] - AiPosY)/(PlayerPosX[k] - AiPosX);
          var yInt = -(slope*AiPosX-AiPosY);
        }
        if (slope == 'v') {
          if (PlayerPosY[k] > AiPosY) {
            var l = AiPosY;
            while (l<PlayerPosY[k]) {
              if (l >= 0) {
                if (l <= 30) {
                  if (Game.map[l][PlayerPosX[k]] == '1') {
                    seeUser = false;
                  }
                }
              }
              l++;
            }
          } else {
            var l = PlayerPosY[k];
            while (l<AiPosY) {
              if (l >= 0) {
                if (l <= 30) {
                  if (Game.map[l][AiPosX] == '1') {
                    seeUser = false;
                  }
                }
              }
              l++;
            }
          }
        } else if (slope == 0) {
          if (PlayerPosX[k] > AiPosX) {
            var l = AiPosX;
            while (l<PlayerPosX[k]) {
              if (l >= 0) {
                if (l <= 30) {
                  if (Game.map[AiPosY][l] == '1') {
                    seeUser = false;
                  }
                }
              }
              l++;
            }
          } else {
            var l = PlayerPosX[k];
            while (l<AiPosX) {
              if (l >= 0) {
                if (l <= 30) {
                  if (Game.map[PlayerPosY[k]][l] == '1') {
                    seeUser = false;
                  }
                }
              }
              l++;
            }
          }
        } else {
          if (PlayerPosX[k] > AiPosX) {
            var l = AiPosX;
            while (l<PlayerPosX[k]) {
              var y = slope*l+yInt;
              if (y >= 0 && l >= 0) {
                if (Math.round(l) < 30 && Math.round(y) < 30) {
                  if (Game.map[Math.round(y)][Math.round(l)] == '1') {
                    seeUser = false;
                  }
                }
              }
              l+=.1;
            }
          } else {
            var l = PlayerPosX[k];
            while (l<AiPosX) {
              var y = slope*l+yInt;
              if (y >= 0 && l >= 0) {
                if (Math.round(l) < 30 && Math.round(y) < 30) {
                  if (Game.map[Math.round(y)][Math.round(l)] == '1') {
                    seeUser = false;
                  }
                }
              }
              l+=.1;
            }
          }
        }
        if ((seeUser && !user.tank.invis) || (user.tank.invis && this.sawUser)) {
          this.seeUser = true;
        }
        seeUser = this.seeUser;
        if (seeUser) {
          this.sawUser = true;
        }
        k++
      }
      if (!this.turret) {
        PlayerPosX = ((user.tank.x+20) - ((user.tank.x+20) % 50))/50;
        PlayerPosY = ((user.tank.y+20) - ((user.tank.y+20) % 50))/50;
        var xdistance = Math.abs((user.tank.x+20)-(this.x+20));
        var ydistance = Math.abs((user.tank.y+20)-(this.y+20));
        var distance = Math.sqrt((xdistance*xdistance)+(ydistance*ydistance));
        if (seeUser || (this.sawUser && distance <= 600)) {
          var pathfind = [{
            x: AiPosX,
            y: AiPosY,
            route: [],
            old: [],
            complete: false,
            step: 0,
            color: generateRandomColor(),
          }];
          var complete = false;
          while (!complete) {
            complete = true;
            var l = 0;
            while (l<pathfind.length) {
              if (!pathfind[l].complete) {
                var action = false;
                var spawnNew = false;
                var down = true, up = true, left = true, right = true;
                var obsticale = false;
                var current = JSON.parse(JSON.stringify(pathfind[l]));
                if (current.y+1 < 31 && Math.abs(current.y+1-PlayerPosY) < Math.abs(current.y-PlayerPosY)) {
                  if (Game.map[current.y+1][current.x] == '1') {
                    obsticale = true;
                  }
                }
                if (current.y-1 > -1 && Math.abs(current.y-1-PlayerPosY) < Math.abs(current.y-PlayerPosY)) {
                  if (Game.map[current.y-1][current.x] == '1') {
                    obsticale = true;
                  }
                }
                if (current.x+1 < 31 && Math.abs(current.x+1-PlayerPosX) < Math.abs(current.x-PlayerPosX)) {
                  if (Game.map[current.y][current.x+1] == '1') {
                    obsticale = true;
                  }
                }
                if (current.x-1 > -1 && Math.abs(current.x-1-PlayerPosX) < Math.abs(current.x-PlayerPosX)) {
                  if (Game.map[current.y][current.x-1] == '1') {
                    obsticale = true;
                  } 
                }
                var q = 0;
                while (q<current.old.length) {
                  if (current.x == current.old[q].x) {
                    if (current.y+1 == current.old[q].y) {
                      down = false;
                    }
                  }
                  q++;
                }
                var q = 0;
                while (q<current.old.length) {
                  if (current.x == current.old[q].x) {
                    if (current.y-1 == current.old[q].y) {
                      up = false;
                    }
                  }
                  q++;
                }
                var q = 0;
                while (q<current.old.length) {
                  if (current.x-1 == current.old[q].x) {
                    if (current.y == current.old[q].y) {
                      left = false;
                    }
                  }
                  q++;
                }
                var q = 0;
                while (q<current.old.length) {
                  if (current.x+1 == current.old[q].x) {
                    if (current.y == current.old[q].y) {
                      right = false;
                    }
                  }
                  q++;
                }
                var optDown = false, optUp = false, optLeft = false, optRight = false;
                var cantDownRight = false, cantDownLeft = false, cantUpRight = false, cantUpLeft = false;
                if (current.y+1 < 31 && (Math.abs(current.y+1-PlayerPosY) < Math.abs(current.y-PlayerPosY) || obsticale)) {
                  if (Game.map[current.y+1][current.x] == '0' && down) {
                    optDown = true;
                  }
                }
                if (current.y-1 > -1 && (Math.abs(current.y-1-PlayerPosY) < Math.abs(current.y-PlayerPosY) || obsticale)) {
                  if (Game.map[current.y-1][current.x] == '0' && up) {
                    optUp = true;
                  }
                }
                if (current.x+1 < 31 && (Math.abs(current.x+1-PlayerPosX) < Math.abs(current.x-PlayerPosX) || obsticale)) {
                  if (Game.map[current.y][current.x+1] == '0' && right) {
                    optRight = true;
                  }
                }
                if (current.x-1 > -1 && (Math.abs(current.x-1-PlayerPosX) < Math.abs(current.x-PlayerPosX) || obsticale)) {
                  if (Game.map[current.y][current.x-1] == '0' && left) {
                    optLeft = true;
                  }
                }
                if (optDown && optRight && current.x+1 < 31 && current.y+1 < 31 && !obsticale) {
                  if (Game.map[current.y+1][current.x+1] == '0') {
                    if (spawnNew) {
                      var n = JSON.parse(JSON.stringify(current));
                      n.old.push({x:n.x, y:n.y});
                      n.route.push(8);
                      n.x += 1;
                      n.y += 1;
                      n.color = generateRandomColor();
                      n.step++;
                      pathfind.push(n);
                      draw.beginPath();
                      draw.strokeStyle = n.color;
                      draw.moveTo((n.x-1)*50, (n.y-1)*50);
                      draw.lineTo(n.x*50, n.y*50);
                      draw.stroke();
                    } else {
                      pathfind[l].old.push({x:pathfind[l].x, y:pathfind[l].y});
                      pathfind[l].route.push(8);
                      pathfind[l].x += 1;
                      pathfind[l].y += 1;
                      pathfind[l].step++;
                      spawnNew = true;
                      draw.beginPath();
                      draw.strokeStyle = pathfind[l].color;
                      draw.moveTo((pathfind[l].x-1)*50, (pathfind[l].y-1)*50);
                      draw.lineTo(pathfind[l].x*50, pathfind[l].y*50);
                      draw.stroke();
                    } 
                    action = true;
                  } else {
                    cantDownRight = true;
                  }
                } else {
                  cantDownRight = true;
                }
                if (optLeft && optUp && current.y-1 > -1 && current.x-1 > -1 && !obsticale) {
                  if (Game.map[current.y-1][current.x-1] == '0') {
                    if (spawnNew) {
                      var n = JSON.parse(JSON.stringify(current));
                      n.old.push({x:n.x, y:n.y});
                      n.route.push(5);
                      n.x -= 1;
                      n.y -= 1;
                      n.color = generateRandomColor();
                      n.step++;
                      pathfind.push(n);
                      draw.beginPath();
                      draw.strokeStyle = n.color;
                      draw.moveTo((n.x+1)*50, (n.y+1)*50);
                      draw.lineTo(n.x*50, n.y*50);
                      draw.stroke();
                    } else {
                      pathfind[l].old.push({x:pathfind[l].x, y:pathfind[l].y});
                      pathfind[l].route.push(5);
                      pathfind[l].x -= 1;
                      pathfind[l].y -= 1;
                      pathfind[l].step++;
                      spawnNew = true;
                      draw.beginPath();
                      draw.strokeStyle = pathfind[l].color;
                      draw.moveTo((pathfind[l].x+1)*50, (pathfind[l].y+1)*50);
                      draw.lineTo(pathfind[l].x*50, pathfind[l].y*50);
                      draw.stroke();
                    }
                    action = true;
                  } else {
                    cantUpLeft = true;
                  }
                } else {
                  cantUpLeft = true;
                }
                if (optRight && optUp && current.y-1 > -1 && current.x+1 < 31 && !obsticale) {
                  if (Game.map[current.y-1][current.x+1] == '0') {
                    if (spawnNew) {
                      var n = JSON.parse(JSON.stringify(current));
                      n.old.push({x:n.x, y:n.y});
                      n.route.push(6);
                      n.y -= 1;
                      n.x += 1;
                      n.color = generateRandomColor();
                      n.step++;
                      pathfind.push(n);
                      draw.beginPath();
                      draw.strokeStyle = n.color;
                      draw.moveTo((n.x-1)*50, (n.y+1)*50);
                      draw.lineTo(n.x*50, n.y*50);
                      draw.stroke();
                    } else {
                      pathfind[l].old.push({x:pathfind[l].x, y:pathfind[l].y});
                      pathfind[l].route.push(6);
                      pathfind[l].y -= 1;
                      pathfind[l].x += 1;
                      pathfind[l].step++;
                      spawnNew = true;
                      draw.beginPath();
                      draw.strokeStyle = pathfind[l].color;
                      draw.moveTo((pathfind[l].x-1)*50, (pathfind[l].y+1)*50);
                      draw.lineTo(pathfind[l].x*50, pathfind[l].y*50);
                      draw.stroke();
                    }
                    action = true;
                  } else {
                    cantUpRight = true;
                  }
                } else {
                  cantUpRight = true;
                }
                if (optDown && optLeft && current.y+1 < 31 && current.x-1 > -1 && !obsticale) {
                  if (Game.map[current.y+1][current.x-1] == '0') {
                    if (spawnNew) {
                      var n = JSON.parse(JSON.stringify(current));
                      n.old.push({x:n.x, y:n.y});
                      n.route.push(7);
                      n.y += 1;
                      n.x -= 1;
                      n.color = generateRandomColor();
                      n.step++;
                      pathfind.push(n);
                      draw.beginPath();
                      draw.strokeStyle = n.color;
                      draw.moveTo((n.x+1)*50, (n.y-1)*50);
                      draw.lineTo(n.x*50, n.y*50);
                      draw.stroke();
                    } else {
                      pathfind[l].old.push({x:pathfind[l].x, y:pathfind[l].y});
                      pathfind[l].route.push(7);
                      pathfind[l].y += 1;
                      pathfind[l].x -= 1;
                      pathfind[l].step++;
                      spawnNew = true;
                      draw.beginPath();
                      draw.strokeStyle = pathfind[l].color;
                      draw.moveTo((pathfind[l].x+1)*50, (pathfind[l].y-1)*50);
                      draw.lineTo(pathfind[l].x*50, pathfind[l].y*50);
                      draw.stroke();
                    }
                    action = true;
                  } else {
                    cantDownLeft = true;
                  }
                } else {
                  cantDownLeft = true;
                }
                if ((optDown && !optRight && !optLeft) || (optDown && ((cantDownRight && cantDownLeft) || obsticale))) {
                  if (spawnNew) {
                    var n = JSON.parse(JSON.stringify(current));
                    n.old.push({x:n.x, y:n.y});
                    n.route.push(1);
                    n.y++;
                    n.color = generateRandomColor();
                    n.step++;
                    pathfind.push(n);
                    draw.beginPath();
                    draw.strokeStyle = n.color;
                    draw.moveTo((n.x)*50, (n.y-1)*50);
                    draw.lineTo(n.x*50, n.y*50);
                    draw.stroke();
                  } else {
                    pathfind[l].old.push({x:pathfind[l].x, y:pathfind[l].y});
                    pathfind[l].route.push(1);
                    pathfind[l].y++;
                    pathfind[l].step++;
                    spawnNew = true;
                    draw.beginPath();
                    draw.strokeStyle = pathfind[l].color;
                    draw.moveTo((pathfind[l].x)*50, (pathfind[l].y-1)*50);
                    draw.lineTo(pathfind[l].x*50, pathfind[l].y*50);
                    draw.stroke();
                  }
                  action = true;
                }
                if ((optUp && !optRight && !optLeft) || (optUp && ((cantUpRight && cantUpLeft) || obsticale))) {
                  if (spawnNew) {
                    var n = JSON.parse(JSON.stringify(current));
                    n.old.push({x:n.x, y:n.y});
                    n.route.push(2);
                    n.y -= 1;
                    n.color = generateRandomColor();
                    n.step++;
                    pathfind.push(n);
                    draw.beginPath();
                    draw.strokeStyle = n.color;
                    draw.moveTo((n.x)*50, (n.y+1)*50);
                    draw.lineTo(n.x*50, n.y*50);
                    draw.stroke();
                  } else {
                    pathfind[l].old.push({x:pathfind[l].x, y:pathfind[l].y});
                    pathfind[l].route.push(2);
                    pathfind[l].y -= 1;
                    pathfind[l].step++;
                    spawnNew = true;
                    draw.beginPath();
                    draw.strokeStyle = pathfind[l].color;
                    draw.moveTo((pathfind[l].x)*50, (pathfind[l].y+1)*50);
                    draw.lineTo(pathfind[l].x*50, pathfind[l].y*50);
                    draw.stroke();
                  }
                  action = true;
                }
                if ((optLeft && !optDown && !optUp) || ( optLeft && ((cantDownLeft && cantUpLeft) || obsticale))) {
                  if (spawnNew) {
                    var n = JSON.parse(JSON.stringify(current));
                    n.old.push({x:n.x, y:n.y});
                    n.route.push(3);
                    n.x -= 1;
                    n.color = generateRandomColor();
                    n.step++;
                    pathfind.push(n);
                    draw.beginPath();
                    draw.strokeStyle = n.color;
                    draw.moveTo((n.x+1)*50, (n.y)*50);
                    draw.lineTo(n.x*50, n.y*50);
                    draw.stroke();
                  } else {
                    pathfind[l].old.push({x:pathfind[l].x, y:pathfind[l].y});
                    pathfind[l].route.push(3);
                    pathfind[l].x -= 1;
                    pathfind[l].step++;
                    spawnNew = true;
                    draw.beginPath();
                    draw.strokeStyle = pathfind[l].color;
                    draw.moveTo((pathfind[l].x+1)*50, (pathfind[l].y)*50);
                    draw.lineTo(pathfind[l].x*50, pathfind[l].y*50);
                    draw.stroke();
                  }
                  action = true;
                }
                if ((optRight && !optDown && !optUp) || (optRight && ((cantDownRight && cantUpRight) || obsticale))) {
                  if (spawnNew) {
                    var n = JSON.parse(JSON.stringify(current));
                    n.old.push({x:n.x, y:n.y});
                    n.route.push(4);
                    n.x += 1;
                    n.color = generateRandomColor();
                    n.step++;
                    pathfind.push(n);
                    draw.beginPath();
                    draw.strokeStyle = n.color;
                    draw.moveTo((n.x-1)*50, (n.y)*50);
                    draw.lineTo(n.x*50, n.y*50);
                    draw.stroke();
                  } else {
                    pathfind[l].old.push({x:pathfind[l].x, y:pathfind[l].y});
                    pathfind[l].route.push(4);
                    pathfind[l].x += 1;
                    pathfind[l].step++;
                    spawnNew = true;
                    draw.beginPath();
                    draw.strokeStyle = pathfind[l].color;
                    draw.moveTo((pathfind[l].x-1)*50, (pathfind[l].y)*50);
                    draw.lineTo(pathfind[l].x*50, pathfind[l].y*50);
                    draw.stroke();
                  } 
                  action = true;
                }
                var q = 0;
                while (q<pathfind.length) {
                  if (pathfind[q].x == PlayerPosX && pathfind[q].y == PlayerPosY) {
                    pathfind[q].complete = true;
                  }
                  if (pathfind[q].step > 15) { // Limit of 15 blocks for pathfinding
                    delete pathfind[q];
                    pathfind.splice(q, 1);
                  } // pathfind length limit
                  q++;
                }
                if (!action) {
                  delete pathfind[l];
                  pathfind.splice(l, 1);
                } 
              }
              l++;
            }
            var q = 0;
            while (q<pathfind.length) {
              if (!pathfind[q].complete) {
                complete = false;
              }
              q++;
            } 
              /*if (doPathfindTracing) {
              var l = 0;
              while (l<pathfind.length) {
                var o = 1;
                draw.strokeStyle = "green";
                while (o<pathfind[l].old.length) {
                  draw.beginPath();
                  draw.lineWidth = '3';
                  draw.moveTo(pathfind[l].old[o-1].x*50+25, pathfind[l].old[o-1].y*50+25);
                  draw.lineTo(pathfind[l].old[o].x*50+25, pathfind[l].old[o].y*50+25);
                  draw.stroke();
                  draw.closePath();
                  o++;
                }
                draw.beginPath();
                draw.lineTo(pathfind[l].x+25, pathfind[l].y+25);
                draw.stroke();
                draw.closePath();
                l++;
              }
            //}*/
          }
          //var pathfindEndTime = new Date();
          //var seeUserTime = endSeeUser.getMilliseconds() - startSeeUser.getMilliseconds();
          //var pathfindTime = pathfindEndTime.getMilliseconds() - startPathfind.getMilliseconds();
          //saveStatus.innerHTML = 'SeeUser Time: '+seeUserTime+' pathfindTime: '+pathfindTime+' with '+iterations+' iterations';
          pathfind.sort(function(a, b) {
            return a.step - b.step;
          });
          if (pathfind.length != 0) {
            return {seeUser: seeUser, sawUser: this.sawUser, movement: pathfind[0].route, old: pathfind[0].old, AiPosX: AiPosX, AiPosY: AiPosY};
          } else {
            return {seeUser: seeUser, sawUser: this.sawUser, movement: [], old: [], AiPosX: AiPosX, AiPosY: AiPosY}
          }
        }
      }
      return { seeUser: seeUser, sawUser: this.sawUser, movement: []}
    }
  }

function checker(x, y, host) {
  return true;
  var l = 0;
  while (l < host.b.length) {
    if ((x + 40 > host.b[l].x * 50 && x + 40 < host.b[l].x * 50 + 50) || (x > host.b[l].x * 50 && x < host.b[l].x * 50 + 50)) {
      if ((y > host.b[l].y * 50 && y < host.b[l].y * 50 + 50) || (y + 40 > host.b[l].y * 50 && y + 40 < host.b[l].y * 50 + 50)) {
        return false;
      }
    }
    l++;
  }
  return true;
}

core.use(Router);


const PORT = 80;
//const main = express();
//main.use(Sentry.Handlers.requestHandler());
//main.use(vhost('lunar69.ml', core));
//main.use(vhost('*.lunar69.ml', core));
//const server = https.createServer(options, core);
core.listen(PORT);
console.log('[SERVER] => Listening on port '+PORT);
