const SETTINGS = {
  use_existing_node_server: true, // Set this to false unless you have your own nodejs web server using port 80
  path: '/ffa-server', // Path you will need to connect to (e.g) example.com/ffa-server instead of example.com (use / for no path)
  bans: [],
  max_players_per_room: 5, // Number of player per room before the server creates another one.
  log_strain: false, // recommended false unless you suffer from lag
  fps_boost: false, // Can lag clients and servers. Recommened false.
  UPS: 60, // Updates per second to be sent to clients (ONLY USED if fps_boost is set to false)
}

import HyperExpress from 'hyper-express';
import util from 'util';

const FFA_SERVER = new HyperExpress.Router();
var sockets = [];
var servers = [];
var incoming_per_second = 0;
var outgoing_per_second = 0;

if (SETTINGS.log_strain) {
  setInterval(() => {
    console.log('Incoming: '+incoming_per_second+' | Outgoing: '+outgoing_per_second);
    incoming_per_second = 0;
    outgoing_per_second = 0;
  }, 1000);
}

FFA_SERVER.ws(SETTINGS.path, {
  compression: HyperExpress.compressors.DEDICATED_COMPRESSOR,
  idleTimeout: Infinity,
  maxBackpressure: 1,
}, (socket) => {
  sockets.push(socket);
  socket.on('message', (data) => {
    incoming_per_second++;
    data = JSON.parse(data);
    if (!socket.username) {
      socket.username = data.username;
    }
    if (data.type === 'join') {
      var l = 0;  
      if (servers.length === 0) {
        servers.push(new FFA());
      }
      var l = 0;
      while (l<servers.length) {
        if (servers[l].pt.length !== 10) {
          socket.room = l;
          servers[l].add(socket, data.tank);
        }
        l++;
      }
      if (socket.room === undefined) {
        var temp = new FFA();
        socket.room = servers.length;
        temp.add(socket, data.tank);
        servers.push(temp);
      }
    } else if (data.type === 'update') {
      servers[socket.room].update(data);
    } else if (data.type === 'ping') {
      socket.send(JSON.stringify({
        event: 'ping',
        id: data.id,
      }));
    } else if (data.type === 'chat') {
      var l = 0;
      while (l<servers[socket.room].pt.length) {
        if (servers[socket.room].pt[l].u === socket.username) {
          servers[socket.room].logs.unshift({
            m: '['+socket.username+'] '+data.msg,
            c: '#ffffff',
          });
        }
        servers[socket.room].pt[l].updates.logs = true;
        l++;
      }
    } else {
      socket.send('{"status":"error", "message":"This is a FFA server. Invalid data received: '+JSON.stringify(data)+'"}');
      setTimeout(() => {
        socket.destroy();
      }, 20);
    }
  });
  socket.on('close', (code, reason) => {
    servers[socket.room].disconnect(socket, code, reason);
  });
});

class FFA {
  constructor(id) {
    this.sockets = [];
    this.spawn = {
      x: 0,
      y: 0,
    }
    this.id = id; // may remove
    this.logs = [];
    this.ai = []; // ai
    this.b = []; // blocks
    this.s = []; // bullets
    this.pt = []; // players
    this.d = []; // damage entities
    this.i = [];
    this.t = [];

    /*this.levelReader([
      '               ===============',
      ' ============= ================',
      ' =            #############===',
      ' =    2       ## 2       1#===',
      ' =           2## #    1   ##===',
      ' =    11  2   ## #          ==',
      ' =   1##1     ## # 1      # ==',
      ' =2  1##1   2 ## #      1 # ==',
      ' =    112     ## ###22##### ==',
      ' =           0##   #  #1111 ==',
      ' =   2      00##2# #  #     ==',
      ' =###    2 000#111#111# 1111==',
      '1111#     00002111#111#    1==',
      '1111#    000@0#111#111#111 1==',
      '==#1####11##########11####2#==',
      '==#1####11##########11####1#==',
      '==12111#111#111#         #1111',
      '==1    #111#1112    2  2 #1111',
      '==1111 #111#111# 2       ###= ',
      '==     #  # #2##            = ',
      '== 1111#  #   ##      2     = ',
      '== #####22### ##  2   11    = ',
      '== # 1      # ##     1##1 2 = ',
      '== #      1 # ##     1##1   = ',
      '==          # ##   2  11    = ',
      '==##   1    # ##        2   = ',
      '===#1       2 ##    2      2= ',
      '===#############            = ',
      '=============== ============= ',
      '===============               ']);*/

      //this.levelReader(['     #     #  ==   #   1   1  ','  #  #  #  #  == #  2#####2## ','  #  #  #  #  == #####      # ','#2#####2####  ==  2#    # #  1','              ==##2###2## ####','              == 1 #      1   ','  #   2222### == # #1# #### #2',' ##   12221   ==     #  2 ### ',' ##   12221   ==#### #### 1   ','  #22 #22##   ==  2#1   #######',' ##12 #####   == # # #1 #     ','   12         == #      # # # ','11 1#        2==2##1##### 2 # ','   1#       222222     1 2# # ','=============2@ 2=============','=============2  2=============','       1    222222     #      ',' #1########1#2==2       111   ',' #     1   2# ==   11      11 ','1# ######## #1==  12211     1 ',' # 1 1   21 # ==   111   2    ',' # # #### # # ==   1    #2    ',' #1# 111# # # ==        22    ',' # # #111 #1# ==####   #2#    ',' # # #### # # ==              ',' # 12   1 1 # ==  2  2  # # # ','1# ######## #1==  1  1  ##### ',' #2    1    # ==        #   # ',' #1########1# ==  1  1      # ','       1      ==   11   #   # ']); OLD

      //this.levelReader(['2     =    #     #  #  =     2',' ==2=    # #  #  #  #    =2== ',' =  = =  #    #     #  = =  = ',' 2 == =  #### # #####  = == 2 ',' ==== =  #    #     #  = ==== ','      1  # #     #  #  1      ','= ===11  # # ### # ##  11=== =','         # #     #  #         ','  222#   ############         ',' 22222#                ###### ',' 22222#   2  2222  2   #  # # ',' 22222#                #  # # ',' 22222#     # 11 #     # ## # ',' 22222#   2  1111  2   #    # ',' 22222#   2 11@ 11 2   # ## # ',' 22222#   2 11  11 2   #  # # ',' 22222#   2  1111  2   #  # # ',' 22222#     # 11 #     #### # ',' 22222#                     # ',' 22222#   2  2222  2   ## # # ',' 22222#                #  # # ','  222#   ############  ###### ','         #    ##    #         ','= ===11  # #      # #  11=== =','      1  #    ##    #  1      ',' ==== =  #    ##    #  = ==== ',' 2 == =  #    ##    #  = == 2 ',' =  = =  #    ##    #  = =  = ',' ==2=    # #      # #    =2== ','2     =  #    ##       =     2']); BRADLET LEVEL

        this.levelReader(['========   #      #   =========','======     2      2     =======','====11     #      #     11=====','===111      #    #      111====','==111#########22#########111==','==11##   #          #   ##11==','=   # #                ###   =','=   #  2    #    #    ####   =','    #   2            # 1 #    ','    #    2          #  # #    ','    # 1   #        ##1## #    ','#2# #      #####2##   #  # #2#','   ##      2222222#1### ###   ','    #      #2    22 #    #    ','    2  1   #2 @  2######1#    ','    2    1 #2    22    # 2    ','    #      #2    2####1# #    ','   ## #    #222222# #  # ##   ','#2# #      #####2## #1##1# #2#','    #     1        ##  # #    ','    #   1     1     ##1# #    ','    #    #        1  #   #    ','=   #          #      ####   =','=   ### 1  1        1  ###   =','==11###                 ##11==','==111#########22#########111==','===111      #    #      111===','====11     #      #     11====','======     2      2     ======','========   #      #   ========']);


    if (!SETTINGS.fps_boost) {
      this.i.push(setInterval(this.send.bind(this), 1000/SETTINGS.UPS));
    }
    this.i.push(setInterval(this.tick.bind(this), 1000/80));
  }

  tick() {

    var l = 0;
    while (l<this.ai.length) {
      if (this.ai[l].p !== 0) {
        this.ai[l].p += 0.25;
      }
      this.ai[l].update();
      l++;
    }
    
    var l = 0;
    while (l<this.s.length) {
      this.s[l].update();
      l++;
    }

    var l = 0;
    while (l<this.pt.length) {
      var q = 0;
      while (q<this.pt.length) {
        if ((this.pt[l].x > this.pt[q].x || this.pt[l].x+40 > this.pt[q].x) && (this.pt[l].x < this.pt[q].x+40 || this.pt[l].x+40 < this.pt[q].x+40)) {
          if ((this.pt[l].y > this.pt[q].y || this.pt[l].y+40 > this.pt[q].y) && (this.pt[l].y < this.pt[q].y+40 || this.pt[l].y+40 < this.pt[q].y+40)) {
            if (this.pt[q].i && this.pt[q].c === 'warrior' && this.pt[l].u !== this.pt[q].u && this.pt[l].canBashed) {
              this.pt[l].h -= 50;
              this.pt[l].canBashed = false;
              setTimeout(function() {
                this.canBashed = true;
              }.bind(this.pt[l]), 500);
            }
          }
        }
        q++;
      }
      l++;
    }

    var l = 0;
    while (l<this.pt.length) {
      if (this.pt[l].p != 0) {
        this.triggerUpdate(this.pt[l].x, this.pt[l].y, 'tank');
        this.pt[l].p += 0.25;
      }
      var q = 0;
      while (q<this.b.length) {
        if ((this.pt[l].x > this.b[q].x || this.pt[l].x+40 > this.b[q].x) && (this.pt[l].x < this.b[q].x+50 || this.pt[l].x+40 < this.b[q].x+50)) {
          if ((this.pt[l].y > this.b[q].y || this.pt[l].y+40 > this.b[q].y) && (this.pt[l].y < this.b[q].y+50 || this.pt[l].y+40 < this.b[q].y+50)) {
            if (this.b[q].t === 'spike' && !this.pt[l].ded && !this.pt[l].i) {
              this.pt[l].h -= 1;
              this.triggerUpdate(this.pt[l].x, this.pt[l].y, 'tank');
              if (this.pt[l].d !== false) {
                clearTimeout(this.pt[l].d.t);
                this.pt[l].d.d += 1;
                this.pt[l].d.x = this.pt[l].x;
                this.pt[l].d.y = this.pt[l].y;
                this.pt[l].d.t = setTimeout(function() {
                  this.d = false;
                }.bind(this.pt[l]), 1000);
              } else {
                this.pt[l].d = {
                  d: 1,
                  x: this.pt[l].x,
                  y: this.pt[l].y,
                  t: setTimeout(function() {
                    this.d = false;
                  }.bind(this.pt[l]), 1000),
                };
              }         
              if (this.pt[l].h <= 0) {
                this.pt[l].ded = true;
                this.logs.unshift({
                  m: this.pt[l].u + ' was skewered on a spike.',
                  c: '#FF8C00',
                });
                var m = 0;
                while (m<this.pt.length) {
                  this.pt[m].updates.logs = true;
                  m++;
                }
                var x = this.pt[l].x;
                var y = this.pt[l].y;
                setTimeout(function() {
                  this.socket.send(JSON.stringify({
                    event: 'ded',
                  }));
                  this.socket.send(JSON.stringify({
                    event: 'override',
                    data: [{
                      key: 'x',
                      value: x,
                    }, {
                      key: 'y',
                      value: y,
                    }],
                  }));
                  this.ded = false;
                  this.h = this.ma;
                }.bind(this.pt[l]), 10000);
              }
              break;
            }
          }
        }
        q++;
      }
      var q = 0;
      while (q<this.pt[l].d.length) {
        this.pt[l].d[q].y -= .2;
        this.triggerUpdate(this.pt[l].x, this.pt[l].y, 'tank');
        q++;
      }
      if (this.pt[l].grapple) {
        var mpf =  5;
        var d = Math.sqrt(Math.pow(this.pt[l].grapple.x-(this.pt[l].x+20), 2)+Math.pow(this.pt[l].grapple.y-(this.pt[l].y+20), 2));
        var x = this.pt[l].x+(this.pt[l].grapple.x-(this.pt[l].x+20))*(mpf/d);
        var y = this.pt[l].y+(this.pt[l].grapple.y-(this.pt[l].y+20))*(mpf/d);
        if (d<mpf) {
          x = this.pt[l].grapple.x-20;
          y = this.pt[l].grapple.y-20;
        }
        var mx = false, my = false;
        if (!this.collision(x, this.pt[l].y)) {
          x = this.pt[l].x;
          mx = true;
        }
        if (!this.collision(this.pt[l].x, y)) {
          y = this.pt[l].y;
          my = true;
        }
        if (Math.round(x+20) === Math.round(this.pt[l].grapple.x)) {
          mx = true;
        }
        if (Math.round(y+20) === Math.round(this.pt[l].grapple.y)) {
          my = true;
        }
        this.pt[l].x = x;
        this.pt[l].y = y;
        this.pt[l].grapple.bullet.sx = this.pt[l].x+20;
        this.pt[l].grapple.bullet.sy = this.pt[l].y+20;
        this.triggerUpdate(this.pt[l].x+20, this.pt[l].y+20, 'bullet');
        if (mx && my) {
          this.pt[l].grapple.bullet.destroy();
          this.pt[l].grapple = false;
        }
        this.pt[l].socket.send(JSON.stringify({
          event: 'override',
          data: [{
            key: 'x',
            value: x,
          }, {
            key: 'y',
            value: y,
          }],
        }));
      }
      l++;
    }

    var l = 0;
    while (l<this.d.length) {
      if (this.d[l].c) {
        var q = 0;
        while (q<this.pt.length) {
          if ((this.d[l].x > this.pt[q].x || this.d[l].x+this.d[l].w > this.pt[q].x) && (this.d[l].x < this.pt[q].x+40 || this.d[l].x+this.d[l].w < this.pt[q].x+40)) {
            if ((this.d[l].y > this.pt[q].y || this.d[l].y+this.d[l].h > this.pt[q].y) && (this.d[l].y < this.pt[q].y+40 || this.d[l].y+this.d[l].h < this.pt[q].y+40)) {
              if (!this.pt[q].ded && !this.pt[q].i && this.pt[q].u !== this.d[l].o) {
                this.damagePlayer(this.pt[q], this.d[l]);
              }
            }
          }
          q++;
        }
        q = 0;
        while (q<this.b.length) {
          if ((this.d[l].x > this.b[q].x || this.d[l].x+this.d[l].w > this.b[q].x) && (this.d[l].x < this.b[q].x+50 || this.d[l].x+this.d[l].w < this.b[q].x+50)) {
            if ((this.d[l].y > this.b[q].y || this.d[l].y+this.d[l].h > this.b[q].y) && (this.d[l].y < this.b[q].y+50 || this.d[l].y+this.d[l].h < this.b[q].y+50)) {
              if (this.b[q].damage(this.d[l].a) && q !== 0) {
                q--;
              };
            }
          }
          q++;
        }
        q = 0;
        while (q<this.ai.length) {
          if ((this.d[l].x > this.ai[q].x || this.d[l].x+this.d[l].w > this.ai[q].x) && (this.d[l].x < this.ai[q].x+40 || this.d[l].x+this.d[l].w < this.ai[q].x+40)) {
            if ((this.d[l].y > this.ai[q].y || this.d[l].y+this.d[l].h > this.ai[q].y) && (this.d[l].y < this.ai[q].y+40 || this.d[l].y+this.d[l].h < this.ai[q].y+40)) {
              if (this.ai[q].o !== this.d[l].o) {
                this.ai[q].damage(this.d[l].a);
              }
            }
          }
          q++;
        }
      }
      this.d[l].c = false;
      l++;
    }
  }
  
  triggerUpdate(x, y, type) {
    var l = 0;
    while (l<this.pt.length) {
      if ((this.pt[l].x-540 > x || this.pt[l].x+580 > x) && (this.pt[l].x-540 < x+5 || this.pt[l].x+580 < x+5) && (this.pt[l].y-230 > y || this.pt[l].y+270 > y) && (this.pt[l].y-230 < y+5 || this.pt[l].y+270 < y+5)) {
        this.pt[l].updates[type] = true;
      }
      l++;
    }
  }

  collision(x, y) {
    if (x < 0 || y < 0 || x+40 > 1500 || y+40 > 1500) return false;
    var l = 0;
    while (l<this.b.length) {
      if ((x > this.b[l].x || x + 40 > this.b[l].x) && (x < this.b[l].x + 50 || x + 40 < this.b[l].x + 50) && (y > this.b[l].y || y + 40 > this.b[l].y) && (y < this.b[l].y + 50 || y + 40 < this.b[l].y + 50)) {
        return false;
      }
      l++;
    }
    return true;
  }

  levelReader(level) {
    var l, q;
    for (l = 0; l < level.length; l++) {
      for (q = 0; q < level[l].split('').length; q++) {
        var p = level[l].split(''); // Block key: # = invincible, 1 = weak, 2 = strong, @ = player, A = ai
        if (p[q] === '=') {
          this.b.push(new Block(q*50, l*50, Infinity, 'void', this));
        } else if (p[q] === '#') {
          this.b.push(new Block(q*50, l*50, Infinity, 'barrier', this));
        } else if (p[q] === '2') {
          this.b.push(new Block(q*50, l*50, 200, 'strong', this));
        } else if (p[q] === '1') {
          this.b.push(new Block(q*50, l*50, 100, 'weak', this));
        } else if (p[q] === '0') {
          this.b.push(new Block(q*50, l*50, Infinity, 'spike', this));
        } else if (p[q] == '@') {
          this.spawn.x = q*50;
          this.spawn.y = l*50;
        } 
      }
    }
  }
  
  add(socket, data) {
    this.sockets.push(socket);
    data.updates = {
      block: true,
      tank: true,
      bullet: true,
      logs: true,
    };
    data.d = false;
    data.ma = data.h; // ma = max 
    data.s = 100;
    data.col = '#'+Math.floor(Math.random()*16777215).toString(16); // generate random color
    data.socket = socket;
    data.canIn = true; // can change invis status
    data.canBashed = true; // can take warrior class bash attack damage
    socket.send(JSON.stringify({
      event: 'override',
      data: [{
        key: 'x',
        value: this.spawn.x,
      }, {
        key: 'y',
        value: this.spawn.y,
      }],
    }));
    this.pt.push(data);
    this.logs.unshift({
      m: this.joinMsg(data.u),
      c: '#66FF00',
    });
    var l = 0;
    while (l<this.pt.length) {
      this.pt[l].updates.logs = true;
      l++;
    }
  }

  update(data) {
    var tank = data.data;
    var l = 0;
    while (l<this.pt.length) {
      if (this.pt[l].u === data.username) {
        var doTankUpdate = false;
        this.pt[l].br = tank.br;
        this.pt[l].i = tank.i;
        this.pt[l].a = tank.a;
        if (this.pt[l].e !== tank.e) {
          this.pt[l].e = tank.e;
          doTankUpdate = true;
        }
        if (this.pt[l].canIn) this.pt[l].in = tank.in;
        if (!this.pt[l].grapple) {
          if (this.pt[l].x !== tank.x) {
            this.pt[l].x = tank.x;
            doTankUpdate = true;
          }
          if (this.pt[l].y !== tank.y) {
            this.pt[l].y = tank.y;
            doTankUpdate = true;
          }
        }
        if (this.pt[l].r !== tank.r) {
          this.pt[l].r = tank.r;
          doTankUpdate = true;
        }
        if (!this.pt[l].ded) {
          if (this.pt[l].ba !== tank.ba) {
            this.pt[l].ba = tank.ba;
            doTankUpdate = true;
          }
          if (tank.fi.length > 0) {
            this.pt[l].p = -3; // pushback
            var q = 0;
            while (q<tank.fi.length) {
              this.s.push(new Shot(this.pt[l].x+20, this.pt[l].y+20, tank.fi[q].x, tank.fi[q].y, tank.fi[q].t, tank.fi[q].r, this.pt[l].u, this));
              q++;
            }
            if (this.pt[l].in) {
              this.pt[l].canIn = false;
              this.pt[l].in = false;
              setTimeout(function() {
                this.canIn = true;
                this.in = true;
              }.bind(this.pt[l]), 150);
            }
            doTankUpdate = true;
          }
          if (tank.to) {
            if (this.pt[l].healInterval === undefined) {
              this.pt[l].in = false;
              this.pt[l].canIn = false;
              this.pt[l].healInterval = setInterval(function() {
                this.h += 0;
                if (this.h > this.ma) this.h = this.ma;
              }.bind(this.pt[l]), 100);
              this.pt[l].healTimeout = setTimeout(function() {
                this.h = this.ma;
                clearInterval(this.healInterval);
                this.healInterval = undefined;
                this.canIn = true;
              }.bind(this.pt[l]), 7500);
            } else {
              clearInterval(this.pt[l].healInterval);
              clearTimeout(this.pt[l].healTimeout);
              this.pt[l].healInterval = undefined;
              this.pt[l].canIn = true;
            }
            doTankUpdate = true;
          }
          if (tank.ta) {
            this.pt[l].h += this.pt[l].ma*.25;
            this.pt[l].h = Math.min(this.pt[l].ma, this.pt[l].h);
          }
          if (tank.gl) {
            this.pt[l].gluInterval = setInterval(function() {
              this.h += 2;
              this.h = Math.min(this.ma, this.h); 
            }.bind(this.pt[l]), 20);
            this.pt[l].gluTimeout = setTimeout(function() {
              clearInterval(this.gluInterval);
            }.bind(this.pt[l]), 10000);
          }
          if (tank.pl) {
            var key = {
              strong: 200,
              weak: 100,
              gold: 300,
            };
            var hp = key[tank.st];
            if (tank.r >= 337.5 || tank.r < 22.5) {
              this.b.push(new Block(this.pt[l].x-5, this.pt[l].y+40, hp*(1+.02*this.pt[l].ra), tank.st, this));
            }
            if (tank.r >= 22.5 && tank.r < 67.5) {
              this.b.push(new Block(this.pt[l].x-50, this.pt[l].y+40, hp*(1+.02*this.pt[l].ra), tank.st, this));
            }
            if (tank.r >= 67.5 && tank.r < 112.5) {
                this.b.push(new Block(this.pt[l].x-50, this.pt[l].y-5, hp*(1+.02*this.pt[l].ra), tank.st, this));
            }
            if (tank.r >= 112.5 && tank.r < 157.5) {
              this.b.push(new Block(this.pt[l].x-50, this.pt[l].y-50, hp*(1+.02*this.pt[l].ra), tank.st, this));
            }
            if (tank.r >= 157.5 && tank.r < 202.5) {
              this.b.push(new Block(this.pt[l].x-5, this.pt[l].y-50, hp*(1+.02*this.pt[l].ra), tank.st, this));
            }
            if (tank.r >= 202.5 && tank.r < 247.5) {
              this.b.push(new Block(this.pt[l].x+40, this.pt[l].y-50, hp*(1+.02*this.pt[l].ra), tank.st, this));
            }
            if (tank.r >= 247.5 && tank.r < 292.5) {
              this.b.push(new Block(this.pt[l].x+40, this.pt[l].y-5, hp*(1+.02*this.pt[l].ra), tank.st, this));
            }
            if (tank.r >= 292.5 && tank.r < 337.5) {
              this.b.push(new Block(this.pt[l].x+40, this.pt[l].y+40, hp*(1+.02*this.pt[l].ra), tank.st, this));
            }
            var b = this.b[this.b.length-1];
            this.triggerUpdate(b.x, b.y, 'block');
            doTankUpdate = true;
          }
          if (tank.fl) {
            // stun tank
            var q = 0;
            while (q<this.pt.length) {
              if ((this.pt[l].x-230 > this.pt[q].x || this.pt[l].x+270 > this.pt[q].x) && (this.pt[l].x-230 < this.pt[q].x+40 || this.pt[l].x+270 < this.pt[q].x+40) && (this.pt[l].y-230 > this.pt[q].y || this.pt[l].y+270 > this.pt[q].y) && (this.pt[l].y-230 < this.pt[q].y+40 || this.pt[l].y+270 < this.pt[q].y+40) && this.pt[q].u !== this.pt[l].u) {
                this.pt[q].fb = true; // flashbanged
                clearTimeout(this.pt[q].flashbangTimeout);
                this.pt[q].flashbangTimeout = setTimeout(function() {
                  this.fb = false;
                  this.updates.tank = true;
                }.bind(this.pt[q]), 2500);
              }
              q++;
            }
          }
          if (tank.bo) {
            // break blocks
            var q = 0;
            while (q<this.b.length) {
              if ((this.pt[l].x > this.b[q].x || this.pt[l].x+40 > this.b[q].x) && (this.pt[l].x < this.b[q].x+50 || this.pt[l].x+40 < this.b[q].x+50) && (this.pt[l].y > this.b[q].y || this.pt[l].y+40 > this.b[q].y) && (this.pt[l].y < this.b[q].y+50 || this.pt[l].y+40 < this.b[q].y+50)) {
                this.b[q].destroy();
                q--;
              }
              q++;
            }
            this.triggerUpdate(this.pt[l].x, this.pt[l].y, 'block');
            doTankUpdate = true;
          }
          if (tank.po) {
            // break blocks
            var q = 0;
            while (q<this.b.length) {
              if ((this.pt[l].x-40 > this.b[q].x || this.pt[l].x+80 > this.b[q].x) && (this.pt[l].x-40 < this.b[q].x+50 || this.pt[l].x+80 < this.b[q].x+50) && (this.pt[l].y-40 > this.b[q].y || this.pt[l].y+80 > this.b[q].y) && (this.pt[l].y-40 < this.b[q].y+50 || this.pt[l].y+80 < this.b[q].y+50)) {
                this.b[q].destroy();
                q--;
              }
              q++;
            }
            this.triggerUpdate(this.pt[l].x, this.pt[l].y, 'block');
            doTankUpdate = true;
          }
          if (tank.tu) {
            var q = 0, result = 0, first;
            while (q<this.ai.length) {
              if (this.ai[q].o === this.pt[l].u) {
                if (result === 0) first = this.ai[q].o;
                result++;
              }
              q++;
            }
            if (result > 5) {
              this.ai.splice(this.ai.indexOf(first), 1);
            }
            this.ai.push(new Ai(this.pt[l].x, this.pt[l].y, 0, this.pt[l].u, this));
          }
          if (tank.bu) {
            this.pt[l].buff = true;
            setTimeout(function() {
              this.buff = false;
            }.bind(this.pt[l]), 15000);
          }
          if (tank.bui) {
            if (tank.r <= 45 || tank.r > 315) {
              this.b.push(new Block(this.pt[l].x-55, this.pt[l].y+40, 800, 'gold', this));
              this.b.push(new Block(this.pt[l].x-5, this.pt[l].y+40, 800, 'gold', this));
              this.b.push(new Block(this.pt[l].x+45, this.pt[l].y+40, 800, 'gold', this));
            }
            if (tank.r > 45 && tank.r <= 135) {
              this.b.push(new Block(this.pt[l].x-50, this.pt[l].y-55, 800, 'gold', this));
              this.b.push(new Block(this.pt[l].x-50, this.pt[l].y-5, 800, 'gold', this));
              this.b.push(new Block(this.pt[l].x-50, this.pt[l].y+45, 800, 'gold', this));
            }
            if (tank.r > 135 && tank.r <= 225) {
              this.b.push(new Block(this.pt[l].x-55, this.pt[l].y-50, 800, 'gold', this));
              this.b.push(new Block(this.pt[l].x-5, this.pt[l].y-50, 800, 'gold', this));
              this.b.push(new Block(this.pt[l].x+45, this.pt[l].y-50, 800, 'gold', this));
            }
            if (tank.r > 225 && tank.r <= 315) {
              this.b.push(new Block(this.pt[l].x+40, this.pt[l].y-55, 800, 'gold', this));
              this.b.push(new Block(this.pt[l].x+40, this.pt[l].y-5, 800, 'gold', this));
              this.b.push(new Block(this.pt[l].x+40, this.pt[l].y+45, 800, 'gold', this));
            }
            this.triggerUpdate(this.pt[l].x, this.pt[l].y, 'block');
          }
        }
        if (doTankUpdate) {
          this.triggerUpdate(this.pt[l].x, this.pt[l].y, 'tank');
        }
        var render = {
          left: {
            x: Math.floor((this.pt[l].x-230)/50),
            y: Math.floor((this.pt[l].y-230)/50),
          },
          right: {
            x: Math.ceil((this.pt[l].x+270)/50),
            y: Math.ceil((this.pt[l].y+270)/50),
          },
        }
        if (JSON.stringify(render) !== JSON.stringify(this.pt[l].render)) {
          this.pt[l].updates.block = true;
          this.pt[l].render = render;
        }
      }
      l++;
    }
    if (SETTINGS.fps_boost) {
      this.send();
    }
  }

  send() {
    var l = 0;
    while (l<this.pt.length) {
      outgoing_per_second++;

      var message = {};

      if (this.pt[l].updates.block) {
        message.blocks = [];
        var q = 0;
        while (q<this.b.length) {
          if ((this.pt[l].x-610 > this.b[q].x || this.pt[l].x+650 > this.b[q].x) && (this.pt[l].x-610 < this.b[q].x+50 || this.pt[l].x+650 < this.b[q].x+50) && (this.pt[l].y-300 > this.b[q].y || this.pt[l].y+340 > this.b[q].y) && (this.pt[l].y-300 < this.b[q].y+50 || this.pt[l].y+340 < this.b[q].y+50)) {
            message.blocks.push(JSON.parse(JSON.stringify(this.b[q], (name, value) => {
              if (['ho', 'bar'].includes(name)) {
                return undefined;
              } else {
                return value;
              }
            })));
          }
          q++;
        }
        this.pt[l].updates.block = false;
      }
      
      if (this.pt[l].updates.tank) {
        message.tanks = [];
        var q = 0;
        while (q<this.pt.length) {
          if ((this.pt[l].x-610 > this.pt[q].x || this.pt[l].x+650 > this.pt[q].x) && (this.pt[l].x-610 < this.pt[q].x+40 || this.pt[l].x+650 < this.pt[q].x+40) && (this.pt[l].y-300 > this.pt[q].y || this.pt[l].y+340 > this.pt[q].y) && (this.pt[l].y-300 < this.pt[q].y+40 || this.pt[l].y+340 < this.pt[q].y+40)) {
            message.tanks.push(JSON.parse(JSON.stringify(this.pt[q], (name, value) => {
              if (['updates', 'socket', 'render', 'healInterval', 'healTimeout', 'flashbangTimeout', 't', 'grapple', 'gluInterval', 'gluTimeout'].includes(name)) {
                return undefined;
              } else {
                return value;
              }
            })));
          }
          q++;
        }
        message.ai = [];
        var q = 0;
        while (q<this.ai.length) {
          // REVISE add culling
          message.ai.push(JSON.parse(JSON.stringify(this.ai[q], (name, value) => {
            if (['o', 'h', 'canFire', 'target', 'self_destruct'].includes(name)) {
              return undefined;
            } else {
              return value;
            }
          })));
          q++;
        }
        this.pt[l].updates.tank = false;
      }

      if (this.pt[l].updates.bullet) {
        var q = 0;
        message.bullets = [];
        while (q<this.s.length) {
          if ((this.pt[l].x-610 > this.s[q].x || this.pt[l].x+650 > this.s[q].x) && (this.pt[l].x-610 < this.s[q].x+5 || this.pt[l].x+650 < this.s[q].x+5) && (this.pt[l].y-300 > this.s[q].y || this.pt[l].y+340 > this.s[q].y) && (this.pt[l].y-300 < this.s[q].y+5 || this.pt[l].y+340 < this.s[q].y+5)) {
            message.bullets.push(JSON.parse(JSON.stringify(this.s[q], function(name, value) {
              if (['host', 'd', 'xm', 'ym', 'damage', 'ra'].includes(name)) {
                return undefined;
              } else {
                return value;
              }
            })));
          }
          q++;
        }
        q = 0;
        message.explosions = [];
        while (q<this.d.length) { // add culling
          message.explosions.push(JSON.parse(JSON.stringify(this.d[q], function(name, value) {
            if (['host', 'a', 'c', 'o'].includes(name)) {
              return undefined;
            } else {
              return value;
            }
          })));
          q++;
        }
        this.pt[l].updates.bullet = false;
      }

      if (this.pt[l].updates.logs) {
        message.logs = this.logs;
        this.pt[l].updates.logs = false;
      }

      if (Object.values(message).length !== 0) {
        message.event = 'hostupdate';
        this.pt[l].socket.send(JSON.stringify(message));
      }
      l++;
    }
  }

  damagePlayer(victim, damage) {
    if (victim.s > 0) {
      victim.s -= damage.a;
      victim.s = Math.max(victim.s, 0);
      return;
    }

    if (victim.buff) {
      damage.a *= .7;
    }
  
    victim.h -= damage.a;

    if (victim.d !== false) {
      clearTimeout(victim.d.t);
      victim.d.d += damage.a;
      victim.d.x = damage.x;
      victim.d.y = damage.y;
      victim.d.t = setTimeout(function() {
        this.d = false;
      }.bind(victim), 1000);
    } else {
      victim.d = {
        d: damage.a,
        x: damage.x,
        y: damage.y,
        t: setTimeout(function() {
          this.d = false;
        }.bind(victim), 1000),
      };
    }

    if (victim.in) {
      victim.canIn = false;
      victim.in = false;
      setTimeout(function() {
        this.canIn = true;
        this.in = true;
      }.bind(victim), 150);
    }
  
    if (victim.h <= 0) {
      victim.ded = true;
      this.logs.unshift({
        m: this.deathMsg(victim.u, damage.o),
        c: '#FF8C00',
      });
      var l = 0;
      while (l<this.pt.length) {
        if (this.pt[l].u === damage.o) {
          this.pt[l].s += 100;
          this.pt[l].socket.send(JSON.stringify({
            event: 'kill',
          }));
        }
        this.pt[l].updates.logs = true;
        l++;
      }
      l = 0;
      while (l<this.ai.length) {
        if (this.ai[l].o === victim.u) {
          this.ai.splice(l, 1);
          l--;
        }
        l++;
      }
      var x = victim.x;
      var y = victim.y;
      setTimeout(() => {
        victim.socket.send(JSON.stringify({
          event: 'ded',
        }));
        victim.socket.send(JSON.stringify({
          event: 'override',
          data: [{
            key: 'x',
            value: x,
          }, {
            key: 'y',
            value: y,
          }],
        }));
        victim.s = 100;
        victim.ded = false;
        victim.h = victim.ma;
      }, 10000);
    }
  }

  deathMsg(victim, killer) {
    var junk = [
      '{VICTIM} was killed by {KILLER}',
      '{VICTIM} got a 2 minute unskippable ad in the middle of their fight with {KILLER}',
      '{VICTIM} got their mouse stolen by {KILLER}',
      '{VICTIM} got their chromebook stolen by {KILLER}',
      '{VICTIM} dosnet know as many keybinds as {KILLER}',
      '{VICTIM} wished the teacher closed their chromebook because of {KILLER}',
      '{VICTIM} was convinced to stick a fork in an electrical socket by {KILLER}',
      '{VICTIM} thought the death messages were not funny, so they got killed by {KILLER}',
      '{VICTIM} clicked on a pop-up, so they got hacked by {KILLER}',
      '{VICTIM} was put out of their misery by {KILLER}',
      '{VICTIM} got comboed so hard that they wanted to nerf the power of {KILLER}',
      "{VICTIM}'s tank realized that it was in a video game because of {KILLER}",
      '{VICTIM} discovered that the floor was turned into lava by {KILLER}',
      '{VICTIM} said the graphics were bad, so justice was serverd by {KILLER}',
      '{VICTIM} was politley assasinated by {KILLER}',
      '{VICTIM} got comboed by {KILLER}',
      '{VICTIM} got eliminated by {KILLER}',
      "{VICTIM}'s existance was forgotten by {KILLER}",
      '{VICTIM} was snapped out of existance by {KILLER}',
      "{VICTIM}'s merge request was canceled by {KILLER}",
      '{VICTIM} found a turret made by {KILLER}',
      '{VICTIM} was slain by {KILLER}',
      '{VICTIM} said that this game was a awesome tanks 2 rip-off, so they were permamently banned by {KILLER}',
      '{VICTIM} got nerfed by {KILLER}',
      '{VICTIM} called for back-up, but no one cared, so they were murdered by {KILLER}',
      '{VICTIM} said they had a 2 kill-streak, so they got assasinated by {KILLER}',
      '{VICTIM} showed integrity in a game of hackers... so they got absolutly deleted by {KILLER}',
      '{VICTIM} was banned for hacking by {KILLER}',
      "{VICTIM}'s minecraft realm subscription was canceled by {KILLER}",
      "{VICTIM}'s iq was scored to be less than 0 by {KILLER}",
      '{VICTIM} was crushed by a falling {KILLER}',
      "{VICTIM}'s lunch got exploded by {KILLER}",
      '{VICTIM} was evanismed by {KILLER}',
      '{VICTIM} got on coolmathgames like a flat and got killed by {KILLER}',
      '{VICTIM} was pricked to death by {KILLER}',
      '{VICTIM} was burnt to a crisp by {KILLER}',
      '{VICTIM} was eaten by {KILLER}',
      '{VICTIM} was yeeted by {KILLER}',
      '{VICTIM} fell on a spike created by {KILLER}',
      '{VICTIM} had low skills while fighting {KILLER}',
      '{VICTIM} had a skill issue while fighting {KILLER}',
      '{VICTIM} was sniped by {KILLER}',
      '{VICTIM} was forced to stop existing by {KILLER}',
      '{VICTIM} was exploded by {KILLER}',
      '{VICTIM} was executed by {KILLER}',
      '{VICTIM} needed to leave because of {KILLER}',
      '{VICTIM} broke their mouse because of {KILLER}',
      '{VICTIM} had their tanks extended warranty expired by {KILLER}',
      '{VICTIM} imploded when they saw {KILLER}',
      "{VICTIM}'s diamonds were burnt in lava by {KILLER}",
      '{VICTIM} forgot their mouse because they were distracted by {KILLER}',
      '{VICTIM} got distracted by {KILLER}',
      '{VICTIM} didnt get to respawn because of {KILLER}',
      "{VICTIM}'s funeral was terrible because of {KILLER}",
      '{VICTIM} wanted revenge, but they suck, so they died to {KILLER}',
      '{VICTIM} conplained about the 5 turrets placed by {KILLER}',
      '{VICTIM} was NOT politley assasinated by {KILLER}',
      '{VICTIM} was consumed by {KILLER}',
      '{VICTIM} got depression from {KILLER}',
      '{VICTIM} wants a kit as good as the one used by {KILLER}',
      '{VICTIM} was deleted by {KILLER}',
      '{VICTIM} was goned by {KILLER}',
      '{VICTIM} was killed twice by {KILLER}',
      '{VICTIM} was killed by THE PERSON WITH KILLSTREAK ---> {KILLER}',
      '{VICTIM} whished that their death message was funnier because of {KILLER}',
      '{VICTIM} got buffed, but get nerfed harder by {KILLER}',
      '{VICTIM} wants to meganuke the universe because of {KILLER}',
      '{VICTIM} got flung into space by {KILLER}',
      '{VICTIM} was yeeted by {KILLER}',
      '{VICTIM} was decapitated by {KILLER}',
      '{VICTIM} was mound into paste by {KILLER}',
      "{VICITM}'s sister's xbox time was started by {KILLER}",
      '{VICTIM} was fed a healty dose of explosives by {KILLER}',
      '{VICTIM} tried to out pizza the hut owned by {KILLER}',
      '{VICTIM} got smushed into flappy bird, doomed to live their fate of bonking into a pipe by '
    ]
    return junk[Math.floor(Math.random()*junk.length)].replace('{VICTIM}', victim).replace('{KILLER}', killer);
  }

  joinMsg(player) {
    var junk = [
      '{IDOT} joined the game',
      '{IDOT} spawned',
      '{IDOT} was pulled out of the recycling bin',
      '{IDOT} unpaused',
      '{IDOT} was put into this server and was locked inside',
      '{IDOT} decided to come back',
      '{IDOT} is still playing the game',
      '{IDOT} actually joined for once',
      '{IDOT} wanted to do something else, but here they are',
      '{IDOT} wants to get a killsteak',
    ];
    return junk[Math.floor(Math.random()*junk.length)].replace('{IDOT}', player);
  }

  rageMsg(player) {
    var junk = [
      '{IDOT}.exe is unresponsive',
      '{IDOT} left the game',
      '{IDOT} ragequit',
      '{IDOT} went to play kingdoms',
      '{IDOT} lost hteir killsteak',
      '{IDOT} got rebooted for matinence',
      '{IDOT} left to cry into their pillow',
      "{IDOT}'s teacher closed their chromebook",
      '{IDOT} punched their chromebook too hard',
      '{IDOT} ran out of lives',
      "{IDOT}'s siblings xbox time started",
      '{IDOT} got away',
      'wild {IDOT} fled',
      '{IDOT} switched to school wifi',
      "{IDOT}'s chromebook got stolen"
    ];
    return junk[Math.floor(Math.random()*junk.length)].replace('{IDOT}', player);
  }

  disconnect(socket, code, reason) {
    this.sockets.splice(this.sockets.indexOf(socket), 1);
    var l = 0;
    while (l<this.pt.length) {
      if (this.pt[l].u === socket.username) {
        this.pt.splice(l, 1);
      }
      l++;
    }
    if (this.pt.length === 0) {
      var l = 0;
      while (l<this.i.length) {
        clearInterval(this.i[l]);
        l++;
      }
      l = 0;
      while (l<this.t.length) {
        clearTimeout(this.t[l]);
        l++;
      }
      if (servers.length-1 === servers.indexOf(this)) {
        servers = [];
      } else {
        servers[servers.indexOf(this)] = new FFA();
      }
      return;
    }
    this.logs.unshift({
      m: this.rageMsg(socket.username),
      c: '#E10600',
    });
    var l = 0;
    while (l<this.pt.length) {
      this.pt[l].updates.logs = true;
      l++;
    }
  }
}

class Block {
  constructor(x, y, health, type, host) {
    this.x = x;
    this.y = y;
    this.m = health; // max health
    this.h = health; // health
    this.t = type; // type
    this.ho = host; // host
    this.s = false; // show health bar
    this.c = true; // collision
    if (type === 'spike') {
      this.c = false;
    }
  }

  damage(d) {
    this.ho.triggerUpdate(this.x, this.y, 'block');
    
    this.h -= d;

    if (this.h !== Infinity) {
      this.s = true;
      clearTimeout(this.bar);
      this.bar = setTimeout(function() {
        this.s = false;
        this.ho.triggerUpdate(this.x, this.y, 'block');
      }.bind(this), 3000);
    }

    if (this.h <= 0) {
      this.destroy();
      return true;
    }
    return false;
  }

  destroy() {
    this.ho.b.splice(this.ho.b.indexOf(this), 1);
  }
}

class Shot {
  constructor(x, y, xm, ym, t, r, u, h) {
    this.settings = {
      damage: {
        bullet: 20,
        shotgun: 20,
        grapple: 0,
        powermissle: 100,
        megamissle: 200,
        cannon: 30,
      },
      speed: {
        bullet: 1,
        shotgun: .8,
        grapple: 2,
        powermissle: 1.5,
        megamissle: 1.5,
        cannon: 2,
      }
    }

    this.d = 0;
    this.u = u;
    this.r = r;
    this.t = t;
    this.sx = x;
    this.sy = y;
    this.host = h;

    var d = 8;
    this.xm = xm*(d/Math.sqrt(xm*xm+ym*ym));
    this.ym = ym*(d/Math.sqrt(xm*xm+ym*ym));
    var data = Shot.calc(x, y, xm, ym);
    this.x = data.x;
    this.y = data.y;

    if (xm == 0 && ym == 1) {
      this.x = x;
      this.y = 35+y;
    } else if (xm == -1 && ym == 0) {
      this.x = -35+x;
      this.y = y;
    } else if (xm == 0 && ym == -1) {
      this.x = x;
      this.y = -35+y;
    } else if (xm == 1 && ym == 0) {
      this.x = 35+x;
      this.y = y;
    }

    this.damage = this.settings.damage[this.t];
    var l = 0;
    while (l<this.host.pt.length) {
      if (this.host.pt[l].u === this.u && this.host.pt[l].buff) {
        this.damage *= 1.3;
      }
      l++;
    }
    this.xm *= this.settings.speed[this.t];
    this.ym *= this.settings.speed[this.t];
    
  }

  static calc(x, y, xm, ym) {
    var sgn, x, y;
    if (((ym/xm)*-20-(ym/xm)*20)<0) {
      sgn = -1;
    } else {
      sgn = 1;
    }
    var x1 = ((20*(ym/xm)*-20-(-20)*(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)+sgn*-40*Math.sqrt(1000*(Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)))-((20*(ym/xm)*-20-(-20)*(ym/xm)*20)*(20*(ym/xm)*-20-(-20)*(ym/xm)*20))))/(Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)));
    var x2 = ((20*(ym/xm)*-20-(-20)*(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)-sgn*-40*Math.sqrt(1000*(Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)))-((20*(ym/xm)*-20-(-20)*(ym/xm)*20)*(20*(ym/xm)*-20-(-20)*(ym/xm)*20))))/(Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)));
    var y1 = (-(20*(ym/xm)*-20-(-20)*(ym/xm)*20)*-40+ Math.abs(((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(1000*(Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)))-((20*(ym/xm)*-20-(-20)*(ym/xm)*20)*(20*(ym/xm)*-20-(-20)*(ym/xm)*20))))/(Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)));
    var y2 = (-(20*(ym/xm)*-20-(-20)*(ym/xm)*20)*-40-Math.abs(((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(1000*(Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))) - ((20*(ym/xm)*-20-(-20)*(ym/xm)*20)*(20*(ym/xm)*-20-(-20)*(ym/xm)*20))))/(Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20))*Math.sqrt(-40*-40+((ym/xm)*-20-(ym/xm)*20)*((ym/xm)*-20-(ym/xm)*20)));
    if (ym >= 0) {
      x = x1+x;
      y = y1+y;
    } else {
      x = x2+x;
      y = y2+y;
    }
    return {
      x: x,
      y: y,
    };
  }

  collision() {
    var x = this.x;
    var y = this.y;
    var key = {
      bullet: 10,
      shotgun: 10,
      powermissle: 50,
      megamissle: 75,
      grapple: 10,
      cannon: 75,
    }

    var l = 0;
    while (l<this.host.pt.length) {
      if ((x > this.host.pt[l].x || x + 5 > this.host.pt[l].x) && (x < this.host.pt[l].x + 40 || x + 5 < this.host.pt[l].x + 40)) {
        if ((y > this.host.pt[l].y || y + 5 > this.host.pt[l].y) && (y < this.host.pt[l].y + 40 || y + 5 < this.host.pt[l].y + 40)) {
          if (!this.host.pt[l].ded) {
            this.host.d.push(new Damage(this.x-key[this.t]/2+5, this.y-key[this.t]/2+5, key[this.t], key[this.t], this.damage, this.u, this.host));
            return true;
          }
          break;
        }
      }
      l++;
    }

    var l = 0;
    while (l<this.host.ai.length) {
      if ((x > this.host.ai[l].x || x + 5 > this.host.ai[l].x) && (x < this.host.ai[l].x + 40 || x + 5 < this.host.ai[l].x + 40)) {
        if ((y > this.host.ai[l].y || y + 5 > this.host.ai[l].y) && (y < this.host.ai[l].y + 40 || y + 5 < this.host.ai[l].y + 40)) {
          this.host.d.push(new Damage(this.x-key[this.t]/2+5, this.y-key[this.t]/2+5, key[this.t], key[this.t], this.damage, this.u, this.host));
          return true;
          break;
        }
      }
      l++;
    }
    
    var offMap = false;
    if (x < 0 || y < 0 || x+5 > 1500 || y+5 > 1500) offMap = true;
    var l = 0;
    while (l < this.host.b.length) {
      if (((x > this.host.b[l].x || x+5 > this.host.b[l].x) && (x < this.host.b[l].x+50 || x+5 < this.host.b[l].x+50)) || offMap) {
        if (((y > this.host.b[l].y || y+5 > this.host.b[l].y) && (y < this.host.b[l].y+50 || y+5 < this.host.b[l].y+50)) || offMap) {
          if (this.host.b[l].c) {
            if (this.t === 'grapple') {
              var q = 0;
              while (q<this.host.pt.length) {
                if (this.host.pt[q].u === this.u) {
                  if (this.host.pt[q].grapple) {
                    this.host.pt[q].grapple.bullet.destroy();
                  }
                  this.host.pt[q].grapple = {
                    sx: this.host.pt[q].x+20,
                    sy: this.host.pt[q].y+20,
                    x: this.x,
                    y: this.y,
                    bullet: this,
                  }
                }
                q++;
              }
              this.update = () => {};
              return false;
            } else {
              this.host.d.push(new Damage(this.x-key[this.t]/2+5, this.y-key[this.t]/2+5, key[this.t], key[this.t], this.damage, this.u, this.host));
              return true;
            }
          }
        }
      }
      l++;
    }

    if (x < 0 || x > 1500 || y < 0 || y > 1500) {
      return true;
    }
    return false;
  }

  update() {

    var l = 0;
    while (l<this.host.pt.length) {
      if ((this.host.pt[l].x-540 > this.x || this.host.pt[l].x+580 > this.x) && (this.host.pt[l].x-540 < this.x+5 || this.host.pt[l].x+580 < this.x+5) && (this.host.pt[l].y-230 > this.y || this.host.pt[l].y+270 > this.y) && (this.host.pt[l].y-230 < this.y+5 || this.host.pt[l].y+270 < this.y+5)) {
        this.host.pt[l].updates.bullet = true;
      }
      l++;
    }

    this.x += this.xm;
    this.y += this.ym;
    if (this.collision()) {
      this.destroy();
      return;
    }

    this.d += Math.sqrt(Math.pow(this.xm, 2)+Math.pow(this.ym, 2));

    if (this.t === 'shotgun') {
      this.damage = this.settings.damage[this.t]-(this.d/150)*this.settings.damage[this.t];
      if (this.d >= 150) {
        this.destroy();
      }
    }
  }

  destroy() {
    this.host.s.splice(this.host.s.indexOf(this), 1);
  }
}

class Damage {
  constructor(x, y, w, h, a, o, host) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.a = a;
    this.o = o;
    this.c = true;
    this.host = host;
    this.f = 0;
    setInterval(function() {
      this.host.triggerUpdate(this.x, this.y, 'bullet');
      this.f++;
    }.bind(this), 25);
    setTimeout(function() {
      this.destroy();
    }.bind(this), 200);
  }

  destroy() {
    this.host.triggerUpdate(this.x, this.y, 'bullet');
    this.host.d.splice(this.host.d.indexOf(this), 1);
  }
}

class Ai {
  constructor(x, y, type, owner, host) {
    /*
      0 -> turret
      1 -> normal
      2 -> shotgun
    */
    this.x = x;
    this.y = y;
    this.r = 0;
    this.o = owner;
    this.h = host;
    this.hp = 400;
    this.p = 0;
    this.setup = false;
    setTimeout(function() {
      this.setup = true;
    }.bind(this), 5000);
    if (type === 0) {
      this.turret = true;
    } else {
      this.turret = false;
    }
    this.canFire = true;
    var l = 0;
    while (l<this.h.pt.length) {
      if (this.h.pt[l].u === this.o) {
        this.c = this.h.pt[l].co;
      }
      l++;
    }
  }

  update() {
    if (!this.setup) return;
    if (!this.identify()) {return};
    this.aim();
    if (this.canFire) {
      this.fire();
      this.canFire = false;
      setTimeout(function() {
        this.canFire = true;
      }.bind(this), 200);
    }
  }

  toAngle(x, y) {
    var angle = Math.atan2(x, y) * 180 / Math.PI
    angle = -angle //-90;
    if (angle < 0) {
      angle += 360;
    }
    if (angle >= 360) {
      angle -= 360;
    }
    return angle;
  }

  toPoint(angle) {
    var theta = (-angle) * Math.PI / 180;
    var y = Math.cos(theta);
    var x = Math.sin(theta);
    if (x < 0) {
      if (y < 0) {
        return {
          x: -1,
          y: Math.round(-Math.abs(y / x) * 1000) / 1000,
        };
      } else {
        return {
          x: -1,
          y: Math.round(Math.abs(y / x) * 1000) / 1000,
        };
      }
    } else {
      if (y < 0) {
        return {
          x: 1,
          y: Math.round(-Math.abs(y / x) * 1000) / 1000,
        };
      } else {
        return {
          x: 1,
          y: Math.round(Math.abs(y / x) * 1000) / 1000,
        };
      }
    }
  }

  identify() {
    var l = 0, targets = [];
    while (l<this.h.pt.length) {
      if (this.h.pt[l].u !== this.o && !this.h.pt[l].ded && (!this.h.pt[l].in || this.hp !== 400)) {
        var x = this.h.pt[l].x-this.x;
        var y = this.h.pt[l].y-this.y;
        var distance = Math.sqrt(x*x+y*y);
        targets.push({
          idot: this.h.pt[l],
          distance: distance,
        });
      }
      l++;
    }
    l = 0;
    while (l<this.h.ai.length) {
      if (this.h.ai[l].o !== this.o) {
        var x = this.h.ai[l].x-this.x;
        var y = this.h.ai[l].y-this.y;
        var distance = Math.sqrt(x*x+y*y);
        targets.push({
          idot: this.h.ai[l],
          distance: distance,
        });
      }
      l++;
    }
    this.h.triggerUpdate(this.x, this.y, 'tank');
    if (targets.length === 0) {
      this.r += 1;
      return false; // no idots :(
    }
    targets.sort((a, b) => { return a.distance - b.distance; }); // sort array to closest target
    if (targets[0].distance > 750) {
      this.r += 1;
      return false; // out of range
    }
    this.target = targets[0].idot; // select closest idot
    return true;
  }

  aim() {
    this.r = this.toAngle(this.target.x - this.x, this.target.y - this.y);
  }

  fire() {
    this.p = -3;
    var data = this.toPoint(this.r);
    this.h.s.push(new Shot(this.x+20, this.y+20, data.x, data.y, 'bullet', 0, this.o, this.h));
  }

  damage(d) {
    this.hp -= d;
    if (this.hp <= 0) {
      this.h.ai.splice(this.h.ai.indexOf(this), 1);
    }
  }
}

if (!SETTINGS.use_existing_node_server) {
  FFA_SERVER.listen(80);
}

export {FFA_SERVER};
