import java.util.HashMap;
import java.net.URI;

public class MegaSocket {

  private final String url;
  private HashMap<String, Object> options;
  private HashMap<String, Object> callstack;
  public String status;

  MegaSocket(String url, HashMap<String, Object> options) {

    url = url;
    options = options;
    callstack.put("open", []);
    callstack.put("close", []);
    callstack.put("message", []);

    if (options == null) {
      options = new HashMap<String, Object>();
    }
    if (options.get("keepAlive") == null) {
      options.put("keepAlive", true);
    }
    if (options.get("autoconnect") == null) {
      options.put("autoconnect", true);
    }
    if (options.get("reconnect") == null) {
      options.put("reconnect", true);
    }

    if (options.get("autoconnect")) {
      status = "connecting";
      connect();
    } else {
      status = "idle";
      /*window.addEventListener('offline', function() { // Arrow Function Wont Work Due To Bind
        this.socket.close();
        this.socket.onclose();
      }.bind(this));
      if (this.options.reconnect) {
        window.addEventListener('online', function() {
          this.connect();
        }.bind(this))
      }*/
    }
  }

  public void connect() {
    try {
      socket = new WebsocketClientEndpoint(new URI(url));
    } catch (InterruptedException e) {
      System.err.println("WebSocket had trouble connecting to "+url+" Error: "+e);
    }

    status = "connected";
    if (options.get("keepAlive")) {
      socket.keepAlive = setInterval(function() {
        socket.send('{}');
      }.bind(this), 50000);
    }
    
    var l = 0;
    while (l < callstack.get("open").length) {
      callstack.open[l]();
      l++;
    }

    this.socket.onmessage = function(data) {
      data = JSON.parse(data.data);
      // custom error handling for server
      if (data.status === 'error') {
        alert('WebSocket Error: ' + data.message);
        return;
      }
      // end
      var l = 0;
      while (l < this.callstack.message.length) {
        this.callstack.message[l](data);
        l++;
      }
    }.bind(this);
    this.socket.onclose = function() {
      clearInterval(this.socket.keepAlive);
      this.status = 'disconnected';
      var l = 0;
      while (l < this.callstack.close.length) {
        this.callstack.close[l]();
        l++;
      }
      if (this.options.reconnect) {
        this.connect();
      }
    }.bind(this);
  }
  on(event, operation) {
    if (event == 'connect') {
      this.callstack.open.push(operation);
    }
    if (event == 'message') {
      this.callstack.message.push(operation);
    }
    if (event == 'close') {
      this.callstack.close.push(operation);
    }
  }
  no(event) {
    if (event == 'connect') {
      this.callstack.open = [];
    }
    if (event == 'message') {
      this.callstack.message = [];
    }
    if (event == 'close') {
      this.callstack.close = [];
    }
  }
  send(data) {
    data = JSON.stringify(data);
    this.socket.send(data);
  }
  close() {
    this.socket.close();
  }
}
