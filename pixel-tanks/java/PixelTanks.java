import java.awt.Color;
  
class PixelTanks {

  public static double resizer;
  
  public static void main(String[] args) {
    GUI.setup();
    PixelTanks.setup();
    //PixelTanks.boot();
  }

  public static void setup() {
    resizer = 1.00; // scale factor
    //GUI.draw.setTransform(resizer, 0, 0, resizer, 0, 0);
    GUI.draw.clearRect(0, 0, 500, 500);
    GUI.draw.setColor(Color.BLACK);
    GUI.draw.fillRect(0, 0, 500, 500);
    //GUI.draw.textAlign = "center";
    GUI.draw.setColor(Color.WHITE);
    GUI.draw.drawString("Loading 0%", 250, 250);
  }
/*
  static updateBootProgress() {
    GUI.draw.font = '20px starfont';
    GUI.draw.clearRect(0, 0, 500, 500);
    GUI.draw.fillStyle = '#000000';
    GUI.draw.fillRect(0, 0, 500, 500);
    GUI.draw.textAlign = 'center';
    GUI.draw.fillStyle = '#ffffff';
    GUI.draw.fillText('Loading ' + Math.floor(PixelTanks.loadedImages / PixelTanks.totalImages * 100) + '%', 250, 250);
    if (PixelTanks.loadedImages == PixelTanks.totalImages) {
      PixelTanks.launch();
    }
  }

  static boot() {
    // setup boot
    PixelTanks.totalImages = 0; // total images needed to load game
    PixelTanks.loadedImages = 0; // % of images loaded

    // reference images
    PixelTanks.texturepack = 'default'; // texturepack
    PixelTanks.images = { // mapping
      blocks: {
        default: {
          barrier: '/images/blocks/default/barrier.png',
          strong: '/images/blocks/default/strong.png',
          weak: '/images/blocks/default/weak.png',
          floor: '/images/blocks/default/floor.png',
        },
        jungle: {
          barrier: '/images/blocks/jungle/barrier.png',
          strong: '/images/blocks/jungle/strong.png',
          weak: '/images/blocks/jungle/weak.png',
          floor: '/images/blocks/jungle/floor.png',
        },
        nature: {
          barrier: '/images/blocks/nature/barrier.png',
          strong: '/images/blocks/nature/strong.png',
          weak: '/images/blocks/nature/weak.png',
          floor: '/images/blocks/nature/floor.png',
        },
        digit: {
          barrier: '/images/blocks/digit/barrier.png',
          strong: '/images/blocks/digit/strong.png',
          weak: '/images/blocks/digit/weak.png',
          floor: '/images/blocks/digit/floor.png',
        },
        color: {
          barrier: '/images/blocks/color/barrier.png',
          strong: '/images/blocks/color/strong.png',
          weak: '/images/blocks/color/weak.png',
          floor: '/images/blocks/color/floor.png',
        },
      },
      bullets: {
        default: {
          // normal: '/images/bullets/default/normal.png', no image yet :(
          shotgun: '/images/bullets/default/shotgun.png',
          condensed: '/images/bullets/default/condensed.png',
          powermissle: '/images/bullets/default/powermissle.png',
          megamissle: '/images/bullets/default/megamissle.png',
          grapple: '/images/bullets/default/grapple.png',
          particle: [
            '/images/bullets/default/bullet-particle-0.png',
            '/images/bullets/default/bullet-particle-1.png',
            '/images/bullets/default/bullet-particle-2.png',
            '/images/bullets/default/bullet-particle-3.png',
            '/images/bullets/default/bullet-particle-4.png',
          ],
        },
      },
      tanks: {
        default: {
          red: {
            top: '/images/tanks/default/red/top.png',
            bottom: [
              '/images/tanks/default/red/bottom.png',
              '/images/tanks/default/red/bottom2.png',
            ],
          },
          iron: {
            top: '/images/tanks/default/iron/top.png',
            bottom: [
              '/images/tanks/default/iron/bottom.png',
              '/images/tanks/default/iron/bottom2.png',
            ],
          },
          diamond: {
            top: '/images/tanks/default/diamond/top.png',
            bottom: [
              '/images/tanks/default/diamond/bottom.png',
              '/images/tanks/default/diamond/bottom2.png',
            ],
          },
          dark: {
            top: '/images/tanks/default/dark/top.png',
            bottom: [
              '/images/tanks/default/dark/bottom.png',
              '/images/tanks/default/dark/bottom2.png',
            ],
          },
          light: {
            top: '/images/tanks/default/light/top.png',
            bottom: [
              '/images/tanks/default/light/bottom.png',
              '/images/tanks/default/light/bottom2.png',
            ],
          },
          cosmetics: [{
            name: 'Astronaut',
            image: new ImageLoader(),
            id: 'astronaut'
          }, {
            name: 'Onfire',
            image: new ImageLoader(),
            id: 'onfire'
          }, {
            name: 'Assassin',
            image: new ImageLoader(),
            id: 'assassin'
          }, {
            name: 'Redsus',
            image: new ImageLoader(),
            id: 'redsus'
          }, {
            name: 'Venom',
            image: new ImageLoader(),
            id: 'venom',
          }, {
            name: 'Blue Tint',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'blue_tint',
          }, {
            name: 'Purple Flower',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'purple_flower',
          }, {
            name: 'Leaf',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'leaf',
          }, {
            name: 'Basketball',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'basketball',
          }, {
            name: 'Purple Top Hat',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'purple_top_hat',
          }, {
            name: 'Terminator',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'terminator',
          }, {
            name: 'Dizzy',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'dizzy',
          }, {
            name: 'Knife',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'knife',
          }, {
            name: 'Scared',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'scared',
          }, {
            name: 'Laff',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'laff',
          }, {
            name: 'Hacker Hoodie',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'hacker_hoodie',
          }, {
            name: 'Error',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'error',
          }, {
            name: 'Purple Grad Hat',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'purple_grad_hat',
          }, {
            name: 'Bat Wings',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'bat_wings',
          }, {
            name: 'Back Button',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'back',
          }, {
            name: 'Fisher Hat',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'fisher_hat',
          }, {
            name: 'Ban',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'ban',
          }, {
            name: 'Blue Ghost',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'blue_ghost',
          }, {
            name: 'Pumpkin Face',
            rarity: 0,
            price: 0,
            image: new ImageLoader,
            id: 'pumpkin_face',
          }, {
            name: 'Pumpkin Hat',
            rarity: 0,
            price: 0,
            image: new ImageLoader,
            id: 'pumpkin_hat',
          }, {
            name: 'Red Ghost',
            rarity: 0,
            price: 0,
            image: new ImageLoader,
            id: 'red_ghost',
          }, {
            name: 'Candy Corn',
            rarity: 0,
            price: 0,
            image: new ImageLoader,
            id: 'candy_corn',
          }, {
            name: 'Yellow Pizza',
            rarity: 0,
            price: 0,
            image: new ImageLoader,
            id: 'yellow_pizza',
          }, {
            name: 'Orange Ghost',
            rarity: 0,
            price: 0,
            image: new ImageLoader,
            id: 'orange_ghost',
          }, {
            name: 'Pink Ghost',
            rarity: 0,
            price: 0,
            image: new ImageLoader,
            id: 'pink_ghost',
          }, {
            name: 'Paleontologist',
            rarity: 0,
            price: 0,
            image: new ImageLoader,
            id: 'paleontologist',
          }, {
            name: 'Yellow Hoodie',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'yellow_hoodie',
          }, {
            name: 'X',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'x',
          }, {
            name: 'Sweat',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'sweat',
          }, {
            name: 'Spirals',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'spirals',
          }, {
            name: 'Spikes',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'spikes',
          }, {
            name: 'Rudolph',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'rudolph',
          }, {
            name: 'Reindeer Hat',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'reindeer_hat',
          }, {
            name: 'Red Hoodie',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'red_hoodie',
          }, {
            name: 'Question Mark',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'question_mark',
          }, {
            name: 'Pink/Purple Hoodie',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'purplepink_hoodie',
          }, {
            name: 'Purple Hoodie',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'purple_hoodie',
          }, {
            name: 'Pumpkin',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'pumpkin',
          }, {
            name: 'Pickle',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'pickle',
          }, {
            name: 'Orange Hoodie',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'orange_hoodie',
          }, {
            name: 'Helment',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'helment',
          }, {
            name: 'Green Hoodie',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'green_hoodie',
          }, {
            name: 'Exclaimation Point',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'exclaimation_point',
          }, {
            name: 'Eggplant',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'eggplant',
          }, {
            name: 'Devils Wings',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'devils_wings',
          }, {
            name: 'Christmas Tree',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'christmas_tree',
          }, {
            name: 'Christmas Lights',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'christmas_lights',
          }, {
            name: 'Checkmark',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'checkmark',
          }, {
            name: 'Cat Hat',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'cat_hat',
          }, {
            name: 'Blueberry',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'blueberry',
          }, {
            name: 'Blue Hoodie',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'blue_hoodie',
          }, {
            name: 'Blue Helment',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'blue_helment',
          }, {
            name: 'Banana',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'bannana',
          }, {
            name: 'Aqua Helment',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'aqua_helment',
          }, {
            name: 'Apple',
            rarity: 0,
            price: 0,
            image: new ImageLoader(),
            id: 'apple',
          }, {
            name: 'Hoodie',
            rarity: 20,
            price: 1000000,
            image: new ImageLoader(),
            id: 'hoodie',
          }, {
            name: 'Purple Helment',
            rarity: 20,
            price: 1000000,
            image: new ImageLoader(),
            id: 'purple_helment',
          }, {
            name: 'Angel Wings',
            rarity: 4,
            price: 20000,
            image: new ImageLoader(),
            id: 'angel_wings',
          }, {
            name: 'Boost',
            rarity: 4,
            price: 20000,
            image: new ImageLoader(),
            id: 'boost',
          }, {
            name: 'Bunny Ears',
            rarity: 2,
            price: 1000,
            image: new ImageLoader(),
            id: 'bunny_ears'
          }, {
            name: 'Cake',
            rarity: 4,
            price: 4000,
            image: new ImageLoader(),
            id: 'cake',
          }, {
            name: 'Cancelled',
            rarity: 1,
            price: 1000,
            image: new ImageLoader(),
            id: 'cancelled',
          }, {
            name: 'Candy Cane',
            rarity: 5,
            price: 1000,
            image: new ImageLoader(),
            id: 'candy_cane',
          }, {
            name: 'Cat Ears',
            rarity: 4,
            price: 20000,
            image: new ImageLoader(),
            id: 'cat_ears',
          }, {
            name: 'Christmas Hat',
            rarity: 8,
            price: 30000,
            image: new ImageLoader(),
            id: 'christmas_hat',
          }, {
            name: 'Controller',
            rarity: 10,
            price: 1000000,
            image: new ImageLoader(),
            id: 'controller',
          }, {
            name: 'Deep Scratch',
            rarity: 4,
            price: 20000,
            image: new ImageLoader(),
            id: 'deep_scratch',
          }, {
            name: 'Devils Horns',
            rarity: 4,
            price: 20000,
            image: new ImageLoader(),
            id: 'devil_horn',
          }, {
            name: 'Headphones',
            rarity: 4,
            price: 20000,
            image: new ImageLoader(),
            id: 'earmuffs',
          }, {
            name: 'Eyebrows',
            rarity: 4,
            price: 20000,
            image: new ImageLoader(),
            id: 'eyebrows',
          }, {
            name: 'First Aid',
            rarity: 4,
            price: 10000,
            image: new ImageLoader(),
            id: 'first_aid',
          }, {
            name: 'Flag',
            rarity: 4,
            price: 20000,
            image: new ImageLoader(),
            id: 'flag',
          }, {
            name: 'Halo',
            rarity: 1,
            price: 5000,
            image: new ImageLoader(),
            id: 'halo',
          }, {
            name: 'Hacks',
            rarity: 10,
            price: 1000000,
            image: new ImageLoader(),
            id: 'hax',
          }, {
            name: 'Low Battery',
            rarity: 5,
            price: 50000,
            image: new ImageLoader(),
            id: 'low_battery',
          }, {
            name: 'Mini Tank',
            rarity: 8,
            price: 20000,
            image: new ImageLoader(),
            id: 'mini_tank',
          }, {
            name: 'MLG Glasses',
            rarity: 10,
            price: 100000,
            image: new ImageLoader(),
            id: 'mlg_glasses',
          }, {
            name: 'Money Eyes',
            rarity: 2,
            price: 10000,
            image: new ImageLoader(),
            id: 'money_eyes',
          }, {
            name: 'No Mercy',
            rarity: 4,
            price: 2000,
            image: new ImageLoader(),
            id: 'no_mercy',
          }, {
            name: 'Peace',
            rarity: 4,
            price: 2000,
            image: new ImageLoader(),
            id: 'peace',
          }, {
            name: 'Police',
            rarity: 4,
            price: 2000,
            image: new ImageLoader(),
            id: 'police',
          }, {
            name: 'Question Mark',
            rarity: 4,
            price: 2000,
            image: new ImageLoader(),
            id: 'question_mark',
          }, {
            name: 'Rage',
            rarity: 4,
            price: 20000,
            image: new ImageLoader(),
            id: 'rage',
          }, {
            name: 'Small Scratch',
            rarity: 4,
            price: 20000,
            image: new ImageLoader(),
            id: 'small_scratch',
          }, {
            name: 'Speaker',
            rarity: 2,
            price: 2000,
            image: new ImageLoader(),
            id: 'speaker',
          }, {
            name: 'Swords',
            rarity: 4,
            price: 20000,
            image: new ImageLoader(),
            id: 'swords',
          }, {
            name: 'Tools',
            rarity: 4,
            price: 2000,
            image: new ImageLoader(),
            id: 'tools',
          }, {
            name: 'Top Hat',
            image: new ImageLoader(),
            id: 'top_hat',
          }, {
            name: 'Uno Reverse',
            image: new ImageLoader(),
            id: 'uno_reverse',
          }, {
            name: 'Victim',
            image: new ImageLoader(),
            id: 'victim',
          }],
        },
        // more texture packs
      },
      menus: {
        default: {
          start: '/images/menus/default/start.png',
          main: '/images/menus/default/main.png',
          multiplayer: '/images/menus/default/multiplayer.png',
          levelSelect: 'data:image/png;base64,',
          levelSelect2: 'data:image/png;base64,',              Menus are generated by canvas not image :)
          levelSelect3: 'data:image/png;base64,',
          levelSelect4: 'data:image/png;base64,',
          shopItems: '/images/menus/default/shop_items.png',
          shopTanks: '/images/menus/default/shop_tanks.png',
          shopClasses: '/images/menus/default/shop_classes.png',
          shopKits: '/images/menus/default/shop_kits.png',
          pvp: '/images/menus/default/pvp.png',
          co_op: '/images/menus/default/co_op.png',
          ffa: '/images/menus/default/ffa.png',
          duels: '/images/menus/default/duels.png',
          cosmeticMain: '/images/menus/default/cosmetic_main.png',
          cosmeticLeft: '/images/menus/default/cosmetics_left.png',
          cosmeticMiddle: '/images/menus/default/cosmetics_middle.png',
          cosmeticRight: '/images/menus/default/cosmetics_right.png',
          defeat: '/images/menus/default/defeat.png',
          victory: '/images/menus/default/victory.png',
        },
      },
    };

    // variable setup
    PixelTanks.b = []; // Blocks
    PixelTanks.s = []; // Bullets
    PixelTanks.ai = []; // Enemies
    PixelTanks.crates = {
      red: [{
        name: 'Apple',
        rarity: 'uncommon',
      }, {
        name: 'Redsus',
        rarity: 'legendary',
      }, {
        name: 'Rage',
        rarity: 'epic',
      }, {
        name: 'X',
        rarity: 'common',
      }, {
        name: 'Hacks',
        rarity: 'rare',
      }, {
        name: 'Red Hoodie',
        rarity: 'common',
      }, {
        name: 'Devils Wings',
        rarity: 'common',
      }, {
        name: 'Devils Horns',
        rarity: 'common',
      }, {
        name: 'Exclaimation Point',
        rarity: 'common',
      }],
      orange: [{
        name: 'Pumpkin',
        rarity: 'uncommon',
      }, {
        name: 'Onfire',
        rarity: 'epic',
      }, {
        name: 'Orange Hoodie',
        rarity: 'common',
      }, {
        name: 'Tools',
        rarity: 'rare',
      }, {
        name: 'Basketball',
        rarity: 'uncommon',
      }, ],
      yellow: [{
        name: 'Yellow Hoodie',
        rarity: 'common',
      }, {
        name: 'Banana',
        rarity: 'uncommon',
      }, {
        name: 'Halo',
        rarity: 'epic',
      }, {
        name: 'Uno Reverse',
        rarity: 'legendary',
      }, {
        name: 'Money Eyes',
        rarity: 'rare',
      }, {
        name: 'Dizzy',
        rarity: 'dizzy',
      }],
      green: [{
        name: 'Pickle',
        rarity: 'uncommon',
      }, {
        name: 'Checkmark',
        rarity: 'rare',
      }, {
        name: 'Green Hoodie',
        rarity: 'common',
      }, {
        name: 'Leaf',
        rarity: 'common',
      }],
      blue: [{
        name: 'Blue Hoodie',
        rarity: 'common',
      }, {
        name: 'Police',
        rarity: 'epic',
      }, {
        name: 'Blueberry',
        rarity: 'uncommon',
      }, {
        name: 'Sweat',
        rarity: 'rare',
      }, {
        name: 'Scared',
        rarity: 'rare',
      }, {
        name: 'Blue Tint',
        rarity: 'rare',
      }],
      purple: [{
        name: 'Purple Hoodie',
        rarity: 'common',
      }, {
        name: 'Eggplant',
        rarity: 'uncommon',
      }, {
        name: 'Purple Flower',
        rarity: 'common',
      }, {
        name: 'Purple Top Hat',
        rarity: 'rare',
      }, {
        name: 'Purple Grad Hat',
        rarity: 'rare',
      }],
      mark: [{
        name: 'Boost',
        rarity: 'common',
      }, {
        name: 'Peace',
        rarity: 'uncommon',
      }, {
        name: 'Question Mark',
        rarity: 'uncommon',
      }, {
        name: 'Cancelled',
        rarity: 'common',
      }, {
        name: 'Eyebrows',
        rarity: 'rare',
      }, {
        name: 'Small Scratch',
        rarity: 'uncommon',
      }, {
        name: 'Deep Scratch',
        rarity: 'epic',
      }, {
        name: 'Spirals',
        rarity: 'common',
      }, {
        name: 'Laff',
        rarity: 'common',
      }, {
        name: 'Ban',
        rarity: 'uncommon',
      }, {
        name: 'Back',
        rarity: 'epic',
      }],
      grey: [{
        name: 'Headphones',
        rarity: 'uncommon',
      }, {
        name: 'Assassin',
        rarity: 'epic',
      }, {
        name: 'Astronaut',
        rarity: 'epic',
      }, {
        name: 'Helment',
        rarity: 'rare',
      }, {
        name: 'Speaker',
        rarity: 'common',
      }, {
        name: 'Spikes',
        rarity: 'common',
      }, {
        name: 'Controller',
        rarity: 'epic',
      }, {
        name: 'Bat Wings',
        rarity: 'common',
      }, {
        name: 'Terminator',
        rarity: 'legendary',
      }, {
        name: 'Knife',
        rarity: 'uncommon',
      }],
      christmas: [{
        name: 'Rudolph',
        rarity: 'rare',
      }, {
        name: 'Christmas Hat',
        rarity: 'legendary',
      }, {
        name: 'Christmas Tree',
        rarity: 'common',
      }, {
        name: 'Christmas Lights',
        rarity: 'epic',
      }, {
        name: 'Candy Cane',
        rarity: 'common',
      }, {
        name: 'Reindeer Hat',
        rarity: 'uncommon',
      }],
      halloween: [{
        name: 'Pumpkin Hat',
        rarity: 'uncommon',
      }, {
        name: 'Pumpkin Face',
        rarity: 'common',
      }, {
        name: 'Candy Corn',
        rarity: 'rare',
      }, {
        name: 'Cat Ears',
        rarity: 'uncommon',
      }, ],
      misc: [{
        name: 'MLG Glasses',
        rarity: 'mythic',
      }, {
        name: 'Cake',
        rarity: 'uncommon',
      }, {
        name: 'Cat Hat',
        rarity: 'uncommon',
      }, {
        name: 'Flag',
        rarity: 'rare',
      }, {
        name: 'Mini Tank',
        rarity: 'legendary',
      }, {
        name: 'No Mercy',
        rarity: 'epic',
      }, {
        name: 'Swords',
        rarity: 'rare',
      }, {
        name: 'Top Hat',
        rarity: 'common',
      }, {
        name: 'Victim',
        rarity: 'common',
      }, {
        name: 'First Aid',
        rarity: 'uncommon',
      }, {
        name: 'Pink/Purple Hoodie',
        rarity: 'common',
      }, {
        name: 'Paleontologist',
        rarity: 'legendary',
      }, {
        name: 'Bunny Ears',
        rarity: 'common',
      }, {
        name: 'Fisher Hat',
        rarity: 'uncommon',
      }, {
        name: 'Error',
        rarity: 'epic',
      }],
      pizza: [{
        name: 'Red Ghost',
        rarity: 'common',
      }, {
        name: 'Blue Ghost',
        rarity: 'common',
      }, {
        name: 'Pink Ghost',
        rarity: 'common',
      }, {
        name: 'Orange Ghost',
        rarity: 'common',
      }, {
        name: 'Yellow Pizza',
        rarity: 'legendary',
      }],
    };

    // Load Images
    for (var property in PixelTanks.images.blocks) {
      for (var blok in PixelTanks.images.blocks[property]) {
        var src = PixelTanks.images.blocks[property][blok];
        PixelTanks.images.blocks[property][blok] = new ImageLoader();
        PixelTanks.images.blocks[property][blok].src = src;
      }
    }
    for (var property in PixelTanks.images.bullets) {
      for (var bullet in PixelTanks.images.bullets[property]) {
        if (bullet == 'particle') {
          var l = 0;
          while (l < PixelTanks.images.bullets[property].particle.length) {
            var src = PixelTanks.images.bullets[property].particle[l];
            PixelTanks.images.bullets[property].particle[l] = new ImageLoader();
            PixelTanks.images.bullets[property].particle[l].src = src;
            l++;
          }
        } else {
          var src = PixelTanks.images.bullets[property][bullet];
          PixelTanks.images.bullets[property][bullet] = new ImageLoader();
          PixelTanks.images.bullets[property][bullet].src = src;
        }
      }
    }
    for (var property in PixelTanks.images.tanks) {
      for (var tank in PixelTanks.images.tanks[property]) {
        if (tank != 'cosmetics') {
          var src = PixelTanks.images.tanks[property][tank].top;
          PixelTanks.images.tanks[property][tank].top = new ImageLoader();
          PixelTanks.images.tanks[property][tank].top.src = src;
          src = PixelTanks.images.tanks[property][tank].bottom[0];
          PixelTanks.images.tanks[property][tank].bottom[0] = new ImageLoader();
          PixelTanks.images.tanks[property][tank].bottom[0].src = src;
          src = PixelTanks.images.tanks[property][tank].bottom[1];
          PixelTanks.images.tanks[property][tank].bottom[1] = new ImageLoader();
          PixelTanks.images.tanks[property][tank].bottom[1].src = src;
        }
      }
      var l = 0;
      while (l < PixelTanks.images.tanks[property].cosmetics.length) {
        PixelTanks.images.tanks[property].cosmetics[l].image.src = '/images/tanks/' + property + '/cosmetics/' + PixelTanks.images.tanks[property].cosmetics[l].id + '.png';
        l++;
      }
    }
    for (var property in PixelTanks.images.menus) {
      for (var menu in PixelTanks.images.menus[property]) {
        var src = PixelTanks.images.menus[property][menu];
        PixelTanks.images.menus[property][menu] = new ImageLoader();
        PixelTanks.images.menus[property][menu].src = src;
      }
    }

    var menuActions = {
      start: {
        buttons: [],
        exec: [{
          x: 100,
          y: 300,
          w: 300,
          h: 100,
          ref: `(() => {
            if (sessionStorage.username == undefined) {
              var t = confirm('Do you want to play as Guest?');
              if (t) {
              var t = prompt('Play as Guest(g) or Accounts(a)?');
              if (t === 'g') {
                PixelTanks.userData = {
                  username: 'Guest#' + new String(Math.floor(Math.random() * 9)) + new String(Math.floor(Math.random() * 9)) + new String(Math.floor(Math.random() * 9)) + new String(Math.floor(Math.random() * 9)),
                  health: 200,
                  coins: 0,
                  level: 1,
                  boosts: 0,
                  blocks: 0,
                  flashbangs: 0,
                  toolkits: 0,
                  class: 'normal',
                  stealth: false,
                  builder: false,
                  tactical: false,
                  summoner: false,
                  steel: false,
                  crystal: false,
                  dark: false,
                  light: false,
                  fire1: true,
                  fire2: false,
                  fire3: false,
                  cosmetic: '',
                  cosmetics: [],
                  xp: 0,
                  rank: 1,
                  crates: 0,
                };
                PixelTanks.user = {};
                PixelTanks.user.username = PixelTanks.userData.username;
                Menus.trigger('main');
              } else {
                /*document.writeln('Ok.');
                document.writeln('Redirecting to Sign In...');
                setTimeout(() => {
                  window.location.href = '/account';
                }, 500);
                var t = prompt('Login(l) or Signup(s)');
                if (t === 'l') {
                  Network.auth(prompt('Username'), prompt('Password'), 'login', () => {
                    PixelTanks.user = {};
                    PixelTanks.user.username = sessionStorage.username;
                    Network.get((data) => {
                      PixelTanks.userData = JSON.parse(data.playerdata)['increedible-tanks'];
                      PixelTanks.playerData = JSON.parse(data.playerdata);
                      if (PixelTanks.userData == undefined) {
                        PixelTanks.userData = {
                          username: sessionStorage.username,
                          health: 200,
                          coins: 0,
                          level: 1,
                          blocks: 0,
                          flashbangs: 0,
                          boosts: 0,
                          toolkits: 0,
                          class: 'normal',
                          stealth: false,
                          builder: false,
                          tactical: false,
                          summoner: false,
                          steel: false,
                          crystal: false,
                          dark: false,
                          light: false,
                          fire1: true,
                          fire2: false,
                          fire3: false,
                          cosmetic: '',
                          cosmetics: [],
                          xp: 0,
                          rank: 1,
                          crates: 0,
                        };
                      }
                      PixelTanks.user.level = PixelTanks.userData.level;
                      PixelTanks.user.coins = PixelTanks.userData.coins;
                      Menus.trigger('main');
                    });
                  });
                } else if (t === 's') {
                  
                } else {
                  window.location.href = '/bruh-stap-evan';
                  return;
                }
              }
            } else {
              PixelTanks.user = {};
              PixelTanks.user.username = sessionStorage.username;
              Network.get(function(data) {
                PixelTanks.userData = JSON.parse(data.playerdata)['increedible-tanks'];
                PixelTanks.playerData = JSON.parse(data.playerdata);
                if (PixelTanks.userData == undefined) {
                  PixelTanks.userData = {
                    username: sessionStorage.username,
                    health: 200,
                    coins: 0,
                    level: 1,
                    blocks: 0,
                    flashbangs: 0,
                    boosts: 0,
                    toolkits: 0,
                    class: 'normal',
                    stealth: false,
                    builder: false,
                    tactical: false,
                    summoner: false,
                    steel: false,
                    crystal: false,
                    dark: false,
                    light: false,
                    fire1: true,
                    fire2: false,
                    fire3: false,
                    cosmetic: '',
                    cosmetics: [],
                    xp: 0,
                    rank: 1,
                    crates: 0,
                  };
                }
                PixelTanks.user.level = PixelTanks.userData.level;
                PixelTanks.user.coins = PixelTanks.userData.coins;
                Menus.trigger('main');
              });
            }
          })();`,
        }],
        listeners: {},
        customOnLoad: () => {},
      },
      main: {
        buttons: [{
          x: 307,
          y: 412,
          w: 188,
          h: 83,
          ref: 'multiplayer',
        }, {
          x: 5,
          y: 412,
          w: 188,
          h: 83,
          ref: 'levelSelect',
        }, {
          x: 6,
          y: 6,
          w: 53,
          h: 53,
          ref: 'shopItems',
        }, {
          x: 442,
          y: 6,
          w: 53,
          h: 53,
          ref: 'mainCosmetics',
        }],
        exec: [],
        listeners: {},
        customOnLoad: () => {
          GUI.draw.textAlign = 'left';
          if (PixelTanks.userData.heath == 200) {
            GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].red.bottom[0], 90, 120);
            GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].red.top, 90, 120);
          } else if (PixelTanks.userData.health == 300) {
            GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].iron.bottom[0], 90, 120);
            GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].iron.top, 90, 120);
          } else if (PixelTanks.userData.health == 400) {
            GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].diamond.bottom[0], 90, 120);
            GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].diamond.top, 90, 120)
          } else if (PixelTanks.userData.health == 500) {
            GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].dark.bottom[0], 90, 120);
            GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].dark.top, 90, 120);
          } else if (PixelTanks.userData.health == 600) {
            GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].light.bottom[0], 90, 120);
            GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].light.top, 90, 120);
          }
          if (PixelTanks.userData.cosmetic != '' || PixelTanks.userData.cosmetic != undefined) {
            var l = 0;
            while (l < PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics.length) {
              if (PixelTanks.userData.cosmetic == PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[l].name) {
                GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[l].image, 90, 120);
                break; // CLEANUP is this faster?????
              }
              l++;
            }
          }
          GUI.draw.fillStyle = '#ffffff';
          GUI.draw.font = '15px starfont';
          GUI.draw.fillText(PixelTanks.user.username, 150, 120);
          var xpToLevelUp = Math.ceil(Math.pow(1.6, PixelTanks.userData.rank - 1) + 20 * (PixelTanks.userData.rank - 1));
          while (PixelTanks.userData.xp >= xpToLevelUp) {
            PixelTanks.userData.xp -= xpToLevelUp;
            PixelTanks.userData.rank += 1;
            xpToLevelUp = Math.ceil(Math.pow(1.6, PixelTanks.userData.rank - 1) + 20 * (PixelTanks.userData.rank - 1));
          }
          GUI.draw.fillText('Rank: ' + PixelTanks.userData.rank, 150, 150);
          GUI.draw.fillText('XP - ' + PixelTanks.userData.xp + '/' + xpToLevelUp, 150, 180);
          //draw.drawImage(coins, 300, 100); CLEANUP
          GUI.draw.fillText(PixelTanks.userData.coins, 350, 130);
        },
      },
      multiplayer: {
        buttons: [{
          x: 58,
          y: 361,
          w: 188,
          h: 83,
          ref: 'co_op'
        }, {
          x: 255,
          y: 361,
          w: 188,
          h: 83,
          ref: 'pvp',
        }, {
          x: 59,
          y: 56,
          w: 53,
          h: 53,
          ref: 'main',
        }],
        exec: [],
        listeners: {},
        customOnLoad: () => {} // Server Data?
      },
      levelSelect: {
        buttons: [],
        exec: [],
        listeners: {},
        customOnLoad: () => {
          GUI.draw.fillStyle = '#556B2f';
          GUI.draw.fillRect(50, 50, 400, 400);
          GUI.draw.fillStyle = '#ffffff';
          GUI.draw.fillRect(139, 94, 44, 44);
          if (PixelTanks.userData.level < 2) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(228, 94, 44, 44);
          if (PixelTanks.userData.level < 3) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(317, 94, 44, 44);
          if (PixelTanks.userData.level < 4) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(139, 94 + 44 * 2, 44, 44);
          if (PixelTanks.userData.level < 5) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(228, 94 + 44 * 2, 44, 44);
          if (PixelTanks.userData.level < 6) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(317, 94 + 44 * 2, 44, 44);
          if (PixelTanks.userData.level < 7) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(139, 94 + 44 * 4, 44, 44);
          if (PixelTanks.userData.level < 8) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(228, 94 + 44 * 4, 44, 44);
          if (PixelTanks.userData.level < 9) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(317, 94 + 44 * 4, 44, 44);
          if (PixelTanks.userData.level < 10) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(139, 94 + 44 * 6, 44, 44);
          if (PixelTanks.userData.level < 11) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(228, 94 + 44 * 6, 44, 44);
          if (PixelTanks.userData.level < 12) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(317, 94 + 44 * 6, 44, 44);
          GUI.draw.fillStyle = '#000000';
          GUI.draw.font = '20px starfont';
          GUI.draw.fillText('1', 139 + 15, 94 + 30);
          if (PixelTanks.userData.level < 2) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('2', 228 + 15, 94 + 30);
          if (PixelTanks.userData.level < 3) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('3', 317 + 15, 94 + 30);
          if (PixelTanks.userData.level < 4) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('4', 139 + 15, 94 + 44 * 2 + 30);
          if (PixelTanks.userData.level < 5) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('5', 228 + 15, 94 + 44 * 2 + 30);
          if (PixelTanks.userData.level < 6) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('6', 317 + 15, 94 + 44 * 2 + 30);
          if (PixelTanks.userData.level < 7) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('7', 139 + 15, 94 + 44 * 4 + 30);
          if (PixelTanks.userData.level < 8) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('8', 228 + 15, 94 + 44 * 4 + 30);
          if (PixelTanks.userData.level < 9) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('9', 317 + 15, 94 + 44 * 4 + 30);
          if (PixelTanks.userData.level < 10) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('10', 139 + 15, 94 + 44 * 6 + 30);
          if (PixelTanks.userData.level < 11) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('11', 228 + 15, 94 + 44 * 6 + 30);
          if (PixelTanks.userData.level < 12) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('12', 317 + 15, 94 + 44 * 6 + 30);
        },
      },
      levelSelect2: {
        buttons: [],
        exec: [],
        listeners: {},
        customOnLoad: () => {
          GUI.draw.fillStyle = '#556B2f';
          GUI.draw.fillRect(50, 50, 400, 400);
          GUI.draw.fillStyle = '#ffffff';
          if (PixelTanks.userData.level < 13) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(139, 94, 44, 44);
          if (PixelTanks.userData.level < 14) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(228, 94, 44, 44);
          if (PixelTanks.userData.level < 15) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(317, 94, 44, 44);
          if (PixelTanks.userData.level < 16) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(139, 94 + 44 * 2, 44, 44);
          if (PixelTanks.userData.level < 17) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(228, 94 + 44 * 2, 44, 44);
          if (PixelTanks.userData.level < 18) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(317, 94 + 44 * 2, 44, 44);
          if (PixelTanks.userData.level < 19) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(139, 94 + 44 * 4, 44, 44);
          if (PixelTanks.userData.level < 20) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(228, 94 + 44 * 4, 44, 44);
          if (PixelTanks.userData.level < 21) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(317, 94 + 44 * 4, 44, 44);
          if (PixelTanks.userData.level < 22) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(139, 94 + 44 * 6, 44, 44);
          if (PixelTanks.userData.level < 23) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(228, 94 + 44 * 6, 44, 44);
          if (PixelTanks.userData.level < 24) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(317, 94 + 44 * 6, 44, 44);
          GUI.draw.fillStyle = '#000000';
          GUI.draw.font = '20px starfont';
          if (PixelTanks.userData.level < 13) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('13', 139 + 15, 94 + 30);
          if (PixelTanks.userData.level < 14) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('14', 228 + 15, 94 + 30);
          if (PixelTanks.userData.level < 15) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('15', 317 + 15, 94 + 30);
          if (PixelTanks.userData.level < 16) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('16', 139 + 15, 94 + 44 * 2 + 30);
          if (PixelTanks.userData.level < 17) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('17', 228 + 15, 94 + 44 * 2 + 30);
          if (PixelTanks.userData.level < 18) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('18', 317 + 15, 94 + 44 * 2 + 30);
          if (PixelTanks.userData.level < 19) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('19', 139 + 15, 94 + 44 * 4 + 30);
          if (PixelTanks.userData.level < 20) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('20', 228 + 15, 94 + 44 * 4 + 30);
          if (PixelTanks.userData.level < 21) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('21', 317 + 15, 94 + 44 * 4 + 30);
          if (PixelTanks.userData.level < 22) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('22', 139 + 15, 94 + 44 * 6 + 30);
          if (PixelTanks.userData.level < 23) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('23', 228 + 15, 94 + 44 * 6 + 30);
          if (PixelTanks.userData.level < 24) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('24', 317 + 15, 94 + 44 * 6 + 30);
        },
      },
      levelSelect3: {
        buttons: [],
        exec: [],
        listeners: [],
        customOnLoad: () => {
          GUI.draw.fillStyle = '#556B2f';
          GUI.draw.fillRect(50, 50, 400, 400);
          GUI.draw.fillStyle = '#ffffff';
          if (PixelTanks.userData.level < 25) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(139, 94, 44, 44);
          if (PixelTanks.userData.level < 26) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(228, 94, 44, 44);
          if (PixelTanks.userData.level < 27) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(317, 94, 44, 44);
          if (PixelTanks.userData.level < 28) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(139, 94 + 44 * 2, 44, 44);
          if (PixelTanks.userData.level < 29) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(228, 94 + 44 * 2, 44, 44);
          if (PixelTanks.userData.level < 30) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(317, 94 + 44 * 2, 44, 44);
          if (PixelTanks.userData.level < 31) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(139, 94 + 44 * 4, 44, 44);
          if (PixelTanks.userData.level < 32) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(228, 94 + 44 * 4, 44, 44);
          if (PixelTanks.userData.level < 33) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(317, 94 + 44 * 4, 44, 44);
          if (PixelTanks.userData.level < 34) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(139, 94 + 44 * 6, 44, 44);
          if (PixelTanks.userData.level < 35) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(228, 94 + 44 * 6, 44, 44);
          if (PixelTanks.userData.level < 36) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(317, 94 + 44 * 6, 44, 44);
          GUI.draw.fillStyle = '#000000';
          GUI.draw.font = '20px starfont';
          if (PixelTanks.userData.level < 25) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('25', 139 + 15, 94 + 30);
          if (PixelTanks.userData.level < 26) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('26', 228 + 15, 94 + 30);
          if (PixelTanks.userData.level < 27) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('27', 317 + 15, 94 + 30);
          if (PixelTanks.userData.level < 28) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('28', 139 + 15, 94 + 44 * 2 + 30);
          if (PixelTanks.userData.level < 29) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('29', 228 + 15, 94 + 44 * 2 + 30);
          if (PixelTanks.userData.level < 30) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('30', 317 + 15, 94 + 44 * 2 + 30);
          if (PixelTanks.userData.level < 31) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('31', 139 + 15, 94 + 44 * 4 + 30);
          if (PixelTanks.userData.level < 32) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('32', 228 + 15, 94 + 44 * 4 + 30);
          if (PixelTanks.userData.level < 33) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('33', 317 + 15, 94 + 44 * 4 + 30);
          if (PixelTanks.userData.level < 34) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('34', 139 + 15, 94 + 44 * 6 + 30);
          if (PixelTanks.userData.level < 35) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('35', 228 + 15, 94 + 44 * 6 + 30);
          if (PixelTanks.userData.level < 36) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('36', 317 + 15, 94 + 44 * 6 + 30);
        }
      },
      levelSelect4: {
        buttons: [],
        exec: [],
        listeners: {},
        customOnLoad: () => {
          GUI.draw.fillStyle = '#556B2f';
          GUI.draw.fillRect(50, 50, 400, 400);
          GUI.draw.fillStyle = '#ffffff';
          if (PixelTanks.userData.level < 37) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(139, 94, 44, 44);
          if (PixelTanks.userData.level < 38) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(228, 94, 44, 44);
          if (PixelTanks.userData.level < 39) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(317, 94, 44, 44);
          if (PixelTanks.userData.level < 40) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(139, 94 + 44 * 2, 44, 44);
          if (PixelTanks.userData.level < 41) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(228, 94 + 44 * 2, 44, 44);
          if (PixelTanks.userData.level < 42) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(317, 94 + 44 * 2, 44, 44);
          if (PixelTanks.userData.level < 43) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(139, 94 + 44 * 4, 44, 44);
          if (PixelTanks.userData.level < 44) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(228, 94 + 44 * 4, 44, 44);
          if (PixelTanks.userData.level < 45) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(317, 94 + 44 * 4, 44, 44);
          if (PixelTanks.userData.level < 46) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(139, 94 + 44 * 6, 44, 44);
          if (PixelTanks.userData.level < 47) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(228, 94 + 44 * 6, 44, 44);
          if (PixelTanks.userData.level < 48) GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(317, 94 + 44 * 6, 44, 44);
          GUI.draw.fillStyle = '#000000';
          GUI.draw.font = '20px starfont';
          if (PixelTanks.userData.level < 37) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('37', 139 + 15, 94 + 30);
          if (PixelTanks.userData.level < 38) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('38', 228 + 15, 94 + 30);
          if (PixelTanks.userData.level < 39) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('39', 317 + 15, 94 + 30);
          if (PixelTanks.userData.level < 40) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('40', 139 + 15, 94 + 44 * 2 + 30);
          if (PixelTanks.userData.level < 41) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('41', 228 + 15, 94 + 44 * 2 + 30);
          if (PixelTanks.userData.level < 42) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('42', 317 + 15, 94 + 44 * 2 + 30);
          if (PixelTanks.userData.level < 43) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('43', 139 + 15, 94 + 44 * 4 + 30);
          if (PixelTanks.userData.level < 44) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('44', 228 + 15, 94 + 44 * 4 + 30);
          if (PixelTanks.userData.level < 45) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('45', 317 + 15, 94 + 44 * 4 + 30);
          if (PixelTanks.userData.level < 46) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('46', 139 + 15, 94 + 44 * 6 + 30);
          if (PixelTanks.userData.level < 47) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('47', 228 + 15, 94 + 44 * 6 + 30);
          if (PixelTanks.userData.level < 48) GUI.draw.fillStyle = '#A9A9A9';
          GUI.draw.fillText('48', 317 + 15, 94 + 44 * 6 + 30);
        }
      },
      shopItems: {
        buttons: [{
          x: 60,
          y: 60,
          w: 80,
          h: 30,
          ref: 'main',
        }],
        exec: [{
          x: 100,
          y: 140,
          w: 100,
          h: 50,
          ref: `(() => {
            if (PixelTanks.userData.coins >= 50) {
              PixelTanks.userData.coins -= 50;
              PixelTanks.userData.boosts += 1;
              PixelTanks.save();
              Menus.redraw();
            } else {
              alert('Not Enough Money! You are ' + JSON.stringify(50 - PixelTanks.userData.coins) + ' coins short!');
            }
          })();`,
        }, {
          x: 100,
          y: 210,
          w: 100,
          h: 50,
          ref: `(() => {
            if (PixelTanks.userData.coins >= 200) {
              PixelTanks.userData.coins -= 200;
              PixelTanks.userData.blocks += 1;
              PixelTanks.save();
              Menus.redraw();
            } else {
              alert('Not Enough Money! You are ' + JSON.stringify(200 - PixelTanks.userData.coins) + ' coins short!');
            }
          })();`,
        }, {
          x: 100,
          y: 280,
          w: 100,
          h: 50,
          ref: `(() => {
            if (PixelTanks.userData.coins >= 500) {
              PixelTanks.userData.coins -= 500;
              PixelTanks.userData.toolkits += 1;
              PixelTanks.save();
              Menus.redraw();
            } else {
              alert('Not Enough Money! You are ' + JSON.stringify(500 - PixelTanks.userData.coins) + ' coins short!');
            }
          })();`,
        }, {
          x: 100,
          y: 350,
          w: 100,
          h: 50,
          ref: `(() => {
            if (PixelTanks.userData.coins >= 1000) {
              PixelTanks.userData.coins -= 1000;
              PixelTanks.userData.flashbangs += 1;
              PixelTanks.save();
              Menus.redraw();
            } else {
              alert('Not Enough Money! You are ' + JSON.stringify(1000 - PixelTanks.userData.coins) + ' coins short!');
            }
          })();`,
        }],
        listeners: {
          mousedown: function(e) {
            PixelTanks.shopItemsCooldown = setTimeout(function(e) {
              PixelTanks.shopItemsLooper = setInterval((e) => {
                if (e.pageX || e.pageY) {
                  x = e.pageX;
                  y = e.pageY;
                } else {
                  x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                  y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
                }
                x -= GUI.canvas.offsetLeft;
                y -= GUI.canvas.offsetTop;
                x /= PixelTanks.resizer;
                y /= PixelTanks.resizer;
                if (x > 100) {
                  if (x < 200) {
                    if (y > 140) {
                      if (y < 190) {
                        if (PixelTanks.userData.coins >= 50) {
                          PixelTanks.userData.coins -= 50;
                          PixelTanks.userData.boosts += 1;
                          Menus.redraw();
                        } else {
                          clearInterval(PixelTanks.shopItemsLooper);
                          PixelTanks.save();
                          alert('Not Enough Money! You are ' + JSON.stringify(50 - PixelTanks.userData.coins) + ' coins short!');
                        }
                      }
                    }
                  }
                }
                if (x > 100) {
                  if (x < 200) {
                    if (y > 210) {
                      if (y < 260) {
                        if (PixelTanks.userData.coins >= 200) {
                          PixelTanks.userData.coins -= 200;
                          PixelTanks.userData.blocks += 1;
                          Menus.redraw();
                        } else {
                          clearInterval(PixelTanks.shopItemsLooper);
                          PixelTanks.save();
                          alert('Not Enough Money! You are ' + JSON.stringify(200 - PixelTanks.userData.coins) + ' coins short!');
                        }
                      }
                    }
                  }
                }
                if (x > 100) {
                  if (x < 200) {
                    if (y > 280) {
                      if (y < 330) {
                        if (PixelTanks.userData.coins >= 500) {
                          PixelTanks.userData.coins -= 500;
                          PixelTanks.userData.toolkits += 1;
                          Menus.redraw();
                        } else {
                          clearInterval(PixelTanks.shopItemsLooper);
                          PixelTanks.save();
                          alert('Not Enough Money! You are ' + JSON.stringify(500 - PixelTanks.userData.coins) + ' coins short!');
                        }
                      }
                    }
                  }
                }
                if (x > 100) {
                  if (x < 200) {
                    if (y > 350) {
                      if (y < 400) {
                        if (PixelTanks.userData.coins >= 1000) {
                          PixelTanks.userData.coins -= 1000;
                          PixelTanks.userData.flashbangs += 1;
                          Menus.redraw();
                        } else {
                          clearInterval(PixelTanks.shopItemsLooper);
                          PixelTanks.save();
                          alert('Not Enough Money! You are ' + JSON.stringify(1000 - PixelTanks.userData.coins) + ' coins short!');
                        }
                      }
                    }
                  }
                }
              }, 20, e);
            }, 500, e);
          },
          mouseup: function shopItemsSupport3(e) {
            clearTimeout(PixelTanks.shopItemsCooldown);
            clearInterval(PixelTanks.shopItemsLooper);
            PixelTanks.save();
          },
        },
        customOnLoad: () => {
          GUI.draw.fillStyle = '#ffffff';
          GUI.draw.font = '10px starfont';
          GUI.draw.fillText('Coins: ' + PixelTanks.userData.coins, 360, 100);
          GUI.draw.fillText('Boosts: ' + PixelTanks.userData.boosts, 345, 170);
          GUI.draw.fillText('Blocks: ' + PixelTanks.userData.blocks, 345, 240);
          GUI.draw.fillText('Toolkits: ' + PixelTanks.userData.toolkits, 345, 310);
          GUI.draw.fillText('Flashbangs: ' + PixelTanks.userData.flashbangs, 345, 380);
        },
      },
      shopTanks: {
        buttons: [{
          x: 60,
          y: 60,
          w: 80,
          h: 30,
          ref: 'main',
        }],
        exec: [{

        }],
        listeners: {},
        customOnLoad: () => {
          alert('Working...')
        }
      },
      shopClasses: {
        buttons: [{
          x: 60,
          y: 60,
          w: 80,
          h: 30,
          ref: 'main',
        }],
        exec: [{

        }],
        customOnLoad: () => {

        },
      },
      shopKits: {

      },
      pvp: {
        buttons: [{
          x: 59,
          y: 56,
          w: 53,
          h: 53, // square???
          ref: 'multiplayer',
        }, {
          x: 255,
          y: 389,
          w: 188,
          h: 55,
          ref: 'duels',
        }, {
          x: 48,
          y: 389,
          w: 188,
          h: 55,
          ref: 'ffa',
        }],
        exec: [],
        customOnLoad: () => {},
      },
      co_op: {
        buttons: [{

        }],
        exec: [],
        customOnLoad: () => {
          alert('This area is still in testing...');
          Menus.trigger('multiplayer');
        },
      },
      ffa: {
        buttons: [{
          x: 59,
          y: 56,
          w: 53,
          h: 53,
          ref: 'pvp',
        }],
        exec: [{
          x: 158,
          y: 233,
          w: 188,
          h: 55,
          ref: `(() => {
            Menus.removeListeners();
            PixelTanks.user.joiner = new MultiPlayerTank();
            PixelTanks.user.joiner.control();
            // join ffa server with ip and room (or quick join)
          })();`
        }],
        listeners: {},
        customOnLoad: () => {
          // initialize key input for server ip and join type
        },
      },
      duels: {
        buttons: [{
          x: 59,
          y: 56,
          w: 53,
          h: 53,
          ref: 'main',
        }],
        exec: [{

        }],
        listeners: {},
        customOnLoad: () => {},
      },
      cosmeticMain: {
        buttons: [{
          x: 59,
          y: 56,
          w: 53,
          h: 53,
          ref: 'main',
        }, {
          x: 388,
          y: 57,
          w: 23,
          h: 53,
          ref: '' // my cosmetics area
        }],
        exec: [],
        listeners: {},
        customOnLoad: () => {
          GUI.draw.textAlign = 'center';
          GUI.draw.fillText('Crates: '+PixelTanks.userData.crates, 250, 130);
          GUI.draw.textAlign = 'left';
        }
      },
      cosmeticLeft: {},
      cosmeticMiddle: '/images/menus/default/cosmetics_middle.png',
      cosmeticRight: '/images/menus/default/cosmetics_right.png',
      defeat: '/images/menus/default/defeat.png',
      victory: '/images/menus/default/victory.png',
    }

    Menus.menus = {
      start: '',
      main: '',
      multiplayer: '',
      levelSelect: '',
      levelSelect2: '',
      levelSelect3: '',
      levelSelect4: '',
      shopItems: '',
      shopTanks: '',
      shopClasses: '',
      shopKits: '',
      pvp: '',
      co_op: '',
      ffa: '',
      duels: '',
      cosmeticMain: '',
      cosmeticLeft: '',
      cosmeticMiddle: '',
      cosmeticRight: '',
      defeat: '',
      victory: '',
    }

    for (var property in Menus.menus) {
      if (property.includes('levelSelect')) {
        var menuNum = property.replace('levelSelect', '');
        if (menuNum == '') menuNum = 1;
        menuNum = new Number(menuNum);
        var y = 0;
        while (y < 4) {
          var x = 0;
          while (x < 3) {
            menuActions[property].exec.push({
              x: 89 * x + 140,
              y: 88 * y + 94,
              w: 44,
              h: 44,
              ref: `(() => {alert('Levels are under revision. Sorry :(');/*PixelTanks.user.world.level = ` + (12 * (menuNum - 1) + (x + y * 3)) + `; PixelTanks.user.world.init();})();`,
            });
            x++;
          }
          y++;
        }
      }
      Menus.menus[property] = new Menu(function() { // No arrow function here
        if (PixelTanks.images.menus[PixelTanks.texturepack][this.id]) {
          GUI.draw.drawImage(PixelTanks.images.menus[PixelTanks.texturepack][this.id], 0, 0);
        }
        this.data.customOnLoad();
      }, {
        click: Menus.onclick,
        ...menuActions[property].listeners,
      }, GUI.canvas);
      Menus.menus[property].data = menuActions[property];
      Menus.menus[property].id = property;
    }
    PixelTanks.socket = new MegaSocket('wss://' + window.location.hostname, {
      keepAlive: false,
    });
  }

  static launch() {
    Menus.trigger('start');
  }

  static save() {
    try {
      var temp = PixelTanks.playerData;
      temp['increedible-tanks'] = PixelTanks.userData;
      Network.update('playerdata', JSON.stringify(temp));
    } catch (e) {
      // PixelTanks.user is not logged in
      console.log('User Not Logged In?: ERR->' + e);
    }
  }*/
}